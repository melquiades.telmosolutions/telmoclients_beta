<?php

require_once "db.php";
ini_set('max_execution_time', 300);
session_start();
error_reporting();

if(isset($_SESSION["login"])){
	
require_once "include/header.php";
require_once "body.php";
require_once "include/footer.php";
}else{
	header("Location: include/login.php");
}
?>
