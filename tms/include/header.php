<!DOCTYPE html>
<html>
  <head>
    <title>Telmo Employee Profile Management System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="media/css/jquery_ui.css" rel="stylesheet" media="screen">
    
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="media/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="media/css/jquery.dataTables.css">
  <!--   <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/buttons.css" rel="stylesheet">
    <link href="css/forms.css" rel="stylesheet">
    <link href="vendors/form-helpers/css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <link href="vendors/select/bootstrap-select.min.css" rel="stylesheet">
    <link href="vendors/tags/css/bootstrap-tags.css" rel="stylesheet">
    <!-- Dropzone.js -->
    <!-- <link href="vendors1/dropzone/dist/min/dropzone.min.css" rel="stylesheet"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
  </head>
