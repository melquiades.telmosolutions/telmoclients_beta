<?php require_once "do_login.php"; 
if(isset($_SESSION["login"])){ 
  header("Location: ../index.php?page=home"); 
}else{ 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Telmo Management System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="../media/css/bootstrap.css">
    <!-- styles -->
    <link href="../css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="login.php">Telmo Management System</a></h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

	<div class="page-content container">
		<div class="row">
			<div class="col-md-4">
			</div>
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
			        <div class="box">
			            <div class="content-wrap">
			            	<form role="form" action="do_login.php" method="POST">
				                <h6>EMPLOYEE LOGIN:</h6>
				                <input name="user" class="form-control" type="text" placeholder="EMPLOYEE ID #" />
				                <input name="pass" class="form-control" type="password" placeholder="PASSWORD" />
				                <div class="action">
				                	<input type="submit" name="login" value="Login" class="btn btn-primary signup">
				                </div>
			                </form>                
			            </div>
			        </div>

			        <div class="already">
			         <p>Go back to <a href="http://www.telmosolutions.com">Telmo Solutions home page.</a></p>
			          <!--  <p>Register a new employee</p> =>
			            <a href="register.php">Register</a> -->
			        </div> 
			    </div>
			</div>
		</div>
	</div>


  </body>
</html>
<?php
}
?>