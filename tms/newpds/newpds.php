<div class="content-box-large">
	<div class="panel-heading">
		<center><h1 class="page-head-line"><p class="text-gray-dark">Personal Data Sheet</p></h1></center>
		 	<hr>
	</div>
	<div class="panel-heading" >
		<h5><b>I. Personal Information</b></h5>
		<hr>
	</div>
	<div class="row">
	 	<div class="panel-body">
	 		<div class="col-md-12 panel-info">
	 			<div class="content-box-large">
				<form role="form" action="" method="post" enctype="multipart/form-data">				  
					<div class="row">
						<div class="col-sm-6">
						<label>Status</label>
							<select name="Status" class="form-control" required>
								<option value="">Select Status</option>
                                <option value="Regular">Regular</option>
                                <option value="Casual">Casual</option>
								<option value="Retired">Retired</option>
                                 <option value="Resign">Resign</option>
                            </select>
						<label>Office</label>
							<select name="Office" class="form-control" required>
								<option value="">Select Office</option>
								<option value="cao">CAO - City Accountants Office</option>
								<option value="cadmo">C ADMIN - City Administrators Office</option>
								<option value="cagrio">C AGRI - City Agricultures Office</option>
								<option value="carchto">C ARCHT - City Architects Office</option>
								<option value="casso">CASSO - City Assessors Office</option>
								<option value="cbo">CBO - City Budget Office</option>
								<option value="ccro">CCRO - City Civil Registrars Office</option>
								<option value="cdo">CDO - City Division Office</option>
								<option value="ceo">CEO - City Engineers Office</option>
								<option value="cenro">C ENRO - City Environment and Natural Resources Office</option>
								<option value="cgso">CGSO - City General Services Office</option>
								<option value="cio">CIO - City Information Office</option>
								<option value="cho">CHO - City Health Office</option> 
								<option value="chcdo">C HOUSING - City Housing and Community Development Office</option>  
								<option value="cmo">CMO - City Mayors Office</option>         
								<option value="cpdo">CPDO - City Planning and Development Office</option>
								<option value="cswdo">CSWDO - City Social Welfare and Development Office</option>
								<option value="cto">CTO - City Treasures Office</option>
								<option value="cvo">CVO - City Veterinary Office</option>
								<option value="iaso">IASO - Internal Audit Service office</option>
								<option value="clgoo-dilg">CLGOO-dilg - City Local Government Operations Office/DILG</option> 
								<option value="hrmdo">HRMDO - Human Resource Management Development Office</option>
								<option value="tomeco">TOMECO - Traffic Operations Management Enforcement Control Office</option> 
								<option value="eemd">EEMD - Economic Enterprise Management Division</option>
								<option value="ccdlao">CCDLAO - City Cooperatives Dev't and Livelihood Assistance Office</option>
								<option value="peso">PESO - Public Employment Services Office</option> 
								<option value="blpd">BLPD - Business License and Permits Division</option>
								<option value="ctoo">CTOO - City Tourism Operations Office</option>   
								<option value="cno">CNO - City Nutrition Office</option> 
								<option value="masa">Mayor Alfred Social Action</option>
								<option value="sp">SP - Sangguniang Panlungsod</option> 
								<option value="tch">TCH - Tacloban City Hospital</option>
								<option value="tccc">TCCC - Tacloban City Convenction Center</option>
								<option value="tcso">TCSO - Tacloban City Sports Office</option>
								<option value="misd">MISD - Management Information System</option>
								<option value="cpo">CPO - City Population Office</option>          
								<option value="clo">CLO - City Legal Office</option>
								<option value="clep">CLEP - Comprehensive Livelihood and Entrepreneurship Program</option>
								<option value="BAO">BAO - Brgy. Affairs Office</option>
								<option value="BAC/SUPPLY">BAC - Bids and Awards Committee</option>
								<option value="MASA">MASA Office – Special Health Service</option>
								<option value="FLET">FLET - Fisheries Law Enforcement Team</option>
								<option value="pdao">PDAO - Persons with disability affairs office</option>
								<option value="CSSO">CSSO - City Security Service Office</option>
								<option value="CARPOOL">CARPOOL</option>
								<option value="osca">OSCA - Office of Senior Citizens Affairs</option>
								<option value="CDRRMO">CDRRMO - City Disaster Risk Reduction And Management Office</option>
								<option value="TNBT">TNBT - Tacloban New Bus Terminal</option>
							</select>
							<label>Employee No.</label>
                            <input name="employeeID" class="form-control" type="text" required>
											
							<label>BIO No.</label>
                            <input name="bioID" class="form-control" type="text" required>
											
							<label>Surname</label>
                            <input name="Sname" class="form-control" type="text" required>
                                           							
                            <label>First name</label>
                            <input name="Fname" class="form-control" type="text" required>
										
                            <label>Middle name</label>
                            <input name="Mname" class="form-control" type="text" required>
											
							<label>Name Extension (e.g Jr.)</label>
                            <input name="NExten" class="form-control" type="text">
											
							<label>Date of Birth (mm/dd/yyyy)</label>
							<input name="Bday" class="form-control" type="date" required>
											 
							<label>Place of Birth</label>
                            <input name="Pbirth" class="form-control" type="text">
											
							<label>Sex</label>
                            <select name="Sex" class="form-control" required>
								<option value="">Select Gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
											
							<label>Civil Status</label>
							<select name="CivilStatus" class="form-control" required>
								<option value="">Select CivilStatus</option>
                                <option value="single">Single</option>
								<option value="married">Married</option>
								<option value="annulled">Annulled</option>
								<option value="widowed">Widowed</option>
								<option value="separated">Separated</option>
							</select>
											
							<label>Citizenship</label>
                            <input name="Citizenship" class="form-control" type="text" required>
											
							<label>Height (m)</label>
                            <input name="Height" class="form-control" type="text">
											
							<label>Weight (kg)</label>
                            <input name="Weight" class="form-control" type="text">
                            <div class="col-sm-5">   
	                		<br>      	
							<img id="image" src="images/user.png" width="100%" height="100%" />
							<br>
							<input type="file" name="image" id="file"
							onchange="document.getElementById('image').src = window.URL.createObjectURL(this.files[0])" style="display:none;">
							<input class="btn btn-success btn-sm" type="button" value="Upload Here!" onclick="document.getElementById('file').click();" />
							</div>
                        </div>
						<div class="col-sm-6">  
							<label>Blood Type</label>
                            <input name="Blood" class="form-control" type="text">
										
							<label>GSIS ID No.</label>
                            <input name="GSIS" class="form-control" type="text">
										
							<label>PAG-IBIG ID No.</label>
                            <input name="Pagibig" class="form-control" type="text">	
										
							<label>Philhealth No.</label>
                            <input name="Philhealth" class="form-control" type="text">
											
							<label>SSS No.</label>
                            <input name="SSS" class="form-control" type="text">
											
                            <label>Residential Address</label>
                            <input name="RAddress" class="form-control">
                                                                                    
                            <label>Zip Code</label>
                            <input name="Zipcode" class="form-control" type="text">
                                    
                            <label>Telephone No.</label>
                            <input name="Telephone" class="form-control" type="text">
											
							<label>Permanent Address</label>
                            <input name="PAddress" class="form-control" type="text">
											
							<label>Zip Code</label>
                            <input name="Zipcode1" class="form-control" type="text">
											
							<label>Telephone No.</label>
                            <input name="Telephone1" class="form-control" type="text">
											
							<label>Email Address (if any)</label>
                            <input name="Email" class="form-control" type="text">
											
							<label>Cellphone No. (if any)</label>
                            <input name="Cellphone" class="form-control" type="text">
											
							<label>Agency Employee No.</label>
                            <input name="AgencyEmployee" class="form-control" type="text">
											
							<label>TIN</label>
                            <input name="TIN" class="form-control" type="text">  
                		</div>                		
                	</div> 
                	<br>
                	<button type="submit" class="btn btn-info" name="submit">Next 1/8</button>
				</form>
                </div>                            
            </div>                              
        </div>
    </div>
</div>
<?php
	if(isset($_POST['submit'])){
		//Personal Information
		$Status = $_POST['Status'];
		$Office = $_POST['Office'];
		$employeeID = mysql_real_escape_string(utf8_decode($_POST['employeeID']));
		$bioID = mysql_real_escape_string(utf8_decode($_POST['bioID']));					
		$Sname = mysql_real_escape_string(utf8_decode($_POST['Sname']));
		$Fname = mysql_real_escape_string(utf8_decode($_POST['Fname']));
		$Mname = mysql_real_escape_string(utf8_decode($_POST['Mname']));
		$NExten = mysql_real_escape_string(utf8_decode($_POST['NExten']));
		$Bday = $_POST['Bday'];
		$Pbirth = mysql_real_escape_string(utf8_decode($_POST['Pbirth'])); 
		$Sex = $_POST['Sex'];
		$CivilStatus = $_POST['CivilStatus'];
		$Citizenship = mysql_real_escape_string(utf8_decode($_POST['Citizenship']));
		$Height = mysql_real_escape_string(utf8_decode($_POST['Height']));
		$Weight = mysql_real_escape_string(utf8_decode($_POST['Weight']));
		$Blood = mysql_real_escape_string(utf8_decode($_POST['Blood']));
		$GSIS = mysql_real_escape_string(utf8_decode($_POST['GSIS']));
		$Pagibig = mysql_real_escape_string(utf8_decode($_POST['Pagibig']));
		$Philhealth = mysql_real_escape_string(utf8_decode($_POST['Philhealth']));
		$SSS = mysql_real_escape_string(utf8_decode($_POST['SSS']));
		$RAddress = mysql_real_escape_string(utf8_decode($_POST['RAddress']));
		$ResZip = mysql_real_escape_string(utf8_decode($_POST['Zipcode']));
		$ResTelephone = mysql_real_escape_string(utf8_decode($_POST['Telephone']));
		$PAddress = mysql_real_escape_string(utf8_decode($_POST['PAddress']));
		$PZip = mysql_real_escape_string(utf8_decode($_POST['Zipcode1']));
		$PTelephone = mysql_real_escape_string(utf8_decode($_POST['Telephone1']));
		$Email = mysql_real_escape_string(utf8_decode($_POST['Email']));
		$Cellphone = mysql_real_escape_string(utf8_decode($_POST['Cellphone']));
		$AgencyEmployee = mysql_real_escape_string(utf8_decode($_POST['AgencyEmployee']));
		$TIN = mysql_real_escape_string(utf8_decode($_POST['TIN']));
		$age = date("Y-m-d")-$Bday;

		$fullname = $Sname.", ".$Fname." ".$NExten." ".$Mname;
		$password = md5($bioID);

		$searcheID = mysql_query("SELECT  * FROM `personalinfo` WHERE `employeeID` = '".$employeeID."' ");
		$eIDcheck = mysql_fetch_assoc($searcheID);
					
		$searchbID = mysql_query("SELECT  * FROM `personalinfo` WHERE `bioID` = '".$bioID."' ");
		$bioIDcheck = mysql_fetch_assoc($searchbID);
					
		if($employeeID==$eIDcheck['employeeID']){
			echo "<script type = \"text/javascript\">
									alert(\"Employee ID already exists!!\");
									window.location = \"index.php?page=newpds\"
								  </script>";
		}elseif($bioID==$bioIDcheck['bioID']){
			echo "<script type = \"text/javascript\">
									alert(\"BIO ID already exists!!\");
									window.location = \"index.php?page=newpds\"
								  </script>";
		}else{		
			$sql = "INSERT INTO `personalinfo` (`id`, `employeeID`,`bioID`, `status`, `office`, `surname`, `firstname`, `middlename`, `namext`, `dobirth`, `age`, `pobirth`, `sex`, `civilstatus`, `citizenship`, `height`, `weight`, `blood`, `gsisno`, `pagibigno`, `philhealthno`, `sssno`, `resaddress`, `reszip`, `resphone`, `peraddress`, `perzip`, `perphone`, `email`, `cellphone`, `employeeno`, `tin`)
								VALUES (NULL, '$employeeID','$bioID', '$Status', '$Office', '$Sname', '$Fname', '$Mname', '$NExten', '$Bday', '$age', '$Pbirth', '$Sex', '$CivilStatus', '$Citizenship', '$Height', '$Weight', '$Blood', '$GSIS', '$Pagibig', '$Philhealth', '$SSS', '$RAddress', '$ResZip', '$ResTelephone', '$PAddress', '$PZip', '$PTelephone', '$Email', '$Cellphone', '$AgencyEmployee', '$TIN')";
			$query = mysql_query($sql);

			if($query){
			$select = mysql_query("SELECT `id` FROM `personalinfo` WHERE `bioID` = $bioID");
			$getID = mysql_fetch_array($select);
			$idxID = $getID['id'];
			// $sql1= mysql_query("INSERT INTO `dtr_info` (`id`, `idx`, `bioID`, `name`, `office`, `status`, `dtrTime`) 
			// 		VALUES (NULL, '$idxID', $bioID', '$fullname', '$Office', '$Status', '9am-5pm')");
			$users="INSERT INTO `users`(`idx`, `bioID`, `name`, `username`, `password`, `user_level`, `user_type`) VALUES('$idxID','$bioID','$fullname','','$password', '2', 'a')";
		    mysql_query($users);

			$ds = DIRECTORY_SEPARATOR;
			$foldername = "../images";

			$fileupload = basename( $_FILES['image']['name']);
		    $fileType = $_FILES['image']['type'];
		    $fileSize = $_FILES['image']['size'];

		    $tempFile = $_FILES['image']['tmp_name'];
		    $targetPath = dirname( __FILE__ ) . $ds. $foldername . $ds;
		    $targetFile =  $targetPath. $fileupload;
		    
			    if(move_uploaded_file($tempFile,$targetFile)){

				    $sql="INSERT INTO `images`(`idx`, `bioID`, `image`,`type`,`size`, `primaryImage`) VALUES('$idxID','$bioID','$fileupload','$fileType','$fileSize', 1)";
		    		mysql_query($sql);

					echo "<script type = \"text/javascript\">
											alert(\"Success!.\");
											window.location = \"./index.php?page=newpds1&eID=$bioID\"
										  </script>";
				}else{
					echo "<script type = \"text/javascript\">
											alert(\"Success! No Image Upload.\");
											window.location = \"./index.php?page=newpds1&eID=$bioID\"
										  </script>";
				}
			}
		}
	}
?>

