<?php 
$eID = $_GET['eID'];
?>
<div class="content-box-large">
	<div class="panel-heading">
		<center><h1 class="page-head-line"><p class="text-gray-dark">Personal Data Sheet</p></h1></center>
		 	<hr>
	</div>
	<div class="panel-heading" >
		<h5><b>II. Family Background</b></h5>
		<hr>
	</div>
	<div class="row">
	 	<div class="panel-body">
	 		<div class="col-md-12 panel-info">
	 			<div class="content-box-large">
				<form role="form" action="" method="post">
					<div class="row">
						<div class="col-sm-6">
							<label>Spouses's Surname</label>
                            <input name="SpouseSurname" class="form-control" type="text">
											
							<label>First Name</label>
                            <input name="SpouseFirstname" class="form-control" type="text">
											
							<label>Middle Name</label>
                            <input name="SpouseMiddlename" class="form-control" type="text">
											
							<label>Occupation</label>
                            <input name="SpouseOccupation" class="form-control" type="text">
											
							<label>Employer/Bus.Name</label>
                            <input name="SpouseEmployer" class="form-control" type="text">
											
							<label>Business Address</label>
                            <input name="SpouseAddress" class="form-control" type="text">
											
							<label>Telephone No.</label>
                            <input name="SpouseTelephone" class="form-control" type="text">	
                        </div>
						<div class="col-sm-6">  
							<label>Father's Surname</label>
                            <input name="FatherSurname" class="form-control" type="text">
											
							<label>First Name</label>
                            <input name="FatherFirstname" class="form-control" type="text">
											
							<label>Middle Name</label>
                            <input name="FatherMiddlename" class="form-control" type="text">
							<br>
							<label>Mother's Maiden Name</label>
                            <br>												
							<label>Surname</label>
                            <input name="MotherSurname" class="form-control" type="text">
											
							<label>First Name</label>
                            <input name="MotherFirstname" class="form-control" type="text">
							
							<label>Middle Name</label>
                            <input name="MotherMiddlename" class="form-control" type="text">                      
                		</div>
                	</div>
                	<div class="row">
						<div class="col-sm-6">
							<br>
							<label>Name of Child (Write full name and list all)</label>
							<input name="Child1" class="form-control" type="text">
							<br>									
                            <input name="Child2" class="form-control" type="text">
							<br>											
                            <input name="Child3" class="form-control" type="text">
							<br>											
                            <input name="Child4" class="form-control" type="text">
							<br>											
                            <input name="Child5" class="form-control" type="text">
							<br>
							<input name="Child6" class="form-control" type="text">
							<br>											
                            <input name="Child7" class="form-control" type="text">
							<br>
							<input name="Child8" class="form-control" type="text">
							<br>
							<input name="Child9" class="form-control" type="text">
							<br>
							<input name="Child10" class="form-control" type="text">
						</div>
						<div class="col-sm-6">
							<br>
							<label>Date of Birth (mm/dd/yyyy)</label>
                            <input name="Bday1" class="form-control" type="date">
							<br>									
                            <input name="Bday2" class="form-control" type="date">
							<br>											
                            <input name="Bday3" class="form-control" type="date">
							<br>											
                            <input name="Bday4" class="form-control" type="date">
							<br>											
                            <input name="Bday5" class="form-control" type="date">
							<br>
							<input name="Bday6" class="form-control" type="date">
							<br>											
                            <input name="Bday7" class="form-control" type="date">
							<br>
							<input name="Bday8" class="form-control" type="date">
							<br>
							<input name="Bday9" class="form-control" type="date">
							<br>
							<input name="Bday10" class="form-control" type="date">
						</div>
					</div>
                	<br>
                	<input name="eID" value="<?php echo $eID; ?>" class="form-control" type="hidden">
				 	<button type="submit" class="btn btn-info" name="submit">Next 2/8</button>
				</form>
                </div>                            
            </div>                              
        </div>
    </div>
</div>
<?php
	if(isset($_POST['submit'])){
		$eID = $_POST['eID'];	
		$sql = "SELECT `id`, `bioID`, `surname`, `firstname`, `middlename`, `namext` FROM `personalinfo` WHERE `bioID`=$eID";
		$query = mysql_query($sql);	
		while($row = mysql_fetch_assoc($query)){
			$id = $row['id'];
			$eID = $row['bioID'];
			$fullname = $row['surname'].", ".$row['firstname']." ".$row['namext']." ".$row['middlename'];
			$newName = mysql_real_escape_string($fullname);
		}
		//Family Background
		$SpouseSurname = mysql_real_escape_string(utf8_decode($_POST['SpouseSurname']));
		$SpouseFirstname = mysql_real_escape_string(utf8_decode($_POST['SpouseFirstname']));
		$SpouseMiddlename = mysql_real_escape_string(utf8_decode($_POST['SpouseMiddlename']));
		$SpouseOccupation = mysql_real_escape_string(utf8_decode($_POST['SpouseOccupation']));
		$SpouseEmployer = mysql_real_escape_string(utf8_decode($_POST['SpouseEmployer']));
		$SpouseAddress = mysql_real_escape_string(utf8_decode($_POST['SpouseAddress']));
		$SpouseTelephone = mysql_real_escape_string(utf8_decode($_POST['SpouseTelephone']));
		$FatherSurname = mysql_real_escape_string(utf8_decode($_POST['FatherSurname']));
		$FatherFirstname = mysql_real_escape_string(utf8_decode($_POST['FatherFirstname']));
		$FatherMiddlename = mysql_real_escape_string(utf8_decode($_POST['FatherMiddlename']));
		$MotherSurname = mysql_real_escape_string(utf8_decode($_POST['MotherSurname']));
		$MotherFirstname = mysql_real_escape_string(utf8_decode($_POST['MotherFirstname']));
		$MotherMiddlename = mysql_real_escape_string(utf8_decode($_POST['MotherMiddlename']));
		$Child1 = mysql_real_escape_string(utf8_decode($_POST['Child1']));
		$Child2 = mysql_real_escape_string(utf8_decode($_POST['Child2']));
		$Child3 = mysql_real_escape_string(utf8_decode($_POST['Child3']));
		$Child4 = mysql_real_escape_string(utf8_decode($_POST['Child4']));
		$Child5 = mysql_real_escape_string(utf8_decode($_POST['Child5']));
		$Child6 = mysql_real_escape_string(utf8_decode($_POST['Child6']));
		$Child7 = mysql_real_escape_string(utf8_decode($_POST['Child7']));
		$Child8 = mysql_real_escape_string(utf8_decode($_POST['Child8']));
		$Child9 = mysql_real_escape_string(utf8_decode($_POST['Child9']));
		$Child10 = mysql_real_escape_string(utf8_decode($_POST['Child10']));
		$Bday1 = $_POST['Bday1'];
		$Bday2 = $_POST['Bday2'];
		$Bday3 = $_POST['Bday3'];
		$Bday4 = $_POST['Bday4'];
		$Bday5 = $_POST['Bday5'];
		$Bday6 = $_POST['Bday6'];
		$Bday7 = $_POST['Bday7'];
		$Bday8 = $_POST['Bday8'];
		$Bday9 = $_POST['Bday9'];
		$Bday10 = $_POST['Bday10'];

		$sql = "INSERT INTO `familybg`  (`id`, `bioID`, `fullname`, `ssurname`, `sfirstname`, `smiddlename`, `soccupation`, `semployername`, `sbusaddress`, `stelephone`, `child1`, `child2`, `child3`, `child4`, `child5`, `child6`, `child7`, `child8`, `child9`, `child10`, `c1bday`, `c2bday`, `c3bday`, `c4bday`, `c5bday`, `c6bday`, `c7bday`, `c8bday`, `c9bday`, `c10bday`,`fsurname`, `ffirstname`, `fmiddlename`, `msurname`, `mfirstname`, `mmiddlename`)
							VALUES ('$id', '$eID', '$newName', '$SpouseSurname', '$SpouseFirstname', '$SpouseMiddlename', '$SpouseOccupation', '$SpouseEmployer', '$SpouseAddress', '$SpouseTelephone', '$Child1', '$Child2', '$Child3', '$Child4', '$Child5', '$Child6', '$Child7', '$Child8', '$Child9', '$Child10', '$Bday1', '$Bday2', '$Bday3', '$Bday4', '$Bday5', '$Bday6', '$Bday7', '$Bday8', '$Bday9', '$Bday10', '$FatherSurname', '$FatherFirstname', '$FatherMiddlename', '$MotherSurname', '$MotherFirstname', '$MotherMiddlename')";
		$query = mysql_query($sql);
			echo "<script type = \"text/javascript\">
									alert(\"Success!.\");
									window.location = \"index.php?page=newpds2&eID=$eID\"
								  </script>";
	}
?>

