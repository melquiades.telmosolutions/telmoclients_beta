<!DOCTYPE html>
<html>
  <head>
    <title>Bootstrap Admin Theme v3</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="media/css/bootstrap.css">
    <!-- Bootstrap -->
   
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.html">Bootstrap Admin Theme</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
	                <div class="col-lg-12">
	                  <div class="input-group form">
	                       <input type="text" class="form-control" placeholder="Search...">
	                       <span class="input-group-btn">
	                         <button class="btn btn-primary" type="button">Search</button>
	                       </span>
	                  </div>
	                </div>
	              </div>
	           </div>
	           <div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="profile.html">Profile</a></li>
	                          <li><a href="login.html">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-2">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li><a href="index.html"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
                    <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li class="current"><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                    <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Pages
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="login.html">Login</a></li>
                            <li><a href="signup.html">Signup</a></li>
                        </ul>
                    </li>
                </ul>
             </div>
		  </div>
		  <div class="col-md-10">

		  	<div class="row">
  				
		  				

  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title">Bootstrap dataTables</div>
				</div>
  				<div class="panel-body">
  					<?php
  					require_once "db.php";
				$select = "SELECT * FROM `educbg`";
				
				$result = mysql_query($select);
			?>
  					<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="dataTables">
						<thead>
			<tr>
				<th>BIO ID</th>
                <th>Fullname</th>
                <th>Highschool</th>
                <th>Yr. Grad. (hs)</th>
                <th>College</th>
                <th>Course</th>
				<th>Yr. Grad. (college)</th>
                <th>Graduate Studies</th>
                <th>Graduate Course</th>
				<th>Yr. Grad. (graduate)</th>
				<th>Action</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th>BIO ID</th>
                <th>Fullname</th>
                <th>Highschool</th>
                <th>Yr. Grad. (hs)</th>
                <th>College</th>
                <th>Course</th>
				<th>Yr. Grad. (college)</th>
                <th>Graduate Studies</th>
                <th>Graduate Course</th>
				<th>Yr. Grad. (graduate)</th>
				<th>Action</th>
			</tr>
		</tfoot>
						<tbody>
							<?php
			
	 	while ($row = mysql_fetch_assoc($result)){
		$id = $row['id'];	
		$gcourse = $row['gdcschool'];				
		echo "<tr>
					<td>".$row['employeeID']."</td>
                    <td>".utf8_encode($row['fullname'])."</td>
                    <td>".utf8_encode($row['sschool'])."</td>
                    <td>".$row['syeargrad']."</td>
                    <td>".utf8_encode($row['cschool'])."</td>
					<td>".utf8_encode($row['cdcschool'])."</td>
					<td>".$row['cyeargrad']."</td>
					<td>".utf8_encode($row['gschool'])."</td>
					<td>".utf8_encode($row['gdcschool'])."</td>
					<td>".$row['gyeargrad']."</td>
                    <td><a href = 'index.php?page=pdsAction&id=$id&p=reduc' class='btn btn-primary btn-sm'>Edit</a></td>
                </tr>
         	";
				}
		?>
						</tbody>
					</table>
  				</div>
  			</div>



		  </div>
		</div>
    </div>

    <footer>
         <div class="container">
         
            <div class="copy text-center">
               Copyright 2014 <a href='#'>Website</a>
            </div>
            
         </div>
      </footer>

      <link href="vendors/datatables/dataTables.bootstrap.css" rel="stylesheet" media="screen">

    <script type="text/javascript" language="javascript" src="media/js/jquery-1.12.3.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/js/dataTables.bootstrap4.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/buttons/js/dataTables.buttons.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/buttons/js/buttons.bootstrap4.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/js/excel.jszip.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/js/pdfmake.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/js/vfs.fonts.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/buttons/js/buttons.html5.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/buttons/js/buttons.print.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/buttons/js/buttons.colVis.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/resources/syntax/shCore.js">
	</script>
	<script type="text/javascript" language="javascript" src="media/resources/demo.js">
	</script>
	
	<script type="text/javascript" language="javascript" class="init">
		$(document).ready(function() {
			var table = $('#dataTables').DataTable( {
				lengthChange: false,
				buttons: [  {
								extend:'copy'
							},
							{
								extend:'excel'
							}, 
							{
								extend:'pdf'
							},
							{
				                extend: 'print',
				                customize: function ( win ) {
				                    $(win.document.body)
				                        .css( 'font-size', '10pt' )
				                        .prepend(
				                            '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
				                        );
				 
				                    $(win.document.body).find( 'table' )
				                        .addClass( 'compact' )
				                        .css( 'font-size', 'inherit' );
				                }
		            		}, 
							{
								extend:'colvis'
							} 
						  ]
			} );

			table.buttons().container()
				.appendTo( '#dataTables_wrapper .col-md-6:eq(0)' );
		} );	
	</script>
  </body>
</html>