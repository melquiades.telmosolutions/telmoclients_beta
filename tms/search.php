<div class="content-box-large">
	<div class="panel-heading">
		<center><h1 class="page-head-line"><p class="text-gray-dark">SEARCH</p></h1>
		<hr>
		 <form action="" method="POST">
			<input type="text" placeholder="Search" name="search" class="form-group" style="width: 800px; padding: 5px; border-width: 4px"">
			<input type='submit' name='submit' value='Go Search' class='btn-info' style="width: 80px; padding: 3px; border-width: 3px"></center>
		</form>
	</div>

	<div class="panel-body">
<?php
require_once "db.php";
include_once "userLevel.php";

if(isset($_POST['submit'])){
	$search = $_POST['search'];
	echo "You searched for <b>".ucwords($search)."</b>";

	$search_exploded = explode (" ", $search);
						 
	$x = "";
	$construct = ""; 
	$construct1 = ""; 
	$construct2 = "";
	$construct3 = "";  
							
	foreach($search_exploded as $search_each){
	$x++;
		if($x==1){
			$select = mysql_query("SELECT * FROM `personalinfo` WHERE concat(`bioID`, `status`, `office`, `surname`, `firstname`, `namext`, `sex`, `civilstatus`, `blood`, `cellphone`) LIKE '%$search_each%'");
			$fetch = mysql_fetch_array($select);

			$select1 = mysql_query("SELECT * FROM `otherinfo` WHERE concat(`type`, `specify`) LIKE '%$search_each%'");
			$fetch1 = mysql_fetch_array($select1);

			$select2 = mysql_query("SELECT * FROM `workexp` WHERE `position` LIKE '%$search_each%'");
			$fetch2 = mysql_fetch_array($select2);

			$select3 = mysql_query("SELECT * FROM `educbg` WHERE concat(`cdcschool`, `gdcschool`) LIKE '%$search_each%'");
			$fetch3 = mysql_fetch_array($select3);


			$construct .="concat(`bioID`, `status`, `office`, `surname`, `firstname`, `namext`, `sex`, `civilstatus`, `blood`, `cellphone`) LIKE '%$search_each%' ";
			$construct1 .="concat(`type`, `specify`) LIKE '%$search_each%'";
			$construct2 .="`position` LIKE '%$search_each%' ";
			$construct3 .="concat(`cdcschool`, `gdcschool`) LIKE '%$search_each%' ";
			
		}else{
			$construct .="AND concat(`bioID`, `status`, `office`, `surname`, `firstname`, `namext`, `sex`, `civilstatus`, `blood`, `cellphone`) LIKE '%$search_each%'";
		}
	}
						
	if($fetch){												
		$constructs = "SELECT * FROM `personalinfo` WHERE $construct";
	}elseif($fetch2){
		$constructs = "SELECT * FROM `workexp` WHERE $construct2";
	}elseif($fetch3){
		$constructs = "SELECT * FROM `educbg` WHERE $construct3";
	}elseif($fetch1){
		$constructs = "SELECT * FROM `otherinfo` WHERE $construct1";
	}else{
		$constructs = "SELECT * FROM `personalinfo` WHERE $construct";
	}
	$run = mysql_query($constructs);
	$foundnum = mysql_num_rows($run);						
	if ($foundnum==0){
		echo "<br>Sorry, there are no matching result for <b>$search</b>.</br></br>1. 
		Try more general words. for example: If you want to search 'office'
		then use general keyword like 'HRMDO' 'TOMECO'</br>2. Try different words with similar
		 meaning</br>3. Please check your spelling";
	}else{
		// echo "<br>About ".$foundnum." results<br><br>";
?>
<br>
<table border="0" cellspacing="5" cellpadding="5">
 	<tbody>
	 	<tr>
			<td>Minimum age:</td>
			<td><input type="text" id="min" name="min"></td>
		</tr>
		<tr>
			<td>Maximum age:</td>
			<td><input type="text" id="max" name="max"></td>
		</tr>
	</tbody>
</table>
<br>
<?php
		if($fetch){
		?>
			<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="search">
				<thead>
		            <tr>
		                <th>BIO ID</th>
		                <th class="select-filter">Status</th>
		                <th class="select-filter">Office</th>
		                <th>Fullname</th>
		                <th>Age</th>
		                <th class="select-filter">Sex</th>
		                <th class="select-filter">Civil Status</th>
						<th>Blood</th>
						<th>Contact</th>
					<?php if($user_level != 3){ ?>
						<th>Action</th> <?php } ?>
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		            </tr>
		        </tfoot>
				<tbody>
		<?php 	
			$getquery = mysql_query("SELECT * FROM `personalinfo` WHERE $construct");				  
				while($runrows = mysql_fetch_assoc($getquery)){
				$id = $runrows ['id'];
				$bioID = $runrows ['bioID'];
				$status = $runrows ['status'];
				$office = $runrows ['office'];
				$fullname = utf8_encode($runrows['surname'].", ".$runrows['firstname']." ".$runrows['namext']." ".$runrows['middlename']);
				$age = $runrows ['age'];
				$sex = $runrows ['sex'];
				$civilstatus = $runrows ['civilstatus'];
				$blood = $runrows ['blood'];
				$cellphone = $runrows ['cellphone'];
				echo "
					<tr>
						<td>".$bioID."</td>
						<td>".$status."</td>
						<td>".strtoupper($office)."</td>
						<td>".ucwords($fullname)."</td>
						<td>".$age."</td>
						<td>".ucwords($sex)."</td>
						<td>".ucwords($civilstatus)."</td>
						<td>".strtoupper($blood)."</td>
						<td>".$cellphone."</td>";
						if($user_level != 3){	
						echo "<td class='nav'><a href = 'index.php?page=pdsAction&id=$id&p=search' class='btn btn-info' style='width: 40px; padding: 1px; border-width: 4px'>Info</a></td>";
							}
				echo "</tr>";
				}
		}elseif($fetch2){
		?>
			<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="search">
				<thead>
		            <tr>
		                <th>BIO ID</th>
		                <th class="select-filter">Status</th>
		                <th class="select-filter">Office</th>
		                <th>Fullname</th>
		                <th>Age</th>
		                <th class="select-filter">Sex</th>
						<th>Work Experience: Position</th>
						<th>Dept/Agency/Office/Company</th>
						<th>Contact</th>
					<?php if($user_level != 3){ ?>
						<th>Action</th> <?php } ?>
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		            </tr>
		        </tfoot>
				<tbody>
		<?php
			$selectID = mysql_query("SELECT * FROM `workexp` WHERE $construct2");
			while($runID = mysql_fetch_assoc($selectID)){
				$getID = $runID['id'];
				$get = mysql_query("SELECT * FROM `personalinfo` WHERE `id` = '$getID'");
				while($rows = mysql_fetch_assoc($get)){
					$id = $rows['id'];
					$bioID = $rows ['bioID'];
					$status = $rows ['status'];
					$office = $rows ['office'];
					$fullname = utf8_encode($rows['surname'].", ".$rows['firstname']." ".$rows['namext']." ".$rows['middlename']);
					$age = $rows ['age'];
					$sex = $rows ['sex'];
					$cellphone = $rows ['cellphone'];
					echo "
						<tr>
						<td>".$bioID."</td>
						<td>".$status."</td>
						<td>".strtoupper($office)."</td>
						<td>".ucwords($fullname)."</td>
						<td>".$age."</td>
						<td>".ucwords($sex)."</td>
						<td>"; 
						$getquery = mysql_query("SELECT DISTINCT `position` FROM `workexp` WHERE $construct2 AND `id` = '$id'");
							while($runrows = mysql_fetch_assoc($getquery)){
								$position = $runrows ['position'];
								echo ucwords($position).",<br>";
							} 
									echo "</td>
						<td>";	
						$getquery1 = mysql_query("SELECT DISTINCT `doac` FROM `workexp` WHERE $construct2 AND `id` = '$id'");
							while($runrows1 = mysql_fetch_assoc($getquery1)){
								$doac = $runrows1 ['doac'];
								echo ucwords($doac).",<br>";
							} 
									echo "</td>
						<td>".$cellphone."</td>";
						if($user_level != 3){	
						echo "<td class='nav'><a href = 'index.php?page=pdsAction&id=$id&p=search' class='btn btn-info' style='width: 40px; padding: 1px; border-width: 4px'>Info</a></td>";
							}
					echo "</tr>";
					}
				}
		}elseif($fetch3){
		?>
			<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="search">
				<thead>
		            <tr>
		                <th>BIO ID</th>
		                <th class="select-filter">Status</th>
		                <th class="select-filter">Office</th>
		                <th>Fullname</th>
		                <th>Age</th>
		                <th class="select-filter">Sex</th>
						<th>College Degree</th>
						<th>Graduate Studies</th>
						<th>Contact</th>
					<?php if($user_level != 3){ ?>
						<th>Action</th> <?php } ?>
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		            </tr>
		        </tfoot>
				<tbody>
		<?php
			$selectID = mysql_query("SELECT * FROM `educbg` WHERE $construct3");
			while($runID = mysql_fetch_assoc($selectID)){
				$getID = $runID['id'];
				$get = mysql_query("SELECT * FROM `personalinfo` WHERE `id` = '$getID'");
				while($rows = mysql_fetch_assoc($get)){
					$id = $rows['id'];
					$bioID = $rows ['bioID'];
					$status = $rows ['status'];
					$office = $rows ['office'];
					$fullname = utf8_encode($rows['surname'].", ".$rows['firstname']." ".$rows['namext']." ".$rows['middlename']);
					$age = $rows ['age'];
					$sex = $rows ['sex'];
					$cellphone = $rows ['cellphone'];	
					
					echo "
						<tr>
						<td>".$bioID."</td>
						<td>".$status."</td>
						<td>".strtoupper($office)."</td>
						<td>".ucwords($fullname)."</td>
						<td>".$age."</td>
						<td>".ucwords($sex)."</td>
						<td>"; 
						$getquery = mysql_query("SELECT * FROM `educbg` WHERE $construct3 AND `id` = '$id'");
							while($runrows = mysql_fetch_assoc($getquery)){
								$cdcschool = $runrows ['cdcschool'];
								echo ucwords($cdcschool).",<br>";
							} 
									echo "</td>
						<td>";	
						$getquery1 = mysql_query("SELECT * FROM `educbg` WHERE $construct3 AND `id` = '$id'");
							while($runrows1 = mysql_fetch_assoc($getquery1)){
								$gdcschool = $runrows1 ['gdcschool'];
								echo ucwords($gdcschool).",<br>";
							} 
									echo "</td>
						<td>".$cellphone."</td>";
						if($user_level != 3){	
						echo "<td class='nav'><a href = 'index.php?page=pdsAction&id=$id&p=search' class='btn btn-info' style='width: 40px; padding: 1px; border-width: 4px'>Info</a></td>";
							}
					echo "</tr>";
					}
				}
		}elseif($fetch1){
		?>
			<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="search">
				<thead>
		            <tr>
		                <th>BIO ID</th>
		                <th class="select-filter">Status</th>
		                <th class="select-filter">Office</th>
		                <th>Fullname</th>
		                <th>Age</th>
		                <th class="select-filter">Sex</th>
						<th>Skill/Membership/Organization</th>
						<th>Contact</th>
					<?php if($user_level != 3){ ?>
						<th>Action</th> <?php } ?>
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		            </tr>
		        </tfoot>
				<tbody>
		<?php
			$selectID = mysql_query("SELECT * FROM `otherinfo` WHERE $construct1");
			while($runID = mysql_fetch_assoc($selectID)){
				$getID = $runID['id'];
				$get = mysql_query("SELECT * FROM `personalinfo` WHERE `id` = '$getID'");
				while($rows = mysql_fetch_assoc($get)){
					$id = $rows['id'];
					$bioID = $rows ['bioID'];
					$status = $rows ['status'];
					$office = $rows ['office'];
					$fullname = utf8_encode($rows['surname'].", ".$rows['firstname']." ".$rows['namext']." ".$rows['middlename']);
					$age = $rows ['age'];
					$sex = $rows ['sex'];
					$cellphone = $rows ['cellphone'];	
					$getquery = mysql_query("SELECT * FROM `otherinfo` WHERE $construct1 AND `id` = '$id'");
					echo "
						<tr>
						<td>".$bioID."</td>
						<td>".$status."</td>
						<td>".strtoupper($office)."</td>
						<td>".ucwords($fullname)."</td>
						<td>".$age."</td>
						<td>".ucwords($sex)."</td>
						<td>"; while($runrows = mysql_fetch_assoc($getquery)){
								$type = $runrows ['type'];
								$specify = $runrows ['specify'];
								echo ucwords($specify).",<br>";
								} 
									echo "</td>
						<td>".$cellphone."</td>";
						if($user_level != 3){	
						echo "<td class='nav'><a href = 'index.php?page=pdsAction&id=$id&p=search' class='btn btn-info' style='width: 40px; padding: 1px; border-width: 4px'>Info</a></td>";
							}
					echo "</tr>";
					}
				}
		}
	}

?>
			</tbody>
		</table>
<?php
}else{
?>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
<?php
}
?>
	</div>
</div>

