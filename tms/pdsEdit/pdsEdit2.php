<?php
$id = $_GET['id'];
$select = mysql_query("SELECT * FROM `educbg` WHERE `id` = '$id'");
?>
<div class="content-box-large">
	<div class="panel-heading">
		<center><h1 class="page-head-line"><p class="text-gray-dark">PERSONAL DATA SHEET</h1></center>
		<hr>
		<div class="panel-options">
<?php
	$p = $_GET['p'];
		if($p=='personalinfo' OR $p=='fambg' OR $p=='reduc' OR $p=='cse' OR $p=='workexp' OR $p=='volwork' OR $p=='trainings' OR $p=='skills' OR $p=='recog' OR $p=='orgmem' OR $p=='search'){
			echo '<a href="index.php?page=pdsAction&id='.$id.'&p='.$p.'"class="fa arrow"><< BACK</a>';
		}else if($p=='pds'){
			echo '<a href="index.php?page='.$p.'"class="fa arrow"><< BACK</a>';
		}
?>
		</div>
	</div>
	<form action="pdsEditEd.php" method="post">	
	<div class="panel-body">
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th colspan="9">III. EDUCATIONAL BACKGROUND</th>
				</tr>
				<tr>
                    <th width='5%'><p align='center'>LEVEL</th>
					<th width='27%'><p align='center'>NAME OF SCHOOL</th>
					<th width='20%'><p align='center'>DEGREE COURSE</th>
					<th width='5%'><p align='center'>YEAR GRADUATED</th>
					<th width='15%'><p align='center'>HIGHEST GRADE / LEVEL / UNITS EARNED</th>
					<th width='9%'><p align='center'>From Year</th>
					<th width='9%'><p align='center'>To Year</th>
					<th width='15%'><p align='center'>SCHOLARSHIP / ACADEMIC HONORS RECEIVED</th></p>
                </tr>
			</thead>
<?php	
	while ($row = mysql_fetch_assoc($select)){	
		$eschool = strtolower($row['eschool']);
		$vschool = strtolower($row['vschool']);
		$ehgrade = strtolower($row['ehgrade']);
		$eacad = strtolower($row['eacad']);
		$sschool = strtolower($row['sschool']);
		$shgrade = strtolower($row['shgrade']);
		$sacad = strtolower($row['sacad']);
		$vhgrade = strtolower($row['vhgrade']);
		$vacad = strtolower($row['vacad']);
		$cschool = strtolower($row['cschool']);
		$cdcschool = strtolower($row['cdcschool']);
		$chgrade = strtolower($row['chgrade']);
		$cacad = strtolower($row['cacad']);
		$gschool = strtolower($row['gschool']);
		$gdcschool = strtolower($row['gdcschool']);
		$ghgrade = strtolower($row['ghgrade']);
		$gacad = strtolower($row['gacad']);
		$g2school = strtolower($row['g2school']);
		$g2dcschool = strtolower($row['g2dcschool']);
		$g2hgrade = strtolower($row['g2hgrade']);
		$g2acad = strtolower($row['g2acad']);
		echo "
             <tbody>
                <tr>
                    <td >Elementary</td>
                    <td ><input name='eschool' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($eschool)), ENT_QUOTES)."' type='text'></td>
					<td></td>
					<td><input name='eyeargrad' class='form-control' value='".htmlspecialchars(utf8_encode($row['eyeargrad']), ENT_QUOTES)."' type='text'></td>
					<td><input name='ehgrade' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($ehgrade)), ENT_QUOTES)."' type='text'></td>
					<td><input name='efyear' class='form-control' value='".htmlspecialchars(utf8_encode($row['efyear']), ENT_QUOTES)."' type='text'></td>
					<td><input name='etyear' class='form-control' value='" .htmlspecialchars(utf8_encode($row['etyear']), ENT_QUOTES)."' type='text'></td>
					<td><input name='eacad' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($eacad)), ENT_QUOTES)."' type='text'></td>
				</tr>
					<td>Secondary</td>
					<td><input name='sschool' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($sschool)), ENT_QUOTES)."' type='text'></td>
					<td></td>
					<td><input name='syeargrad' class='form-control' value='".htmlspecialchars(utf8_encode($row['syeargrad']), ENT_QUOTES)."' type='text'></td>
					<td><input name='shgrade' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($shgrade)), ENT_QUOTES)."' type='text'></td>
					<td><input name='sfyear' class='form-control' value='".htmlspecialchars(utf8_encode($row['sfyear']), ENT_QUOTES)."' type='text'></td>
					<td><input name='styear' class='form-control' value='".htmlspecialchars(utf8_encode($row['styear']), ENT_QUOTES)."' type='text'></td>
					<td><input name='sacad' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($sacad)), ENT_QUOTES)."' type='text'></td>
				</tr>
				<tr>
					<td>Vocational / Trade Course</td>
                    <td><input name='vschool' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($vschool)), ENT_QUOTES)."' type='text'></td>
					<td><input name='vdcschool' class='form-control' value='".htmlspecialchars(utf8_encode($row['vdcschool']), ENT_QUOTES)."' type='text'></td>
					<td><input name='vyeargrad' class='form-control' value='".htmlspecialchars(utf8_encode($row['vyeargrad']), ENT_QUOTES)."' type='text'></td>
					<td><input name='vhgrade' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($vhgrade)), ENT_QUOTES)."' type='text'></td>
					<td><input name='vfyear' class='form-control' value='".htmlspecialchars(utf8_encode($row['vfyear']), ENT_QUOTES)."' type='text'></td>
					<td><input name='vtyear' class='form-control' value='".htmlspecialchars(utf8_encode($row['vtyear']), ENT_QUOTES)."' type='text'></td>
					<td><input name='vacad' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($vacad)), ENT_QUOTES)."' type='text'></td>
				</tr>
				<tr>
					<td>College</td>
					<td><input name='cschool' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($cschool)), ENT_QUOTES)."' type='text'></td>
					<td><input name='cdcschool' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($cdcschool)), ENT_QUOTES)."' type='text'></td>
					<td><input name='cyeargrad' class='form-control' value='".htmlspecialchars(utf8_encode($row['cyeargrad']), ENT_QUOTES)."' type='text'></td>
					<td><input name='chgrade' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($chgrade)), ENT_QUOTES)."' type='text'></td>
					<td><input name='cfyear' class='form-control' value='".htmlspecialchars(utf8_encode($row['cfyear']), ENT_QUOTES)."' type='text'></td>
					<td><input name='ctyear' class='form-control' value='".htmlspecialchars(utf8_encode($row['ctyear']), ENT_QUOTES)."' type='text'></td>
					<td><input name='cacad' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($cacad)), ENT_QUOTES)."' type='text'></td>
				</tr>
				<tr>
					<td>Graduate Studies</td>
					<td><input name='gschool' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($gschool)), ENT_QUOTES)."' type='text'></td>
					<td><input name='gdcschool' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($gdcschool)), ENT_QUOTES)."' type='text'></td>
					<td><input name='gyeargrad' class='form-control' value='".htmlspecialchars(utf8_encode($row['gyeargrad']), ENT_QUOTES)."' type='text'></td>
					<td><input name='ghgrade' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($ghgrade)), ENT_QUOTES)."' type='text'></td>
					<td><input name='gfyear' class='form-control' value='".htmlspecialchars(utf8_encode($row['gfyear']), ENT_QUOTES)."' type='text'></td>
					<td><input name='gtyear' class='form-control' value='".htmlspecialchars(utf8_encode($row['gtyear']), ENT_QUOTES)."' type='text'></td>
					<td><input name='gacad' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($gacad)), ENT_QUOTES)."' type='text'></td>
				</tr>
				<tr>
					<td>Graduate Studies II</td>
					<td><input name='g2school' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($g2school)), ENT_QUOTES)."' type='text'></td>
					<td><input name='g2dcschool' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($g2dcschool)), ENT_QUOTES)."' type='text'></td>
					<td><input name='g2yeargrad' class='form-control' value='".htmlspecialchars(utf8_encode($row['g2yeargrad']), ENT_QUOTES)."' type='text'></td>
					<td><input name='g2hgrade' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($g2hgrade)), ENT_QUOTES)."' type='text'></td>
					<td><input name='g2fyear' class='form-control' value='".htmlspecialchars(utf8_encode($row['g2fyear']), ENT_QUOTES)."' type='text'></td>
					<td><input name='g2tyear' class='form-control' value='".htmlspecialchars(utf8_encode($row['g2tyear']), ENT_QUOTES)."' type='text'></td>
					<td><input name='g2acad' class='form-control' value='".htmlspecialchars(utf8_encode(ucwords($g2acad)), ENT_QUOTES)."' type='text'></td>
				</tr>
					<input name='id' class='form-control' value='".$id."' type='hidden'>
					<input name='p' class='form-control' value='".$p."' type='hidden'>
			</tbody>";
	}
?>
	</table>
	<button type="submit" class="btn btn-info" name="updateEb">Update</button>
	</form>
	</div>
</div>