<div class="content-box-large">
	<div class="panel-heading">
		<center><h1 class="page-head-line"><p class="text-gray-dark">Educational Background</p></h1></center>
		<hr>
		 	<table class="table table-striped table-bordered table-hover">	
				<thead>
					<tr>
						<th><center><a href = "index.php?page=Rpersonalinfo" class="text-gray-dark">Personal Info</a></center></th>
						<th><center><a href = "index.php?page=RfamBg" class="text-gray-dark">Family Background</a></center></th>
						<th><center><a class="current" href = "index.php?page=Reduc" class="text-gray-dark">Education</a></center></th>
						<th><center><a href = "index.php?page=Rcse" class="text-gray-dark">CSE</a></center></th>
						<th><center><a href = "index.php?page=RworkExp" class="text-gray-dark">Work Exp</a></center></th>
						<th><center><a href = "index.php?page=RvoluntWork" class="text-gray-dark">Volunt Work</a></center></th>
						<th><center><a href = "index.php?page=Rtrainings" class="text-gray-dark">Trainings</a></center></th>
						<th><center><a href = "index.php?page=Rskills" class="text-gray-dark">Skills/Hobbies</a></center></th>
						<th><center><a href = "index.php?page=Rrecog" class="text-gray-dark">Recognition</a></center></th>
						<th><center><a href = "index.php?page=RorgMem" class="text-gray-dark">Org. Membership</a></center></th>
					</tr>
				</thead>
			</table>
	</div>

	<div class="panel-body">
<?php
	require_once "db.php";
		$select = "SELECT * FROM `educbg`";
				
		$result = mysql_query($select);
?>
	<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="dataTables">
		<thead>
			<tr>
				<th>BIO ID</th>
                <th>Fullname</th>
                <th>Highschool</th>
                <th>Yr. Grad. (hs)</th>
                <th>College</th>
                <th>Course</th>
				<th>Yr. Grad. (college)</th>
                <th>Graduate Studies</th>
                <th>Graduate Course</th>
				<th>Yr. Grad. (graduate)</th>
				<th>Action</th>
			</tr>
		</thead>
	<tbody>
<?php
	while ($row = mysql_fetch_assoc($result)){
		$id = $row['id'];	
		$gcourse = $row['gdcschool'];				
		echo "<tr>
					<td>".$row['bioID']."</td>
                    <td>".ucwords(utf8_encode($row['fullname']))."</td>
                    <td>".utf8_encode($row['sschool'])."</td>
                    <td>".utf8_encode($row['syeargrad'])."</td>
                    <td>".utf8_encode($row['cschool'])."</td>
					<td>".utf8_encode($row['cdcschool'])."</td>
					<td>".utf8_encode($row['cyeargrad'])."</td>
					<td>".utf8_encode($row['gschool'])."</td>
					<td>".utf8_encode($row['gdcschool'])."</td>
					<td>".utf8_encode($row['gyeargrad'])."</td>
                     <td><a href = 'index.php?page=pdsAction&id=$id&p=reduc' class='btn btn-info btn-sm'>View</a></td>
                </tr>
         	";
	}
?>
		</tbody>
	</table>
	</div>
</div>