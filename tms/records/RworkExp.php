<div class="content-box-large">
	<div class="panel-heading">
		<center><h1 class="page-head-line"><p class="text-gray-dark">Working Experience</p></h1></center>
		<hr>
			<table class="table table-striped table-bordered table-hover">	
				<thead>
					<tr>
						<th><center><a href = "index.php?page=Rpersonalinfo" class="text-gray-dark">Personal Info</a></center></th>
						<th><center><a href = "index.php?page=RfamBg" class="text-gray-dark">Family Background</a></center></th>
						<th><center><a href = "index.php?page=Reduc" class="text-gray-dark">Education</a></center></th>
						<th><center><a href = "index.php?page=Rcse" class="text-gray-dark">CSE</a></center></th>
						<th><center><a class="current" href = "index.php?page=RworkExp" class="text-gray-dark">Work Exp</a></center></th>
						<th><center><a href = "index.php?page=RvoluntWork" class="text-gray-dark">Volunt Work</a></center></th>
						<th><center><a href = "index.php?page=Rtrainings" class="text-gray-dark">Trainings</a></center></th>
						<th><center><a href = "index.php?page=Rskills" class="text-gray-dark">Skills/Hobbies</a></center></th>
						<th><center><a href = "index.php?page=Rrecog" class="text-gray-dark">Recognition</a></center></th>
						<th><center><a href = "index.php?page=RorgMem" class="text-gray-dark">Org. Membership</a></center></th>
					</tr>
				</thead>
			</table>
	</div>

	<div class="panel-body">
<?php
	require_once "db.php";
		$select = "SELECT * FROM `workexp`";
				
		$result = mysql_query($select);
?>
	<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="dataTables">
		<thead>
			<tr>
				<th>BIO ID</th>
                <th>Fullname</th>
                <th>From</th>
                <th>To</th>
                <th>Position</th>
                <th>Dept/Company</th>
				<th>Month Salary</th>
                <th>SG</th>
                <th>Status</th>
				<th>Gov't svc (YES/NO)</th>
				<th>Action</th>
			</tr>
		</thead>
	<tbody>
<?php
	while ($row = mysql_fetch_assoc($result)){
		$id = $row['id'];						
		echo "
            <tr>
				<td>".$row['bioID']."</td>
                <td>".ucwords(utf8_encode($row['fullname']))."</td>
                <td>".$row['fromdate']."</td>
                <td>".$row['todate']."</td>
				<td>".ucwords(utf8_encode($row['position']))."</td>
				<td>".ucwords(utf8_encode($row['doac']))."</td>
				<td>".utf8_encode($row['salary'])."</td>
				<td>".utf8_encode($row['sg'])."</td>
				<td>".ucwords(utf8_encode($row['status']))."</td>
				<td>".ucwords(utf8_encode($row['govservice']))."</td>
                <td><a href = 'index.php?page=pdsAction&id=$id&p=workexp' class='btn btn-info btn-sm'>View</a></td>
            </tr>";
	}
	if (!$result) {
		die(mysql_error());
	}
?>
		</tbody>
	</table>
	</div>
</div>