<div class="content-box-large">
	<div class="panel-heading">
		<center><h1 class="page-head-line"><p class="text-gray-dark">Personal Information</p></h1></center>
		<hr>
		 	<table class="table table-striped table-bordered table-hover">	
				<thead>
					<tr>
						<th><center><a class="current" href = "index.php?page=Rpersonalinfo" class="text-gray-dark">Personal Info</a></center></th>
						<th><center><a href = "index.php?page=RfamBg" class="text-gray-dark">Family Background</a></center></th>
						<th><center><a href = "index.php?page=Reduc" class="text-gray-dark">Education</a></center></th>
						<th><center><a href = "index.php?page=Rcse" class="text-gray-dark">CSE</a></center></th>
						<th><center><a href = "index.php?page=RworkExp" class="text-gray-dark">Work Exp</a></center></th>
						<th><center><a href = "index.php?page=RvoluntWork" class="text-gray-dark">Volunt Work</a></center></th>
						<th><center><a href = "index.php?page=Rtrainings" class="text-gray-dark">Trainings</a></center></th>
						<th><center><a href = "index.php?page=Rskills" class="text-gray-dark">Skills/Hobbies</a></center></th>
						<th><center><a href = "index.php?page=Rrecog" class="text-gray-dark">Recognition</a></center></th>
						<th><center><a href = "index.php?page=RorgMem" class="text-gray-dark">Org. Membership</a></center></th>
					</tr>
				</thead>
			</table>
	</div>

	<div class="panel-body">
<?php
	require_once "db.php";
	$select = "SELECT * FROM `personalinfo`";
			
	$result = mysql_query($select);
?>
	<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="dataTables">
		<thead>
            <tr>
				<th>Employee ID</th>
                <th>BIO ID</th>
                <th>Status</th>
                <th>Office</th>
                <th>Fullname</th>
                <th>Birthdate</th>
                <th>Age</th>
                <th>Sex</th>
                <th>Civil Status</th>
				<th>Blood</th>
				<th>Action</th>
            </tr>
        </thead>
		<tbody>
<?php
 	$output = '';
	while ($row = mysql_fetch_assoc($result)){
		$id = $row['id'];
		$bioID = $row['bioID'];
		// $password = md5($bioID);
		$office = strtoupper($row['office']);
		$status = $row['status'];
		$fullname = strtolower($row['surname'].", ".$row['firstname']." ".$row['namext']." ".$row['middlename']);
		$newName = utf8_encode(ucwords($fullname));
		$output .= "
                <tr>
                    <td>".$row['employeeID']."</td>
					<td>".$row['bioID']."</td>
                    <td>".$row['status']."</td>
                    <td>".strtoupper($row['office'])."</td>
                    <td>".$newName."</td>                         
                    <td>".date("F d, Y",strtotime($row['dobirth']))."</td>									
                    <td>".$age=date("Y-m-d")-$row['dobirth']."</td>
                    <td>".$row['sex']."</td>
					<td>".$row['civilstatus']."</td>
                    <td>".utf8_encode(strtoupper($row['blood']))."</td>
					<td><a href = 'index.php?page=pdsAction&id=$id&p=personalinfo' class='btn btn-info btn-sm'>View</a></td>
                </tr>
                ";
         //        $insert = mysql_query("INSERT INTO `users` (`id`, `idx`, `bioID`, `name`, `username`, `password`, `user_level`, `user_type`) 
									// VALUES (NULL, '$id', '$bioID', '$newName', '', '$password', '2', 'a')");
         //        if($insert){
         //        	echo "SUccess";
         //        }else{
         //        	echo "error";
         //        }

	}
	echo $output;
?>
		</tbody>
	</table>
	</div>
</div>

