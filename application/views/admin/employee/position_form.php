<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s mbot10">
          <div class="panel-body _buttons">
            <button type="button" class="btn btn-info modal-md" onclick="add_id()" data-target="#add0" data-toggle="modal">
              <?php echo _l('new_position');?>
            </button>
            <div class="visible-xs">
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div id="add0" class="modal fade" role="dialog">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <strong><?php echo _l('add_position');?></strong>
              </div>
              <?php echo (isset($position) ? form_open(admin_url('employee/update_pos/'.$position->pos_id),array('id'=>'position_form')) : form_open(admin_url('employee/create_position'),array('id'=>'position_form'))) ; ?>
                <div class="modal-body">
                  <div class="form-group row">
                    <div class="col-md-12">
                      <?php $position_name_lbl = "<span style='color:red;'>*</span> " . _l('position_name'); ?>
                      <?php $value = (isset($position) ? $position->position_name : ''); ?>
                      <?php echo render_input('position_name', $position_name_lbl, $value, 'text', array('required'=>true), '', '', 'input_val_n'); ?>
                      <?php $value = (isset($position) ? $position->position_details : ''); ?>
                      <?php echo render_input('position_details', _l('position_details'), $value, 'text', '', '', '', 'input_val'); ?>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="form-group text-right">
                    <button type="close" class="btn btn-primary w-md m-b-5" data-dismiss="modal"><?php echo _l('cancel'); ?></button>
                    <button type="submit" data-form="benefit_form" onclick="submit_p()" class="btn btn-success w-md m-b-5 btn_modal" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo (isset($position) ? _l('update') : _l('add_position')); ?></button>
                  </div>
                </div>
              <?php echo form_close(); ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12" id="small-table">
            <div class="panel_s">
              <div class="panel-body">
                <?php
                  $table_data = array();
                  $_table_data = array(
                    array(
                      'name'=>_l('id'),
                      'th_attrs'=>array('class'=>'toggleable', 'id'=>'th-number')
                    ),
                    array(
                      'name'=>_l('position_name'),
                      'th_attrs'=>array('class'=>'toggleable', 'id'=>'th-position_name')
                    ),
                    array(
                      'name'=>_l('position_details'),
                      'th_attrs'=>array('class'=>'toggleable', 'id'=>'th-position_details')
                    ),
                  );
                  foreach($_table_data as $_t){
                    array_push($table_data,$_t);
                  }

                  $table_data = hooks()->apply_filters('position_table_columns', $table_data);

                  render_datatable($table_data,'position',[],[
                    'data-last-order-identifier' => 'position',
                    'data-default-order'         => get_table_last_order('position'),
                  ]);
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php init_tail(); ?>
<script>
  $(function(){
    var PositionServerParams = {};
    $.each($('._hidden_inputs._filters input'),function(){
      PositionServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
    });
    PositionServerParams['exclude_inactive'] = '[name="exclude_inactive"]:checked';

    var tAPI = initDataTable('.table-position', admin_url+'employee/table', [0], [0], PositionServerParams,<?php echo hooks()->apply_filters('position_table_default_order', json_encode(array(0,'desc'))); ?>);
    $('input[name="exclude_inactive"]').on('change',function(){
      tAPI.ajax.reload();
    });

    var url = window.location.pathname;
    var id = url.substring(url.lastIndexOf('/') + 1);
    if(!empty(id) && $.isNumeric(id)) {
      $('#add0').modal('show');
    }
  });
  function submit_p() {
    var pos_name = $('.input_val_n').val();
    if(!empty(pos_name)) {
      $("#position_form").submit();
    }
  }
  
  function add_id() {
    $('.input_val').val('');
    $('.input_val_n').val('');
    $('.btn_modal').text('Add Position');
    $('#position_form').attr('action', admin_url+'employee/create_position');
  };

</script>
</body>
</html>