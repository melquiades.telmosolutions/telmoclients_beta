<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head();  ?>

<style type="text/css">
  .nav.nav-tabs li {
    padding: 3px;
  }
  .nav.nav-tabs li a{
    padding: 20px;
  }
  .nav.nav-tabs li:not(.active) a:hover {
    background-color: #0000FF;
  }
  .nav.nav-tabs li a {
    background-color: #0000FF;
    color:white;
  }
  .nav.nav-tabs li:not(.active) a {
    /*pointer-events:none;*/
    background-color: #000099;
    color:white;
  }
  .nav.nav-tabs li (.active) a{
    background-color: #0000FF;
  }
  ul li a.a_nav{
    font-size: 19.7px;
  }
  select {
    cursor: pointer;
  }
  select option {
    font-size: 15px;
  }
  .foot-btn {
      background-color: #000099;
      color: white;
  }
</style>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel_s mbot10">
          <div class="panel-body">
            <!-- <div class="container"> -->
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" class="a_nav" href="#home"><?php echo _l('nav_basic_info')?></a></li>
                <li><a data-toggle="tab" class="a_nav" href="#menu5"><?php echo _l('nav_additional_address')?></a></li>
                <li><a data-toggle="tab" class="a_nav" href="#menu6"><?php echo _l('nav_emerg_contct')?></a></li>
                <li><a data-toggle="tab" class="a_nav" href="#menu1"><?php echo _l('nav_positional_info')?></a></li>
                <li><a data-toggle="tab" class="a_nav" href="#menutax"><?php echo _l('tax')?></a></li>
                <li><a data-toggle="tab" class="a_nav" href="#menu2"><?php echo _l('nav_benefits')?></a></li>
                <li><a data-toggle="tab" class="a_nav" href="#menu3"><?php echo _l('nav_supervisor')?></a></li>
              </ul>

              <div class="tab-content">

                <div id="home" class="tab-pane fade in active">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel">
                        <div class="panel-body">
                          <?php if(isset($employees)) { echo form_open_multipart(admin_url('employee/update_emp/'.$employees->emp_id),array('class'=>'emp_form','id'=>'emp_form')); } else { echo form_open_multipart(admin_url('employee/create_employee'),array('class'=>'emp_form','id'=>'emp_form')); }?>
                          <div class="row">
                            <div class="col-sm-6">
                              <span style='color:red;'>*</span> <label for="id_number"><?php echo _l('employee_id_num')?></label>
                              <input type="text" class="form-control" id="id_number" name="id_number" value="<?php echo (isset($employees) ? $employees->emp_id_number : ''); ?>">
                            </div>
                            <div class="col-sm-6">
                              <label for="picture"><?php echo _l('image')?></label>
                              <input type="file" class="form-control" accept="image/*" id="pic" name="picture" onchange="loadFile(event)">
                              <input type="hidden" name="e_picture" id="e_pic"><?php if(isset($employees)) { ?><input type="hidden" name="e_picture_db" value="<?php echo (isset($employees) ? $employees->picture : ''); ?>"> <?php } ?>
                              <small id="fileHelp" class="text-muted"><img <?php if(isset($employees)) { if(empty($employees->picture)) { ?> src="<?php echo base_url();?>assets/images/user-placeholder.jpg" <?php } else { ?> src="<?php echo base_url($employees->picture);?>" <?php } } else { ?> src="<?php echo base_url();?>assets/images/user-placeholder.jpg" <?php } ?> id="output" style="height: 150px;width: 160px" class="img-thumbnail"/>
                              </small>
                            </div>
                          </div> <br>
                          <div class="row">
                            <div class="col-sm-4">
                              <span style='color:red;'>*</span> <label for="first_name"><?php echo _l('nav_first_name'); ?></label>
                              <input type="text" name="first_name" id="first_name" class="form-control" value="<?php echo (isset($employees) ? $employees->first_name : ''); ?>" required>
                            </div>
                            <div class="col-sm-4">
                              <label for="middle_name"><?php echo _l('nav_middle_name'); ?></label>
                              <input type="text" name="middle_name" id="middle_name" class="form-control" value="<?php echo (isset($employees) ? $employees->middle_name : ''); ?>">
                            </div>
                            <div class="col-sm-4">
                              <span style='color:red;'>*</span> <label for="last_name"><?php echo _l('nav_last_name'); ?></label>
                              <input type="text" name="last_name" id="last_name" class="form-control" value="<?php echo (isset($employees) ? $employees->last_name : ''); ?>" required>
                            </div>
                          </div> <br>
                          <div class="row">
                            <div class="col-sm-4">
                              <label for="email"><?php echo _l('nav_email'); ?></label>
                              <input type="email" name="email" id="email" class="form-control" value="<?php echo (isset($employees) ? $employees->email : ''); ?>">
                              <span id="email_v_message"></span>
                            </div>
                            <div class="col-sm-4">
                              <label for="phone"><?php echo _l('nav_mobile'); ?></label>
                              <input type="number" name="phone" id="phone" class="form-control" value="<?php echo (isset($employees) ? $employees->phone : ''); ?>">
                            </div>
                            <div class="col-sm-4">
                              <label for="alt_phone"><?php echo _l('nav_alt_mobile'); ?></label>
                              <input type="number" name="alter_phone" id="alt_phone" class="form-control" value="<?php echo (isset($employees) ? $employees->alter_phone : ''); ?>" >
                            </div>
                          </div> <br>
                          <div class="row">
                            <div class="col-sm-4">
                              <label for="dob"><?php echo _l('nav_dob'); ?></label>
                              <?php $val = (isset($employees) ? date('m-d-Y', strtotime($employees->dob)) : 'mm-dd-yyyy'); ($employees->dob == 0000-00-00 ? $value = 'mm-dd-yyyy' : $value = $val); ?>
                              <input type="text" name="dob" id="dob" class="datepicker form-control" value="<?php echo $value; ?>">
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label for="gender"><?php echo _l('nav_gender'); ?></label>
                                <select name="gender" id="gender" class="form-control selectpicker">
                                  <option value=""></option>
                                  <?php if(isset($employees) && ($employees->gender) == 1) { ?>
                                    <option value="1" selected>Male</option>;
                                    <option value="2">Female</option>;
                                  <?php } elseif(isset($employees) && ($employees->gender) == 2) { ?>
                                    <option value="1">Male</option>;
                                    <option value="2" selected>Female</option>;
                                  <?php } else { ?>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                  <?php } ?>
                                </select>
                                <span id="gend"></span>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="form-group">
                                <label for="marital_status"><?php echo _l('nav_marital_status'); ?></label>
                                <select name="marital_status" id="status" class="form-control selectpicker">
                                  <option value=""></option>
                                  <?php foreach ($marital_status as $row_marital_status) { ?>
                                    <option <?php if(isset($employees) && $employees->marital_status == $row_marital_status['id']) { echo 'selected'; } ?> value="<?php echo $row_marital_status['id']; ?>"><?php echo $row_marital_status['marital_sta']; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-4">
                              <?php $value = (isset($employees) ? $employees->blood_type : ''); ?>
                              <?php echo render_input('blood_type', _l('nav_bloodtype'), $value); ?>
                            </div>
                            <div class="col-sm-4">
                              <label for="no_children"><?php echo _l('nav_nochildren'); ?></label>
                              <input type="number" name="no_children" id="h_children" class="form-control" value="<?php echo (isset($employees) ? $employees->no_children != 0 ? $employees->no_children : '' : ''); ?>">
                            </div>
                            <div class="col-sm-4">
                              <div class="row">
                                <div class="col-sm-6">
                                   <label for="spouse_fname"><?php echo _l('nav_spousefname'); ?></label>
                                   <input type="text" name="spouse_fname" id="h_spousef" class="form-control" value="<?php echo (isset($employees) ? $employees->spouse_fname : ''); ?>">
                                </div>
                                <div class="col-sm-6">
                                  <label for="spouse_lname"><?php echo _l('nav_spouselname'); ?></label>
                                   <input type="text" name="spouse_lname" id="h_spousel" class="form-control" value="<?php echo (isset($employees) ? $employees->spouse_lname : ''); ?>">
                                </div>
                              </div>
                             
                            </div>
                          </div>
                        </div>
                        <div class="panel-footer">
                          <div class="form-group text-center">
                            <a href="<?php echo admin_url('employee/manageemployee') ?>" type="button" class="btn foot-btn"><?php echo _l('cancel'); ?></a>
                             &emsp;<input type="button" class="btn btn-default btnSkip" value="Skip">&emsp;
                            <input type="button" class="btn btnNext foot-btn" onclick="valid_inf()" value="NEXT"> <br><br>
                            &emsp;&emsp;<?php if(isset($employees)) { ?> <button type="submit" data-form="emp_form" onclick="valid_inf5()" class="btn btn-info foot-btn" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('clients_edit_profile_update_btn'); ?></button> <?php } ?>&emsp;
                          </div>
                        </div>
                      </div>
                    </div>  
                  </div>
                </div>

                <div id="menu5" class="tab-pane fade">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-4">
                              <?php $value = (isset($employees) ? $employees->house_no : ''); ?>
                              <?php echo render_input('house_no', _l('nav_houseno'), $value); ?>
                            </div>
                            <div class="col-sm-4">
                              <?php $value = (isset($employees) ? $employees->street_name : ''); ?>
                              <?php echo render_input('street_name', _l('nav_streetname'), $value); ?>
                            </div>
                            <div class="col-sm-4">
                              <?php $value = (isset($employees) ? $employees->subdivision : ''); ?>
                              <?php echo render_input('subdivision', _l('nav_subdivision'), $value); ?>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-4">
                              <?php $value = (isset($employees) ? $employees->barangay : ''); ?>
                              <?php $brgy_lbl = _l('nav_barangay'); ?>
                              <?php echo render_input('barangay', $brgy_lbl, $value, 'text', '', '', '', array('id'=>'barangay')); ?>
                            </div>
                            <div class="col-sm-4">
                              <?php $value = (isset($employees) ? $employees->city : ''); ?>
                              <?php $c_lbl = _l('nav_city'); ?>
                              <?php echo render_input('city', $c_lbl, $value, 'text', '', '', '', array('id'=>'city')); ?>
                            </div>
                            <div class="col-sm-4">
                              <?php $value = (isset($employees) ? $employees->province : ''); ?>
                              <?php echo render_input('province', _l('nav_province'), $value); ?>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-4">
                              <?php $val = (isset($employees) ? $employees->zip : ''); ($employees->zip == 0 ? $value = '' : $value = $val); ?>
                              <?php echo render_input('zip', _l('nav_zip_code'), $value, 'number'); ?>
                            </div>
                          </div>
                        </div>
                        <div class="panel-footer">
                          <div class="form-group text-center">
                            <input type="button" class="btn btnPrevious foot-btn" value="Previous">
                             &emsp;<input type="button" class="btn btn-default btnSkip" value="Skip">&emsp;
                            <input type="button" class="btn btnNext foot-btn" onclick="valid_inf6()" value="NEXT"> <br><br>
                            &emsp;&emsp;&emsp;<?php if(isset($employees)) { ?> <button type="submit" data-form="emp_form" onclick="valid_inf5()" class="btn btn-info foot-btn" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('clients_edit_profile_update_btn'); ?></button> <?php } ?>&emsp;
                         </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="menu6" class="tab-pane fade">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-6">
                              <label for="emrg_contact_person"><?php echo _l('nav_emerg_contact_person'); ?></label>
                              <input type="text" name="emrg_contact_person" id="emrg_contact_person" class="form-control" value="<?php echo (isset($employees) ? $employees->emrg_contact_person : ''); ?>">
                            </div>
                            <div class="col-sm-6">
                              <label for="emerg_contct"><?php echo _l('nav_emerg_contact'); ?></label>
                              <input type="number" name="emerg_contct" id="em_contact" class="form-control" value="<?php echo (isset($employees) ? $employees->emerg_contct : ''); ?>">
                            </div>
                          </div> <br>
                          <div class="row">
                            <div class="col-sm-6">
                              <?php $value = (isset($employees) ? $employees->emgr_contct_relation : ''); ?>
                              <?php echo render_input('emgr_contct_relation', _l('nav_emerg_contact_relation'), $value); ?>
                            </div>
                            <div class="col-sm-6">
                              <?php $value = (isset($employees) ? $employees->alt_em_contct_person : ''); ?>
                              <?php echo render_input('alt_em_contct_person', _l('nav_alt_emerg_contact_person'), $value); ?>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <?php $value = (isset($employees) ? $employees->alt_em_contct : ''); ?>
                              <?php echo render_input('alt_em_contct', _l('nav_alt_emerg_contact'), $value, 'number'); ?>
                            </div>
                            <div class="col-sm-6">
                              <?php $value = (isset($employees) ? $employees->alt_emg_contact_rel : ''); ?>
                              <?php echo render_input('alt_emg_contact_rel', _l('nav_alt_emerg_contact_rel'), $value, 'text'); ?>
                            </div>
                          </div>
                        </div>
                        <div class="panel-footer">
                          <div class="form-group text-center">
                            <input type="button" class="btn btnPrevious foot-btn" value="Previous">
                             &emsp;<input type="button" class="btn btn-default btnSkip" value="Skip">&emsp;
                            <input type="button" class="btn btnNext foot-btn" onclick="valid_inf7()" value="NEXT"> <br><br>
                            &emsp;&emsp;&emsp;<?php if(isset($employees)) { ?> <button type="submit" data-form="emp_form" onclick="valid_inf5()" class="btn btn-info foot-btn" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('clients_edit_profile_update_btn'); ?></button> <?php } ?>&emsp;
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="menu1" class="tab-pane fade">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="dept_id"><?php echo _l('nav_department'); ?></label>
                                <select name="dept_id" id="dept" class="form-control">
                                  <option value="">Nothing Selected</option>
                                  <?php foreach ($department as $row_department) { ?>
                                    <option <?php if(isset($employees) && $employees->dept_id == $row_department['dept_id']) { echo 'selected'; } ?> value="<?php echo $row_department['dept_id']; ?>"><?php echo $row_department['department_name']; ?></option>
                                  <?php } ?>
                                </select>
                                <span id="deptt"></span>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="position_id"><?php echo _l('nav_position'); ?></label>
                                <select name="position_id" id="designation" class="form-control">
                                  <option value="">Nothing Selected</option>
                                  <?php foreach ($position as $row_position) { ?>
                                    <option <?php if(isset($employees) && $employees->position_id == $row_position['pos_id']) { echo 'selected'; } ?> value="<?php echo $row_position['pos_id']; ?>"><?php echo $row_position['position_name']; ?></option>
                                  <?php } ?>
                                </select>
                                <span id="desig"></span>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="duty_type"><?php echo _l('nav_duty_type'); ?></label>
                                <select name="duty_type" class="form-control">
                                  <option value="">Nothing Selected</option>
                                  <?php foreach ($duty_type as $row_duty_type) { ?>
                                    <option <?php if(isset($employees) && $employees->duty_type == $row_duty_type['id']) { echo 'selected'; } ?> value="<?php echo $row_duty_type['id']; ?>"><?php echo $row_duty_type['type_name']; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <label for="hire_date"><?php echo _l('nav_hireddate'); ?></label>
                              <input type="text" name="hire_date" id="hireddate" class="datepicker form-control" value="<?php echo (isset($employees) ? $employees->hire_date == '0000-00-00' ? 'mm-dd-yyyy' : date('m-d-Y', strtotime($employees->hire_date)) : 'mm-dd-yyyy'); ?>">
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <?php $val = (isset($employees) ? date('m-d-Y', strtotime($employees->termination_date)) : ''); ($employees->termination_date == 0000-00-00 ? $value = 'mm-dd-yyyy' : $value = $val); ?>
                              <?php echo render_input('termination_date', _l('nav_terminationdate'), $value, '', '', '', '', 'datepicker'); ?>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="voluntary_termination"><?php echo _l('nav_volunt_termination'); ?></label>
                                <select name="voluntary_termination" class="form-control">
                                  <option value="">Nothing Selected</option>
                                  <?php if(isset($employees) && ($employees->voluntary_termination) == 1) { ?>
                                    <option value="1" selected>Yes</option>;
                                    <option value="2">No</option>;
                                  <?php } elseif(isset($employees) && ($employees->voluntary_termination) == 2) { ?>
                                    <option value="1">Yes</option>;
                                    <option value="2" selected>No</option>;
                                  <?php } else { ?>
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12">
                              <?php $value = (isset($employees) ? $employees->termination_reason : ''); ?>
                              <?php echo render_textarea('termination_reason', _l('nav_terminationreason'), $value); ?>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <?php $val = (isset($employees) ? date('m-d-Y', strtotime($employees->rehire_date)) : ''); ($employees->rehire_date == 0000-00-00 ? $value = 'mm-dd-yyyy' : $value = $val); ?>
                              <?php echo render_input('rehire_date', _l('nav_rehiredate'), $value, '', '', '', '', 'datepicker'); ?>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="rate_type"><?php echo _l('nav_rate_type'); ?></label>
                                <select name="rate_type" id="rate_type" class="form-control">
                                  <option value="">Nothing Selected</option>
                                  <?php if(isset($employees) && ($employees->rate_type) == 1) { ?>
                                    <option value="1" selected>Hourly</option>;
                                    <option value="2">Daily</option>;
                                  <?php } elseif(isset($employees) && ($employees->rate_type) == 2) { ?>
                                    <option value="1">Hourly</option>;
                                    <option value="2" selected>Daily</option>;
                                  <?php } else { ?>
                                    <option value="1">Hourly</option>
                                    <option value="2" selected>Daily</option>
                                  <?php } ?>
                                </select>
                                <span id="rat_tp"></span>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <label for="rate"><?php echo _l('rate'); ?></label>
                              <?php $val = (isset($employees) ? $employees->rate . '.00' : ''); ($employees->rate == 0 ? $value = '' : $value = $val); ?>
                              <input type="number" name="rate" id="rate" class="form-control" value="<?php echo $value == '' ? '325' : $value ?>" placeholder=".00">
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="skill_index"><?php echo _l('nav_pay_frequency'); ?></label>
                                <input type="number" name="skill_index" id="skill_index" class="form-control">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="panel-footer">
                          <div class="form-group text-right">
                            <div class="form-group text-center">
                              <input type="button" class="btn btnPrevious foot-btn" value="Previous">
                               &emsp;<input type="button" class="btn btn-default btnSkip" value="Skip">&emsp;
                              <input type="button" class="btn btnNext foot-btn" onclick="valid_inf2()" value="NEXT"> <br><br>
                              &emsp;&emsp;&emsp;<?php if(isset($employees)) { ?> <button type="submit" data-form="emp_form" onclick="valid_inf5()" class="btn btn-info foot-btn" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('clients_edit_profile_update_btn'); ?></button> <?php } ?>&emsp;
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="menutax" class="tab-pane fade">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="form-group">
                                <?php $value = (isset($employees) ? $employees->tin_number : ''); ?>
                                <?php echo render_input('tax_tin', _l('tin_id'), $value); ?>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-6">
                              <?php $val = (isset($employees) ? $employees->tax_percentage : ''); ($employees->tax_percentage == 0.00 ? $value = '' : $value = $val); ?>
                              <?php echo render_input('tax_percentage', _l('tax_percentage'), $value, 'number'); ?>
                            </div>
                            <div class="col-sm-6">
                              <?php $val = (isset($employees) ? $employees->tax_value : ''); ($employees->tax_value == 0 ? $value = '' : $value = $val); ?>
                              <?php echo render_input('tax_value', _l('tax_value'), $value, 'number'); ?>
                            </div>
                          </div>
                        </div>
                        <div class="panel-footer">
                          <div class="form-group text-right">
                            <div class="form-group text-center">
                              <input type="button" class="btn btnPrevious foot-btn" value="Previous">
                               &emsp;<input type="button" class="btn btn-default btnSkip" value="Skip">&emsp;
                              <input type="button" class="btn btnNext foot-btn" onclick="valid_inf3()" value="NEXT"> <br><br>
                              &emsp;&emsp;&emsp;<?php if(isset($employees)) { ?> <button type="submit" data-form="emp_form" onclick="valid_inf5()" class="btn btn-info foot-btn" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('clients_edit_profile_update_btn'); ?></button> <?php } ?>&emsp;
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="menu2" class="tab-pane fade">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel">
                        <div class="panel-body">
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-6">
                                <label for="benefit_name"><?php echo _l('sss'); ?></label>
                                <input type="text" name="benefit_sss" class="form-control" value="Philippine Social Security System" readonly>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <?php $val = (isset($emp_benefit) ? $emp_benefit->benefit_sss_number : ''); ($emp_benefit->benefit_sss_number == 0 ? $value = '' : $value = $val); ?>
                                  <?php echo render_input('benefit_sss_number', _l('nav_benefit_id'), $value); ?>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-6">
                                <?php $val = (isset($emp_benefit) ? date('m-d-Y', strtotime($emp_benefit->benefit_sss_date)) : ''); ($emp_benefit->benefit_sss_date == 0000-00-00 ? $value = 'mm-dd-yyyy' : $value = $val); ?>
                                <?php echo render_input('benefit_sss_date', _l('nav_benefit_acc_date'), $value, '', '', '', '', 'datepicker'); ?>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label for="benefit_sss_status"><?php echo _l('nav_benefit_status'); ?></label>
                                  <select name="benefit_sss_status" class="form-control">
                                    <option value="">Nothing Selected</option>
                                    <?php if(isset($emp_benefit) && ($emp_benefit->benefit_sss_status) == 1) { ?>
                                    <option value="1" selected>Active</option>;
                                    <option value="2">Inactive</option>;
                                    <?php } elseif(isset($emp_benefit) && ($emp_benefit->benefit_sss_status) == 2) { ?>
                                      <option value="1">Active</option>;
                                      <option value="2" selected>Inactive</option>;
                                    <?php } else { ?>
                                      <option value="1">Active</option>
                                      <option value="2">Inactive</option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                          <hr>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-6">
                                <label for="benefit_name"><?php echo _l('pi'); ?></label>
                                <input type="text" name="benefit_pi" class="form-control" value="Pagtutulungan sa Kinabukasan: Ikaw, Bangko, Industria at Gobyerno" readonly>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <?php $val = (isset($emp_benefit) ? $emp_benefit->benefit_pi_number : ''); ($emp_benefit->benefit_pi_number == 0 ? $value = '' : $value = $val); ?>
                                  <?php echo render_input('benefit_pi_number', _l('nav_benefit_id'), $value); ?>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-6">
                                <?php $val = (isset($emp_benefit) ? date('m-d-Y', strtotime($emp_benefit->benefit_pi_date)) : ''); ($emp_benefit->benefit_pi_date == 0000-00-00 ? $value = 'mm-dd-yyyy' : $value = $val);  ?>
                                <?php echo render_input('benefit_pi_date', _l('nav_benefit_acc_date'), $value, '', '', '', '', 'datepicker'); ?>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label for="benefit_pi_status"><?php echo _l('nav_benefit_status'); ?></label>
                                  <select name="benefit_pi_status" class="form-control">
                                    <option value="">Nothing Selected</option>
                                    <?php if(isset($emp_benefit) && ($emp_benefit->benefit_pi_status) == 1) { ?>
                                    <option value="1" selected>Active</option>;
                                    <option value="2">Inactive</option>;
                                    <?php } elseif(isset($emp_benefit) && ($emp_benefit->benefit_pi_status) == 2) { ?>
                                      <option value="1">Active</option>;
                                      <option value="2" selected>Inactive</option>;
                                    <?php } else { ?>
                                      <option value="1">Active</option>
                                      <option value="2">Inactive</option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                          <hr>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-6">
                                <label for="benefit_name"><?php echo _l('ph'); ?></label>
                                <input type="text" name="benefit_ph" class="form-control" value="Philippine Health Insurance Corporation" readonly>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <?php $val = (isset($emp_benefit) ? $emp_benefit->benefit_ph_number : ''); ($emp_benefit->benefit_ph_number == 0 ? $value = '' : $value = $val); ?>
                                  <?php echo render_input('benefit_ph_number', _l('nav_benefit_id'), $value); ?>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-6">
                                <?php $val = (isset($emp_benefit) ? date('m-d-Y', strtotime($emp_benefit->benefit_ph_date)) : ''); ($emp_benefit->benefit_ph_date == 0000-00-00 ? $value = 'mm-dd-yyyy' : $value = $val);  ?>
                                <?php echo render_input('benefit_ph_date', _l('nav_benefit_acc_date'), $value, '', '', '', '', 'datepicker'); ?>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label for="benefit_ph_status"><?php echo _l('nav_benefit_status'); ?></label>
                                  <select name="benefit_ph_status" class="form-control">
                                    <option value="">Nothing Selected</option>
                                    <?php if(isset($emp_benefit) && ($emp_benefit->benefit_ph_status) == 1) { ?>
                                    <option value="1" selected>Active</option>;
                                    <option value="2">Inactive</option>;
                                    <?php } elseif(isset($emp_benefit) && ($emp_benefit->benefit_ph_status) == 2) { ?>
                                      <option value="1">Active</option>;
                                      <option value="2" selected>Inactive</option>;
                                    <?php } else { ?>
                                      <option value="1">Active</option>
                                      <option value="2">Inactive</option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="panel-footer">
                          <div class="form-group text-center">
                            <input type="button" class="btn btnPrevious foot-btn" value="Previous">
                             &emsp;<input type="button" class="btn btn-default btnSkip" value="Skip">&emsp;
                            <input type="button" class="btn btnNext foot-btn" onclick="valid_inf4()" value="NEXT"> <br><br>
                            &emsp;&emsp;&emsp;<?php if(isset($employees)) { ?> <button type="submit" data-form="emp_form" onclick="valid_inf5()" class="btn btn-info foot-btn" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('clients_edit_profile_update_btn'); ?></button> <?php } ?>&emsp;
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="menu3" class="tab-pane fade">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="super_visor_id"><?php echo _l('nav_supervisor'); ?></label>
                                <select name="super_visor_id" class="form-control">
                                  <option value="">Nothing Selected</option>
                                  <?php 
                                    foreach ($staff_admin as $sadmin) { ?>
                                    <option <?php if(isset($employees) && $employees->super_visor_id == $sadmin['staffid']) { echo 'selected'; } ?> value="<?php echo $sadmin['staffid']; ?>"><?php echo $sadmin['firstname'], ' ', $sadmin['lastname'] ; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label for="supervisor_status"><?php echo _l('nav_stat_supervisor'); ?></label>
                                <select name="supervisor_status" class="form-control">
                                  <option value="">Nothing Selected</option>
                                  <?php if(isset($employees) && ($employees->supervisor_status) == 1) { ?>
                                    <option value="1" selected>Active</option>;
                                    <option value="2">Inactive</option>;
                                  <?php } elseif(isset($employees) && ($employees->supervisor_status) == 2) { ?>
                                    <option value="1">Active</option>;
                                    <option value="2" selected>Inactive</option>;
                                  <?php } else { ?>
                                    <option value="1">Active</option>
                                    <option value="2">Inactive</option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12">
                              <?php $value = (isset($employees) ? $employees->supervisor_report : ''); ?>
                              <?php echo render_textarea('supervisor_report', _l('nav_supervisor_report'), $value); ?>
                            </div>
                          </div>
                        </div>
                        <div class="panel-footer">
                          <div class="form-group text-center">
                            <input type="button" class="btn btnPrevious foot-btn" value="Previous">&emsp;
                            <?php if(isset($employees)) { ?> <button type="submit" data-form="emp_form" onclick="valid_inf5()" class="btn btn-info foot-btn" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('clients_edit_profile_update_btn'); ?></button> <?php } else { ?> <button type="submit" data-form="emp_form" onclick="valid_inf5()" class="btn foot-btn" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('submit'); ?></button> <?php }?>
                            
                          </div>
                        </div>
                        <?php echo form_close(); ?>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            <!-- </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php init_tail(); ?>
<script type="text/javascript">
    if( $('#status').val() == '') {
      $('#h_spousef').prop( "readonly", true );
      $('#h_spousel').prop( "readonly", true );
    } 
    $('#status').change(function() {
      if($(this).val() == 2) {
          $('#h_spousef').prop( "readonly", false );
          $('#h_spousel').prop( "readonly", false );
      } else {       
          $('#h_spousef').prop( "readonly", true );
          $('#h_spousel').prop( "readonly", true );
          $('#h_spousef').val('');
          $('#h_spousel').val('');
          $('#h_children').val('');
      }
  });
</script>
<script>
  var loadFile = function(event) {
    $('#e_pic').val($('#pic').val());
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
<script>
  $('.btnPrevious').click(function() {
    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
  });
  $('.btnSkip').click(function() {
    $('.nav-tabs > .active').next('li').find('a').trigger('click');
  });

  $("#id_number").on('keyup', function() {
    var inpidnumber = document.getElementById('id_number');
    if (inpidnumber.value.length === 0) return;
    document.getElementById("id_number").style.borderColor = "green";
  });
  $("#first_name").on('keyup', function() {
    var inpfirstname = document.getElementById('first_name');
    if (inpfirstname.value.length === 0) return;
    document.getElementById("first_name").style.borderColor = "green";
  });
  $("#last_name").on('keyup', function() {
    var inpfirstname = document.getElementById('last_name');
    if (inpfirstname.value.length === 0) return;
    document.getElementById("last_name").style.borderColor = "green";
  });
  $("#email").on('keyup', function() {
    var inpemail = document.getElementById('email');
    if (inpemail.value.length === 0) return;
    document.getElementById("email").style.borderColor = "green";
    var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

    if(!(inpemail.value).match(reEmail)) {
      document.getElementById("email_v_message").style.color = "red";
      document.getElementById("email_v_message").innerHTML = "Invalid email address";
      document.getElementById("email").style.borderColor = "red";
      return false;
    }
    document.getElementById("email_v_message").innerHTML = "";
    return true;
  });
  //hired date
  $("#hireddate").on('change', function() {
    var inputhiredate = document.getElementById('hireddate');
    if (inputhiredate.value.length === 0) return;
   document.getElementById("hireddate").style.borderColor = "green";
  });
  $("#rate").on('keyup', function() {
    var inputrate = document.getElementById('rate');
    if (inputrate.value.length === 0) return;
    document.getElementById("rate").style.borderColor = "green";
  });
  $("#skill_index").on('keyup', function() {
    var inputskill_index = document.getElementById('skill_index');
    if (inputskill_index.value.length === 0) return;
    document.getElementById("skill_index").style.borderColor = "green";
  });
  $("#dob").on('change', function() {
    var inputdob = document.getElementById('dob');
    if (inputdob.value.length === 0) return;
    document.getElementById("dob").style.borderColor = "green";
  });
  $("#gender").on('change', function() {
    var inputgender = document.getElementById('gender');
    if (inputgender.value.length === 0) return;
    document.getElementById("gend").innerHTML = "";
  });
  $("#h_phone").on('keyup', function() {
    var inputh_phone = document.getElementById('h_phone');
    if (inputh_phone.value.length === 0) return;
    document.getElementById("h_phone").style.borderColor = "green";
  });
  $("#emrg_contact_person").on('keyup', function() {
    var inpute_h_phone = document.getElementById('emrg_contact_person');
    if (inpute_h_phone.value.length === 0) return;
    document.getElementById("emrg_contact_person").style.borderColor = "green";
  });
  $("#em_contact").on('keyup', function() {
    var inputem_contact = document.getElementById('em_contact');
    if (inputem_contact.value.length === 0) return;
    document.getElementById("em_contact").style.borderColor = "green";
  });
  $("#dept").on('change', function() {
    var inputdepartment = document.getElementById('dept');
    if (inputdepartment.value.length === 0) return;
    document.getElementById("deptt").innerHTML = "";
  });
  $("#designation").on('change', function() {
    var inputdesignaiton = document.getElementById('designation');
    if (inputdesignaiton.value.length === 0) return;
   document.getElementById("desig").innerHTML = "";
  });
  $("#rate_type").on('change', function() {
    var inputrate_type = document.getElementById('rate_type');
    var rt = $('#rate_type').val();
    if (inputrate_type.value.length === 0) { return;
     document.getElementById("rat_tp").innerHTML = ""; }
    else if (rt == '') { $('#rate').val(''); }
    else if (rt == 1) { $('#rate').val('41'); } 
    else if (rt == 2) { $('#rate').val('325'); }
  });

  function valid_inf() {
    var usernameInput = document.getElementById('first_name');
    var emailInput = document.getElementById('email');
    var id_number = $('#id_number').val();
    var firstname = $('#first_name').val();
    var lastname = $('#last_name').val();
    var email = $('#email').val();
    if (empty(id_number)) {
      document.getElementById("id_number").style.borderColor = "red";
    } else {
      $("#id_number").on('keyup', function(){
        document.getElementById("id_number").style.borderColor = "green";
      });
    }
    if (empty(firstname)) {
      document.getElementById("first_name").style.borderColor = "red";
    } else {
      $("#first_name").on('keyup', function(){
        document.getElementById("first_name").style.borderColor = "green";
      });
    }
    if (empty(lastname)) {
      document.getElementById("last_name").style.borderColor = "red";
    } else {
      $("#last_name").on('keyup', function(){
        document.getElementById("last_name").style.borderColor = "green";
      });
    }
    if(id_number !== "" && firstname !== "" && lastname !== ""){
      $('.nav-tabs > .active').next('li').find('a').trigger('click');
    }
  }

  function valid_inf6() {
    $('.nav-tabs > .active').next('li').find('a').trigger('click');
  }

  function valid_inf7() {
    $('.nav-tabs > .active').next('li').find('a').trigger('click');
  }

  function valid_inf2() {
    $('.nav-tabs > .active').next('li').find('a').trigger('click');
  }

  function valid_inf3() {
    $('.nav-tabs > .active').next('li').find('a').trigger('click');
  }

   function valid_inf4() {
    $('.nav-tabs > .active').next('li').find('a').trigger('click');
  }

  function valid_inf5() {
    $("#emp_form").submit();
  }

</script>

<script type="text/javascript">
  $(document).ready(function() {
    // choose text for the show/hide link - can contain HTML (e.g. an image)
    var showText='<span class="btn btn-primary" >Add More</span>';
    var hideText='<span class="btn btn-danger" >Close</span>';

    // initialise the visibility check
    var is_visible = false;

    // append show/hide links to the element directly preceding the element with a class of "toggle"
    $('.toggle').prev().append(' <a href="#" class="toggleLink">'+showText+'</a>');

    // hide all of the elements with a class of 'toggle'
    $('.toggle').hide();

    // capture clicks on the toggle links
    $('a.toggleLink').click(function() {
      // switch visibility
      is_visible = !is_visible;

      // change the link depending on whether the element is shown or hidden
      $(this).html( (!is_visible) ? showText : hideText);

      // toggle the display - uncomment the next line for a basic "accordion" style
      //$('.toggle').hide();$('a.toggleLink').html(showText);
      $(this).parent().next('.toggle').toggle('slow');

      // return false so any link destination is not followed
      return false;

    });
  });
</script>
</body>
</html>