<?php

defined('BASEPATH') or exit('No direct script access allowed');

$aColumns = [
    '1',
    db_prefix().'department.dept_id as dept_id',
    'department_name',
];

$sIndexColumn = 'dept_id';
$sTable       = db_prefix().'department' ;
$where        = [];
$join = [];

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, []);

$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];

    // User id
    $row[] = $aRow['dept_id'];

    // Company
    $department_name  = $aRow['department_name'];

    $url = admin_url('department/create_dept/' . $aRow['dept_id']);
    $edit_url = admin_url('department/fetch_dept/' . $aRow['dept_id']);
    $del_url  = admin_url('department/delete_dept/' . $aRow['dept_id']);

    $department_name = $department_name;

    $department_name .= '<div class="row-options">';
    $department_name .= '<a href="' . $edit_url . '">' . _l('edit') . '</a>';
    $department_name .= ' | <a href="' . $del_url . '" class="text-danger _delete">' . _l('delete') . '</a>';

    $department_name .= '</div>'; 

    $row[] = $department_name;

    $row['DT_RowClass'] = 'has-row-options';

    $row = hooks()->apply_filters('customers_table_row_data', $row, $aRow);

    $output['aaData'][] = $row;
}
?>

