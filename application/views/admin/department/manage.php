
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s mbot10">

               <div class="panel-body _buttons">
               <!--  <h2 class="no-margin font-large pull-left">
                  <i class="fa fa-home" aria-hidden="true"></i> 
                  Department</h2> -->
                  <a href="#" class="pull-left btn btn-info mleft5 btn-with-tooltip" data-target="#newdept" data-toggle="modal" title="Add New Department">New Department</a>
               </div>
        </div> 
        <div class="row">
          <div class="col-md-12" id="small-table">
            <div class="panel_s">
              <div class="panel-body">
                <div class="clearfix"></div>
                <?php
                  $table_data = array();
                  $_table_data = array(
                    array(
                      'name'=>_l('sl_number'),
                      'th_attrs'=>array('class'=>'toggleable', 'id'=>'th-number')
                    ),
                    array(
                      'name'=>_l('dept_name'),
                      'th_attrs'=>array('class'=>'toggleable', 'id'=>'th-dept_name')
                    ),
                  );
                  foreach($_table_data as $_t){
                    array_push($table_data,$_t);
                  }

                  // $custom_fields = get_custom_fields('customers',array('show_on_table'=>1));
                  // foreach($custom_fields as $field){
                  //   array_push($table_data,$field['name']);
                  // }

                  $table_data = hooks()->apply_filters('department_table_columns', $table_data);

                     render_datatable($table_data,'department',[],[
                           'data-last-order-identifier' => 'department',
                           'data-default-order'         => get_table_last_order('department'),
                     ]);
                     ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- modals -->
<div id="newdept" class="modal fade" role="dialog">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <strong>
                    <h3 class="no-margin">
                      <i class="fa fa-home" aria-hidden="true"></i> 
                      New Department</h3>
                  </strong>
              </div>
              <?= form_open('admin/department/dept_insert/') ?> 
                <div class="modal-body">
                  <div class="form-group">
                    <label for="dept"><strong>Department</strong></label>
                    <input type="text" class="form-control" id="dept" name="dept_name" placeholder="Enter here">
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="form-group text-right">
                    <button type="button" data-dismiss="modal" class="btn btn-default w-md m-b-5">Cancel</button>
                    <button type="submit" class="btn btn-info w-md m-b-5">Save</button>
                  </div>
                </div>
              <?php echo form_close() ?> 
            </div>
          </div>
        </div>
<div id="newdiv" class="modal fade" role="dialog">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <strong>
                    <h3 class="no-margin">
                      <i class="fa fa-home" aria-hidden="true"></i> 
                      New Division</h3>
                  </strong>
              </div>
               <?= form_open('admin/division/div_insert/') ?> 
                <div class="modal-body">
                  <div class="form-group">
                    <label for="sel_dept"><strong>Select Department</strong></label>
                    <div class="row-fluid">
                      <select name="dept_id" class="selectpicker" id="sel_dept"  data-width="100%" data-live-search="true">
                        <option>Select</option>
                         <?php
                          foreach ($list_dept as $dept_name) {
                            ?>
                              <option value="<?php echo $dept_name['dept_id']?>"><?php echo $dept_name['department_name'] ?> </option>
                          <?php
                            }
                          ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="dept"><strong>Division</strong></label>
                    <input type="text" name="div_name" class="form-control" id="dept" placeholder="Enter here">
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="form-group text-right">
                    <button type="button" data-dismiss="modal" class="btn btn-default w-md m-b-5">Cancel</button>
                    <button type="submit" class="btn btn-info w-md m-b-5">Save</button>
                  </div>
                </div>
              <?php echo form_close() ?> 
            </div>
          </div>
        </div>

<?php init_tail(); ?>


<script>
   $(function(){
       var DepartmentServerParams = {};
       $.each($('._hidden_inputs._filters input'),function(){
          DepartmentServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
      });
       DepartmentServerParams['exclude_inactive'] = '[name="exclude_inactive"]:checked';

       var tAPI = initDataTable('.table-department', admin_url+'department/table', [0], [0], DepartmentServerParams,<?php echo hooks()->apply_filters('department_table_default_order', json_encode(array(0,'asc'))); ?>);
       $('input[name="exclude_inactive"]').on('change',function(){
           tAPI.ajax.reload();
       });
   });
</script>
</body>
</html>
