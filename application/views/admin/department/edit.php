<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
       
        <div class="row">
          <div class="col-md-12" id="small-table">
            <div class="panel_s">
              <div class="panel-body">
                <div class="clearfix"></div>
                   <br>

                   <?php
                    if (isset($show_dept)) { ?>
                       <?= form_open('admin/department/update_dept/' . $show_dept->dept_id ) ?> 
                      <div class="form-group">
                        <label for="dept"><strong>Edit Department</strong></label>
                        <input type="text" class="form-control" id="dept" name="dept_name" value="<?php echo $show_dept->department_name; ?>">
                      </div>
                      <div class="form-group text-right">
                        <button type="submit" name="submit" class="btn btn-info w-md m-b-5">SAVE CHANGE</button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <a href="<?php echo admin_url('department/create_dept/') ?>" type="button" class="btn btn-default">GO BACK</a>
                      </div>
                      <?php echo form_close() ?> 
                   <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<?php init_tail(); ?>

<script>
   $(function(){
       var DepartmentServerParams = {};
       $.each($('._hidden_inputs._filters input'),function(){
          DepartmentServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
      });
       DepartmentServerParams['exclude_inactive'] = '[name="exclude_inactive"]:checked';

       var tAPI = initDataTable('.table-department', admin_url+'department/table', [0], [0], DepartmentServerParams,<?php echo hooks()->apply_filters('department_table_default_order', json_encode(array(0,'asc'))); ?>);
       $('input[name="exclude_inactive"]').on('change',function(){
           tAPI.ajax.reload();
       });
   });
</script>
</body>
</html>
