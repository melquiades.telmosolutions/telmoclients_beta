<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div id="salary" class="hide">
   <table class="table table-salary scroll-responsive">
      <thead>
         <tr>
            <th><?php echo '#'; ?></th>
            <th><?php echo _l('employee_id_num'); ?></th>
            <th><?php echo _l('employee'); ?></th>
            <th><?php echo _l('date'); ?></th>
            <th><?php echo _l('tot_w_h'); ?></th>
            <th><?php echo _l('tot_w_d'); ?></th>
            <th><?php echo _l('tot_ot'); ?></th>
            <th><?php echo _l('rate'); ?></th>
            <th><?php echo _l('tot_salary'); ?></th>
         </tr>
      </thead>
      <tbody></tbody>
      <tfoot>
         <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="total_salary"></td>
         </tr>
      </tfoot>
   </table>
</div>