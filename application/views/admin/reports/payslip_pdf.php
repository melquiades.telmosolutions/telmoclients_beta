<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo isset($title) ? $title : get_option('companyname'); ?></title>
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
</head>
<style>
	td {
		font-weight: bold;
		padding: 1%;
	}
	.c {
        text-align: center;
    }
    .tdb {
        border-bottom: 1px solid white;
    }
    .tdt {
        border-top: 1px solid white;
    }
    /*body {
    	page-break-after: always;
    }*/
</style>
<body>
	<?php foreach($payslip_pdf as $value) {
		$pos            = $this->db->where('pos_id', $value['position_id'])->get(db_prefix().'position')->row();
		$sv             = $this->db->where('staffid', $value['super_visor_id'])->get(db_prefix().'staff')->row();
		$pm             = $this->db->where('id', $value['paymentmode'])->get(db_prefix().'payment_modes')->row();
		$cur            = $this->db->where('id', $value['currency'])->get(db_prefix().'currencies')->row();
		$duty_t         = $this->db->where('id', $value['duty_type'])->get(db_prefix().'duty_type')->row();
		$get_projid     = $this->db->query("SELECT GROUP_CONCAT(name SEPARATOR ',<br>') as projname FROM " . db_prefix() . "employeesalarypayment_project JOIN " . db_prefix() . "projects ON " . db_prefix() . "projects.id=project_id WHERE emp_sal_pay_id = " . $value['emp_sal_pay_id'])->row();
		$date_minmax    = $this->db->query('SELECT MIN(start_date) as min_start, MAX(end_date) as max_end FROM ' . db_prefix() . 'salary_sheet_generate WHERE ssg_id= ' . $value['salary_name_id'])->row();
		$get_benefit    = $this->db->where('amount !=', 0)->where('employee_id', $value['employee_id'])->get(db_prefix() . 'employee_salary_setup')->row();
// 		$benefit_name   = $this->db->where('benefit_id', $get_benefit->salary_type_id)->get(db_prefix() . 'benefit')->row();
		$bonus_name     = $this->db->where('bonus_emp_id', $value['employee_id'])->where('bonus_amount', $value['salary_setup_bonus'])->get(db_prefix() . 'bonus_setup')->row();
	?>
		<div class="payslip">
	        <div class="company-logo">
	            <img src="./uploads/company/logopdf.jpg" width="250" height="100" alt="Company Logo" style="margin-left: 35%;">
	        </div>
	        <h5 style="text-align: center;font-weight: bold;">
	        	<?php echo get_option('invoice_company_address'),'<br>',get_option('invoice_company_city'),',',get_option('company_state'),'<br>','<a href="https://'.get_option('main_domain').'" target="_blank">'.get_option('main_domain').'</a>','<br>','<a href="https://www.facebook.com/telmosolutions" target="_blank">www.facebook.com/telmosolutions</a>';
	        	?>
	        </h5>
	        <table class="table" border="0" table-layout: auto;>
	        	<tr>
	        		<th>Email: <a href="#" style="font-weight: normal;">contact@telmosolutions.com</a></th>
	        		<th style="text-align: right;">Mobile: <span style="font-weight: normal;"><?php echo get_option('invoice_company_phonenumber'); ?></span></th>
	        	</tr>
	        </table>
	    </div>
	    <br>
	    <table class="table" border="1">
	    	<thead>
		    	<tr>
		    		<th colspan="2" style="text-align: center;padding: 2%;background-color: #000099;color: white;">Employee/Freelancer Info:</th>
		    	</tr>
		    </thead>
		    <tbody>
		    	<tr>
		    		<td style="width: 50%;">Employee Name:&nbsp;&nbsp;<?php echo $value['first_name'], ' ', $value['last_name']; ?></td>
		    		<td style="width: 50%;">Employer:&nbsp;&nbsp;<?php if($sv->staffid != '') echo $sv->firstname, ' ', $sv->lastname; else echo 'Telmo Solutions'; ?></td>
		    	</tr>
		    	<tr>
		    		<td>Employee Address:&nbsp;&nbsp;<?php echo $value['house_no'], ' ', $value['street_name'], ' ', $value['subdivision'], ' ', $value['barangay']; ?></td>
		    		<td>Job Status:&nbsp;&nbsp;<?php echo $pos->position_name; ?></td>
		    	</tr>
		    	<tr>
		    		<td>Employee City, Province:&nbsp;&nbsp;<?php echo $value['city'], ' ', $value['province']; ?></td>
		    		<td>Services Rendered:&nbsp;&nbsp;<?php echo $get_projid->projname;?></td>
		    	</tr>
		    	<tr>
		    		<td>Payment Mode:&nbsp;&nbsp;<?php echo $pm->name; ?></td>
		    		<td>Account number: &nbsp;&nbsp; N/A</td>
		    	</tr>
		    </tbody>
	    </table>
	    <br>
	    <table class="table" border="1" table-layout: auto;>
	    	<thead>
		    	<tr>
		    		<th style="text-align: center;padding: 2%;background-color: #000099;color: white;">Release Date</th>
		    		<th style="text-align: center;padding: 2%;background-color: #000099;color: white;">Date of Service Rendered</th>
		    		<th style="text-align: center;padding: 2%;background-color: #000099;color: white;">Total Working Days Rendered</th>
		    		<th style="text-align: center;padding: 2%;background-color: #000099;color: white;">Benefits</th>
		    		<th style="text-align: center;padding: 2%;background-color: #000099;color: white;">Adjustment/Bonus</th>
		    		<th style="text-align: center;padding: 2%;background-color: #000099;color: white;">Gross Pay<br>Php</th>
		    		<th style="text-align: center;padding: 2%;background-color: #000099;color: white;">Total Pay<br>Php</th>
		    </thead>
		    <tbody>
		    	<tr>
		    		<td class="c"><br><?php echo date('M d, Y', strtotime($value['end_date'])), '<br>', date('l', strtotime($value['end_date'])), '<br>', date('m/d/Y', strtotime($value['end_date'])); ?></td>
		    		<td class="c"><?php $dmin = date('M d, Y', strtotime($date_minmax->min_start)); $dmax = date('M d, Y', strtotime($date_minmax->max_end)); echo $dmin, '<br> — <br>', $dmax; ?></td>
		    		<td class="c"><?php $value['working_period'] > 1 ? $d = 'Working Days' : $d = 'Working Day'; echo $value['working_period'], ' ', $d, '<br><br>', $value['remarks'];?></td>
		    		<td class="c"><?php echo (!empty($benefit_name->benefit_id) ? $benefit_name->benefit_name : ''); echo $value['salary_setup_benefits'] == 0 || $value['salary_setup_benefits'] == 0.00 ? 'N/A' : number_format($value['salary_setup_benefits'], 2). ' '. $cur->name; ?></td>
		    		<td class="c"><?php echo (!empty($bonus_name->bonus_id) ? $bonus_name->bonus_name : ''), '<br>', $value['salary_setup_bonus'] == 0 ? 'N/A' : number_format($value['salary_setup_bonus'], 2). ' '. $cur->name; ?></td>
		    		<td class="c"><?php echo number_format($value['rate'], 2), ' ', $cur->name; ?></td>
		    		<?php $total_ = $value['total_salary'] + $value['salary_setup_benefits'] + $value['salary_setup_bonus']; ?>
		    		<td class="c"><?php echo number_format($total_, 2), ' ', $cur->name; ?></td>
		    	</tr>
		    </tbody>
	    </table>
	    <br>
	    <table class="table" border="1" table-layout: auto; style="page-break-after: always;">
	    	<thead>
		    	<tr>
		    		<th style="text-align: center;padding: 2%;background-color: #000099;color: white;">Prepared By:</th>
		    		<th style="text-align: center;padding: 2%;"><img src="./uploads/company/logopdf.jpg" width="250" height="100" alt="Company Logo" style="text-align: center;"><br><h6>Telmo Solutions Billing</h6></th>
		    		<th style="text-align: center;padding: 2%;background-color: #000099;color: white;">Received By:</th>
		    		<th style="text-align: center;padding: 2%;"><br>_____________________<br><?php echo $value['first_name'], ' ', $value['last_name']; ?><br><?php echo $pos->position_name; ?><br><?php echo $duty_t->type_name; ?><br><br><br><?php echo date('m/d/Y', strtotime($value['end_date'])), '<br>', date('l', strtotime($value['end_date'])); ?></th>
		    	</tr>
		    </thead>
	    </table>
	<?php } ?>
</body>
</html>