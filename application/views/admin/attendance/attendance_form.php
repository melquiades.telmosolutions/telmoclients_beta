<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<style type="text/css">
  .form-control[readonly]{
    background-color: #ffffff !important;
    border-color: #d1dbe5 !important;
    color: #000000 !important;
    cursor: not-allowed; }
    .checkbox {
   margin-top: 2px !important;
    margin-bottom: 0px !important;
    padding: 0px !important;
}
.badgebox
{opacity: 0;}
.badgebox + .badge
{text-indent: -999999px;width: 27px;}
.badgebox:focus + .badge
{box-shadow: inset 0px 0px 5px;}
.badgebox:checked + .badge
{text-indent: 0;}
table thead>tr>th {
    color: #4e75ad !important;
    background: #f6f8fa !important;
    vertical-align: middle !important;
    border-bottom: 1px solid !important;
    border-color: #ebf5ff !important;
    font-size: 13px !important;
    padding-top: 9px !important;
    padding-bottom: 8px !important;
}
.btn-custom{padding: 5px 7px !important;font-size: 11.5px !important}
.text-danger{margin-left: 15px;}
</style>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">

        </div> 
            <div class="row">
               <div class="col-md-12" id="small-table">
                  <div class="panel_s">
                     <div class="panel-body">
                        <div class="clearfix"></div>

                        <?php
                        if ($savedlog) { ?>
                        <?= form_open('admin/attendance_form/updateweeklylog'); ?>
                        <h5>Projects:</h5> 
                        <h6> &emsp;&emsp;&emsp;
                          <?php foreach($savedlog_proj as $key => $saved_proj) { 
                            echo $saved_proj->name;
                            echo count($savedlog_proj)-1 == $key ? '':', ';
                            ?> <input type="hidden" name="proid[]" id="project_id_<?php echo $key; ?>" value="<?php echo $saved_proj->project_id; ?>">
                          <?php } ?>
                        </h6>
                        <input type="hidden" value="<?php echo count($savedlog_proj); ?>" id="proj_number">

                        <table class="table" id="tblmodal_members" style="width: 100%">
                          <thead>
                            <tr>
                              <th style="width: 24%"><strong><center>Selected Employee</center></strong></th>
                              <th style="width: 3%"></th>
                              <th style="width: 23%"><strong><center>Selected Date</center></strong></th>
                              <th style="width: 23%"><strong><center>Select Period</center></strong></th>
                              <th style="width: 15%"><strong><center>Missed Hours</center></strong></th>
                              <th style="width: 15%"><strong><center>OT hours</center></strong></th>
                              <th class="hide">Remove</th>
                            </tr>
                          </thead>
                          <tbody>
                            <input type="hidden" value="<?php echo $datalength ?>" id="datalength">
                            <tr>
                              <td></td>
                              <td></td>
                              <td><center> 
                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate2"><i class="fa fa-plus"></i> Add another date</button>
                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate3"><i class="fa fa-plus"></i> Add another date</button>
                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate4"><i class="fa fa-plus"></i> Add another date</button>
                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate5"><i class="fa fa-plus"></i> Add another date</button>
                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate6"><i class="fa fa-plus"></i> Add another date</button>
                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate7"><i class="fa fa-plus"></i> Add another date</button>

                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate8"><i class="fa fa-plus"></i> Add another date</button>
                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate9"><i class="fa fa-plus"></i> Add another date</button>
                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate10"><i class="fa fa-plus"></i> Add another date</button>
                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate11"><i class="fa fa-plus"></i> Add another date</button>
                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate12"><i class="fa fa-plus"></i> Add another date</button>
                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate13"><i class="fa fa-plus"></i> Add another date</button>
                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate14"><i class="fa fa-plus"></i> Add another date</button>
                                <button style="display: none;" type="button" class="btn btn-sm btn-primary" id="btnadddate15"><i class="fa fa-plus"></i> Add another date</button>
                              </center></td>
                              <td style="vertical-align: middle;">
                                <center><div class="checkbox checkbox-primary check_period<?php echo $key ?>"><input type="checkbox" id="check_period"><label for="check_period"><small><strong>Same period for all employee</strong></small></label></div></center>
                              </td>
                              <td></td>
                              <td style="vertical-align: middle;">
                              </td>
                              <td class="hide"></td>
                            </tr>
                           <?php

                            foreach ($savedlog as $key => $value) { ?>

                            <tr id="addedrow">
                              <td style="vertical-align: middle;">
                                <input type="hidden" id="att_id<?php echo $key ?>" name="atten_his_id[]" value="<?php echo $value->atten_id ?>">
                                <input type="hidden" id="lo_id<?php echo $key ?>" name="loid[]" value="<?php echo $value->locationid ?>">
                                <input type="hidden" id="u_id<?php echo $key ?>" name="u_id[]" value="<?php echo $value->emploid ?>">
                                <input type="hidden" id="num_ber<?php echo $key ?>" name="number_num[]" value="<?php echo $value->num ?>">
                                <input type="hidden" id="pre_fix<?php echo $key ?>" name="prefix_pre[]" value="<?php echo $value->pre ?>">
                                <input type="hidden" id="att_date" name="att_date[]" value="<?php echo $value->att_date  ?>">
                                <input type="hidden" id="holiday<?php echo $key ?>" name="holiday[]">
                                <center><strong><?php echo $value->name ?></strong></center>
                              </td>
                              <td id="remove<?php echo $key ?>" style="vertical-align: middle;font-size: 15px">
                                <span><i class="fa fa-window-close" style="font-size:25px;color:white;"></i></span>
                              </td>
                              <td class="addedrows" id="adddate<?php echo $key ?>" style="vertical-align: middle;font-size: 15px" id="adddate<?php echo $key ?>">
                                <center><?php echo date("l, M d, Y", strtotime($value->att_date)) ?></center>
                              </td>
                              <td id="addcheckbox<?php echo $key ?>">
                                <center>
                                  <label for="am<?php echo $key ?>" class="btn btn-custom btn-primary">AM 
                                  <input type="checkbox" id="am<?php echo $key ?>" value="AM" class="badgebox">
                                  <span class="badge">&check;</span></label> &nbsp;&nbsp;
                                  <label for="pm<?php echo $key ?>" class="btn btn-custom btn-primary">PM 
                                  <input type="checkbox" value="PM" id="pm<?php echo $key ?>" class="badgebox">
                                  <span class="badge">&check;</span></label>
                                  <input type="hidden" name="period[]" id="period<?php echo $key ?>">&emsp;
                                  <div style="cursor:pointer;display:inline-block;" onclick="show_marker(<?php echo $key ?>)" class="showothers_btn<?php echo $key ?>"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div>
                                  <div style="cursor:pointer;display:inline-block;" onclick="hide_marker(<?php echo $key ?>)" class="hide hideothers_btn<?php echo $key ?>"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div>
                                  <br><br>
                                  <div id="divmarkers_id<?php echo $key ?>" style="display: none;">
                                    <select id="markers_id<?php echo $key ?>" class="selectpicker" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                      <option value=""><?php echo _l('dropdown_non_selected_tex'); ?></option>
                                      <option value="onleave">On Leave</option>
                                      <option value="sick">Sick</option>
                                      <option value="excused">Excused</option>
                                      <option value="holiday">Holiday</option>
                                      <option value="awol">Absent Without Leave</option>
                                    </select>
                                  </div>
                                </center>
                              </td>
                              <td id="addabsent<?php echo $key ?>">
                                <center>
                                  <input type="number" name="absent[]" id="absent<?php echo $key ?>" placeholder="Enter here" class="form-control">
                                </center>
                              </td>
                              <?php
                              $date = date("l", strtotime($value->att_date));
                              if ($date == 'Sunday' ) { ?>
                              <td id="addot<?php echo $key ?>">
                                <center>
                                  <input type="number" value="8" class="form-control" name="overtime[]"></center>
                              </td>
                             <?php }else{ ?>
                              <td id="addot<?php echo $key ?>">
                                <center>
                                  <input type="number" placeholder="Enter here" class="form-control clear" id="overtime<?php echo $key ?>" name="overtime[]"></center>
                              </td>
                              <td class="hide" id="remove_array<?php echo $key ?>" style="vertical-align: middle;font-size: 15px">
                                <input type="text" name="removeid[]" id="removeid<?php echo $key ?>" value="<?php echo $value->emploid ?>">
                                <input type="text" name="ival[]" id="ival_<?php echo $key ?>" value="<?php echo $key ?>">
                              </td>
                            <?php }  ?>
                            </tr>
                             
                           <?php }

                           ?>
                          </tbody>
                        </table>
                          <div class="btn-bottom-toolbar text-right" id="btn-bottom">
                            <input type="submit" id="butsave" class="btn btn-info" name="butsave" value="Save Attendance">  <?php echo form_close(); ?>
                            &nbsp;&nbsp;
                            <button type="button" class="btn btn-default btncancel">Cancel</button>
                            
                          </div>
                          <?= form_open('admin/attendance_form/cancelweeklylog') ?> 
                           <div id="contentcancel"></div>
                           <button style="display: none;" type="submit" class="btn btn-default clickbtncancel">btnCancel</button>
                          <?php echo form_close(); ?>
                        <?php }

                        else{ ?>
                        <?= form_open('admin/attendance_form/saveweeklylog') ?> 
                         <br>
                          <div class="form-group row">
                            <div class="col-sm-12">
                                <label class="control-label"><i style="color: red">*</i> Select Project</label>
                                <select name="proj_id[]" class="form-control selectpicker" id="proj_id" data-live-search="true" data-width="100%" data-actions-box="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" multiple="" required="">
                                  <?php
                                    foreach ($projects as $proj_name) {
                                      ?>
                                       <option value="<?php echo $proj_name['id']?>"><?php echo $proj_name['name'] ?> </option>
                                  <?php } ?>
                                </select>
                                
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-sm-12">
                              <label class="control-label"><i style="color: red">*</i> Location</label>
                                <select name="loc_id" class="form-control selectpicker" id="loc_id" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                    <option value="">Nothing Selected</option>
                                    <option value="1">Office Work</option>
                                    <option value="2">Work At Home</option>
                                </select>
                              
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-sm-12">
                              <label class="control-label"><i style="color: red">*</i> Members</label>
                                <select name="emp_id[]" class="form-control selectpicker" id="emp_id" data-live-search="true" data-width="100%" multiple="" data-actions-box="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" required>
                                    <?php
                                        foreach ($employee as $row_emp) { ?>
                                            <option value="<?php echo $row_emp['emp_id']; ?>"><?php echo $row_emp['first_name'], ' ', $row_emp['last_name']; ?></option>
                                    <?php } ?>
                                </select>
                              
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-sm-12">
                              <label class="control-label"><i style="color: red">*</i> Date</label>
                                <input type="date" class="form-control" name="att_time" required="">
                              
                            </div>
                          </div>
                          <?php
                            $max_num = $this->db->query("SELECT MAX(number) as number FROM " . db_prefix() . "attendance_history")->row();
                            $number = $max_num->number + 1;
                          ?>
                          <input type="hidden" name="num" value="<?php echo $number; ?>">
                          <input type="hidden" name="pre" value="ATTENDANCE-">
                          <br>
                         <button type="submit" class="btn btn-info" id="Continue">Continue</button>
                        <?php echo form_close(); }?> 
                       

                        </div> 
                        
                     </div>
                  </div>
               </div>
             
            </div>
         </div>
      </div>
   </div>
</div>
<!-- modal -->
<div id="add_date" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-body">
          <div class="form-group">
            <label for="dept" style="font-size: 17px"><strong><i style="color: red">*</i> Date</strong></label> <br>
            <input type="date" class="form-control" id="input_date">
          </div>
        </div>
        <div class="modal-footer">
          <div class="form-group text-right">
            <button type="button" id="dismissid" data-dismiss="modal" class="btn btn-default">Cancel</button>
            <button style="display: none;" type="button" class="btn btn-info btn_add2">Save</button>
            <button style="display: none;" type="button" class="btn btn-info btn_add3">Save</button>
            <button style="display: none;" type="button" class="btn btn-info btn_add4">Save</button>
            <button style="display: none;" type="button" class="btn btn-info btn_add5">Save</button>
            <button style="display: none;" type="button" class="btn btn-info btn_add6">Save</button>
            <button style="display: none;" type="button" class="btn btn-info btn_add7">Save</button>

            <button style="display: none;" type="button" class="btn btn-info btn_add8">Save</button>
            <button style="display: none;" type="button" class="btn btn-info btn_add9">Save</button>
            <button style="display: none;" type="button" class="btn btn-info btn_add10">Save</button>
            <button style="display: none;" type="button" class="btn btn-info btn_add11">Save</button>
            <button style="display: none;" type="button" class="btn btn-info btn_add12">Save</button>
            <button style="display: none;" type="button" class="btn btn-info btn_add13">Save</button>
            <button style="display: none;" type="button" class="btn btn-info btn_add14">Save</button>
            <button style="display: none;" type="button" class="btn btn-info btn_add15">Save</button>
          </div>
        </div>
    </div>
  </div>
</div>
<script>var hidden_columns = [4,5,6,7,8,9];</script>
<?php init_tail(); ?>
<script type="text/javascript">

  function show_marker(i) {
    $('.showothers_btn'+i).addClass('hide');
    $('.hideothers_btn'+i).removeClass('hide');
    $('#divmarkers_id'+i).show(100);
    $('#am'+i).prop('disabled', true);
    $('#pm'+i).prop('disabled', true);
  }
  function hide_marker(i) {
    $('.showothers_btn'+i).removeClass('hide');
    $('.hideothers_btn'+i).addClass('hide');
    $('#divmarkers_id'+i).hide(90);
    $('#am'+i).prop('disabled', false);
    $('#pm'+i).prop('disabled', false);
  }

  function show_marker2(i) {
    $('.showothers_btn2'+i).addClass('hide');
    $('.hideothers_btn2'+i).removeClass('hide');
    $('#divmarkers_id2'+i).show(100);
    $('#am2'+i).prop('disabled', 'true');
    $('#pm2'+i).prop('disabled', 'true');
  }
  function hide_marker2(i) {
    $('.showothers_btn2'+i).removeClass('hide');
    $('.hideothers_btn2'+i).addClass('hide');
    $('#divmarkers_id2'+i).hide(90);
    $('#am2'+i).prop('disabled', false);
    $('#pm2'+i).prop('disabled', false);
  }
  
  function show_marker3(i) {
    $('.showothers_btn3'+i).addClass('hide');
    $('.hideothers_btn3'+i).removeClass('hide');
    $('#divmarkers_id3'+i).show(100);
    $('#am3'+i).prop('disabled', true);
    $('#pm3'+i).prop('disabled', true);
  }
  function hide_marker3(i) {
    $('.showothers_btn3'+i).removeClass('hide');
    $('.hideothers_btn3'+i).addClass('hide');
    $('#divmarkers_id3'+i).hide(90);
    $('#am3'+i).prop('disabled', false);
    $('#pm3'+i).prop('disabled', false);
  }

  function show_marker4(i) {
    $('.showothers_btn4'+i).addClass('hide');
    $('.hideothers_btn4'+i).removeClass('hide');
    $('#divmarkers_id4'+i).show(100);
    $('#am4'+i).prop('disabled', true);
    $('#pm4'+i).prop('disabled', true);
  }
  function hide_marker4(i) {
    $('.showothers_btn4'+i).removeClass('hide');
    $('.hideothers_btn4'+i).addClass('hide');
    $('#divmarkers_id4'+i).hide(90);
    $('#am4'+i).prop('disabled', false);
    $('#pm4'+i).prop('disabled', false);
  }

  function show_marker5(i) {
    $('.showothers_btn5'+i).addClass('hide');
    $('.hideothers_btn5'+i).removeClass('hide');
    $('#divmarkers_id5'+i).show(100);
    $('#am5'+i).prop('disabled', true);
    $('#pm5'+i).prop('disabled', true);
  }
  function hide_marker5(i) {
    $('.showothers_btn5'+i).removeClass('hide');
    $('.hideothers_btn5'+i).addClass('hide');
    $('#divmarkers_id5'+i).hide(90);
    $('#am5'+i).prop('disabled', false);
    $('#pm5'+i).prop('disabled', false);
  }

  function show_marker6(i) {
    $('.showothers_btn6'+i).addClass('hide');
    $('.hideothers_btn6'+i).removeClass('hide');
    $('#divmarkers_id6'+i).show(100);
    $('#am6'+i).prop('disabled', true);
    $('#pm6'+i).prop('disabled', true);
  }
  function hide_marker6(i) {
    $('.showothers_btn6'+i).removeClass('hide');
    $('.hideothers_btn6'+i).addClass('hide');
    $('#divmarkers_id6'+i).hide(90);
    $('#am6'+i).prop('disabled', false);
    $('#pm6'+i).prop('disabled', false);
  }

  function show_marker7(i) {
    $('.showothers_btn7'+i).addClass('hide');
    $('.hideothers_btn7'+i).removeClass('hide');
    $('#divmarkers_id7'+i).show(100);
    $('#am7'+i).prop('disabled', true);
    $('#pm7'+i).prop('disabled', true);
  }
  function hide_marker7(i) {
    $('.showothers_btn7'+i).removeClass('hide');
    $('.hideothers_btn7'+i).addClass('hide');
    $('#divmarkers_id7'+i).hide(90);
    $('#am7'+i).prop('disabled', false);
    $('#pm7'+i).prop('disabled', false);
  }

  function show_marker8(i) {
    $('.showothers_btn8'+i).addClass('hide');
    $('.hideothers_btn8'+i).removeClass('hide');
    $('#divmarkers_id8'+i).show(100);
    $('#am8'+i).prop('disabled', true);
    $('#pm8'+i).prop('disabled', true);
  }
  function hide_marker8(i) {
    $('.showothers_btn8'+i).removeClass('hide');
    $('.hideothers_btn8'+i).addClass('hide');
    $('#divmarkers_id8'+i).hide(90);
    $('#am8'+i).prop('disabled', false);
    $('#pm8'+i).prop('disabled', false);
  }

  function show_marker9(i) {
    $('.showothers_btn9'+i).addClass('hide');
    $('.hideothers_btn9'+i).removeClass('hide');
    $('#divmarkers_id9'+i).show(100);
    $('#am9'+i).prop('disabled', true);
    $('#pm9'+i).prop('disabled', true);
  }
  function hide_marker9(i) {
    $('.showothers_btn9'+i).removeClass('hide');
    $('.hideothers_btn9'+i).addClass('hide');
    $('#divmarkers_id9'+i).hide(90);
    $('#am9'+i).prop('disabled', false);
    $('#pm9'+i).prop('disabled', false);
  }

  function show_marker10(i) {
    $('.showothers_btn10'+i).addClass('hide');
    $('.hideothers_btn10'+i).removeClass('hide');
    $('#divmarkers_id10'+i).show(100);
    $('#am10'+i).prop('disabled', true);
    $('#pm10'+i).prop('disabled', true);
  }
  function hide_marker10(i) {
    $('.showothers_btn10'+i).removeClass('hide');
    $('.hideothers_btn10'+i).addClass('hide');
    $('#divmarkers_id10'+i).hide(90);
    $('#am10'+i).prop('disabled', false);
    $('#pm10'+i).prop('disabled', false);
  }

  function show_marker11(i) {
    $('.showothers_btn11'+i).addClass('hide');
    $('.hideothers_btn11'+i).removeClass('hide');
    $('#divmarkers_id11'+i).show(100);
    $('#am11'+i).prop('disabled', true);
    $('#pm11'+i).prop('disabled', true);
  }
  function hide_marker11(i) {
    $('.showothers_btn11'+i).removeClass('hide');
    $('.hideothers_btn11'+i).addClass('hide');
    $('#divmarkers_id11'+i).hide(90);
    $('#am11'+i).prop('disabled', false);
    $('#pm11'+i).prop('disabled', false);
  }

  function show_marker12(i) {
    $('.showothers_btn12'+i).addClass('hide');
    $('.hideothers_btn12'+i).removeClass('hide');
    $('#divmarkers_id12'+i).show(100);
    $('#am12'+i).prop('disabled', true);
    $('#pm12'+i).prop('disabled', true);
  }
  function hide_marker12(i) {
    $('.showothers_btn12'+i).removeClass('hide');
    $('.hideothers_btn12'+i).addClass('hide');
    $('#divmarkers_id12'+i).hide(90);
    $('#am12'+i).prop('disabled', false);
    $('#pm12'+i).prop('disabled', false);
  }

  function show_marker13(i) {
    $('.showothers_btn13'+i).addClass('hide');
    $('.hideothers_btn13'+i).removeClass('hide');
    $('#divmarkers_id13'+i).show(100);
    $('#am13'+i).prop('disabled', true);
    $('#pm13'+i).prop('disabled', true);
  }
  function hide_marker13(i) {
    $('.showothers_btn13'+i).removeClass('hide');
    $('.hideothers_btn13'+i).addClass('hide');
    $('#divmarkers_id13'+i).hide(90);
    $('#am13'+i).prop('disabled', false);
    $('#pm13'+i).prop('disabled', false);
  }

  function show_marker14(i) {
    $('.showothers_btn14'+i).addClass('hide');
    $('.hideothers_btn14'+i).removeClass('hide');
    $('#divmarkers_id14'+i).show(100);
    $('#am14'+i).prop('disabled', true);
    $('#pm14'+i).prop('disabled', true);
  }
  function hide_marker14(i) {
    $('.showothers_btn14'+i).removeClass('hide');
    $('.hideothers_btn14'+i).addClass('hide');
    $('#divmarkers_id14'+i).hide(90);
    $('#am14'+i).prop('disabled', false);
    $('#pm14'+i).prop('disabled', false);
  }

  function show_marker15(i) {
    $('.showothers_btn15'+i).addClass('hide');
    $('.hideothers_btn15'+i).removeClass('hide');
    $('#divmarkers_id15'+i).show(100);
    $('#am15'+i).prop('disabled', true);
    $('#pm15'+i).prop('disabled', true);
  }
  function hide_marker15(i) {
    $('.showothers_btn15'+i).removeClass('hide');
    $('.hideothers_btn15'+i).addClass('hide');
    $('#divmarkers_id15'+i).hide(90);
    $('#am15'+i).prop('disabled', false);
    $('#pm15'+i).prop('disabled', false);
  }

  function removeBtn2(i) {
    $('#div_remove2_'+i).addClass('hide');
    $('#div_adddate2_'+i).addClass('hide');
    $('#div_addcheckbox2_'+i).addClass('hide');
    $('#div_addabsent2_'+i).addClass('hide');
    $('#div_addot2_'+i).addClass('hide');
    $('#ival2_'+i).val('remove');
    $('#div_adddate2_'+i).removeClass('addedrows');

    // var ll = $('.addedrows').length;
    // // if (ll != 15)

    // // get the hide class
    // var x = $('#div_addot2_'+i).attr('class');
    // var a = x.indexOf(' ');
    // var l = x.length;
    // var d = x.substr(a+1, l);
    // alert(d);

    // // get the class length. if (x2 != l) add another date btn.show elseif (x3 != l) add another date btn.show
  }
  function removeBtn3(i) {
    $('#div_remove3_'+i).addClass('hide');
    $('#div_adddate3_'+i).addClass('hide');
    $('#div_addcheckbox3_'+i).addClass('hide');
    $('#div_addabsent3_'+i).addClass('hide');
    $('#div_addot3_'+i).addClass('hide');
    $('#ival3_'+i).val('remove');
    $('#div_adddate3_'+i).removeClass('addedrows');
  }
  function removeBtn4(i) {
    $('#div_remove4_'+i).addClass('hide');
    $('#div_adddate4_'+i).addClass('hide');
    $('#div_addcheckbox4_'+i).addClass('hide');
    $('#div_addabsent4_'+i).addClass('hide');
    $('#div_addot4_'+i).addClass('hide');
    $('#ival4_'+i).val('remove');
    $('#div_adddate4_'+i).removeClass('addedrows');
  }
  function removeBtn5(i) {
    $('#div_remove5_'+i).addClass('hide');
    $('#div_adddate5_'+i).addClass('hide');
    $('#div_addcheckbox5_'+i).addClass('hide');
    $('#div_addabsent5_'+i).addClass('hide');
    $('#div_addot5_'+i).addClass('hide');
    $('#ival5_'+i).val('remove');
    $('#div_adddate5_'+i).removeClass('addedrows');
  }
  function removeBtn6(i) {
    $('#div_remove6_'+i).addClass('hide');
    $('#div_adddate6_'+i).addClass('hide');
    $('#div_addcheckbox6_'+i).addClass('hide');
    $('#div_addabsent6_'+i).addClass('hide');
    $('#div_addot6_'+i).addClass('hide');
    $('#ival6_'+i).val('remove');
    $('#div_adddate6_'+i).removeClass('addedrows');
  }
  function removeBtn7(i) {
    $('#div_remove7_'+i).addClass('hide');
    $('#div_adddate7_'+i).addClass('hide');
    $('#div_addcheckbox7_'+i).addClass('hide');
    $('#div_addabsent7_'+i).addClass('hide');
    $('#div_addot7_'+i).addClass('hide');
    $('#ival7_'+i).val('remove');
    $('#div_adddate7_'+i).removeClass('addedrows');
  }
  function removeBtn8(i) {
    $('#div_remove8_'+i).addClass('hide');
    $('#div_adddate8_'+i).addClass('hide');
    $('#div_addcheckbox8_'+i).addClass('hide');
    $('#div_addabsent8_'+i).addClass('hide');
    $('#div_addot8_'+i).addClass('hide');
    $('#ival8_'+i).val('remove');
    $('#div_adddate8_'+i).removeClass('addedrows');
  }
  function removeBtn9(i) {
    $('#div_remove9_'+i).addClass('hide');
    $('#div_adddate9_'+i).addClass('hide');
    $('#div_addcheckbox9_'+i).addClass('hide');
    $('#div_addabsent9_'+i).addClass('hide');
    $('#div_addot9_'+i).addClass('hide');
    $('#ival9_'+i).val('remove');
    $('#div_adddate9_'+i).removeClass('addedrows');
  }
  function removeBtn10(i) {
    $('#div_remove10_'+i).addClass('hide');
    $('#div_adddate10_'+i).addClass('hide');
    $('#div_addcheckbox10_'+i).addClass('hide');
    $('#div_addabsent10_'+i).addClass('hide');
    $('#div_addot10_'+i).addClass('hide');
    $('#ival10_'+i).val('remove');
    $('#div_adddate10_'+i).removeClass('addedrows');
  }
  function removeBtn11(i) {
    $('#div_remove11_'+i).addClass('hide');
    $('#div_adddate11_'+i).addClass('hide');
    $('#div_addcheckbox11_'+i).addClass('hide');
    $('#div_addabsent11_'+i).addClass('hide');
    $('#div_addot11_'+i).addClass('hide');
    $('#ival11_'+i).val('remove');
    $('#div_adddate11_'+i).removeClass('addedrows');
  }
  function removeBtn12(i) {
    $('#div_remove12_'+i).addClass('hide');
    $('#div_adddate12_'+i).addClass('hide');
    $('#div_addcheckbox12_'+i).addClass('hide');
    $('#div_addabsent12_'+i).addClass('hide');
    $('#div_addot12_'+i).addClass('hide');
    $('#ival12_'+i).val('remove');
    $('#div_adddate12_'+i).removeClass('addedrows');
  }
  function removeBtn13(i) {
    $('#div_remove13_'+i).addClass('hide');
    $('#div_adddate13_'+i).addClass('hide');
    $('#div_addcheckbox13_'+i).addClass('hide');
    $('#div_addabsent13_'+i).addClass('hide');
    $('#div_addot13_'+i).addClass('hide');
    $('#ival13_'+i).val('remove');
    $('#div_adddate13_'+i).removeClass('addedrows');
  }
  function removeBtn14(i) {
    $('#div_remove14_'+i).addClass('hide');
    $('#div_adddate14_'+i).addClass('hide');
    $('#div_addcheckbox14_'+i).addClass('hide');
    $('#div_addabsent14_'+i).addClass('hide');
    $('#div_addot14_'+i).addClass('hide');
    $('#ival14_'+i).val('remove');
    $('#div_adddate14_'+i).removeClass('addedrows');
  }
  function removeBtn15(i) {
    $('#div_remove15_'+i).addClass('hide');
    $('#div_adddate15_'+i).addClass('hide');
    $('#div_addcheckbox15_'+i).addClass('hide');
    $('#div_addabsent15_'+i).addClass('hide');
    $('#div_addot15_'+i).addClass('hide');
    $('#ival15_'+i).val('remove');
    $('#div_adddate15_'+i).removeClass('addedrows');
  }

  function count_addeddate() {
    var l = $('.addedrows').length;
    // var m = $('.addedrows').attr('id');
    for(var x=0;x<l;x++) {
      // alert(x);
    }
  }
  
$(document).ready(function(){
      var datalength = $('#datalength').val();
      var proj_number = $('#proj_number').val();
      for(var i=0; i<datalength; i++){
        $('#contentcancel').append('<input type="hidden" value="'+$('#lo_id'+i).val()+'" name="loid[]"><input type="hidden" value="'+$('#u_id'+i).val()+'" name="uid[]"><input type="hidden" value="'+$('#att_date').val()+'" name="atttime[]">');
      }
      for(var i=0; i<proj_number; i++){
        $('#contentcancel').append('<input type="hidden" value="'+$('#project_id_'+i).val()+'" name="proid[]">');
      }
      
      $('.btncancel').on('click', function() {
          $('.clickbtncancel').click();
      });
  });
</script>
<script type="text/javascript"> 
    
$(document).ready(function(){

    $('#btnadddate2').css({display:'inline-block'});
    $('.btn_add2').css({display:'inline-block'});

    $('#dismissid').on('click', function() {
          $('#input_date').val('');
      });
    $('#btnadddate2').on('click', function() {
          $('#add_date').modal({backdrop: 'static', keyboard: false});
          $('#btnadddate2').css({display:'none'});
          $('#btnadddate3').css({display:'inline-block'});
          
      });
    $('#btnadddate3').on('click', function() {
          $('#add_date').modal({backdrop: 'static', keyboard: false});
          $('#btnadddate3').css({display:'none'});
          $('#btnadddate4').css({display:'inline-block'});
          
      });
    $('#btnadddate4').on('click', function() {
          $('#add_date').modal({backdrop: 'static', keyboard: false});
          $('#btnadddate4').css({display:'none'});
          $('#btnadddate5').css({display:'inline-block'});
          
      });
    $('#btnadddate5').on('click', function() {
          $('#add_date').modal({backdrop: 'static', keyboard: false});
          $('#btnadddate5').css({display:'none'});
          $('#btnadddate6').css({display:'inline-block'});
          
      });
    $('#btnadddate6').on('click', function() {
          $('#add_date').modal({backdrop: 'static', keyboard: false});
          $('#btnadddate6').css({display:'none'});
          $('#btnadddate7').css({display:'inline-block'});
          
      });
    $('#btnadddate7').on('click', function() {
          $('#add_date').modal({backdrop: 'static', keyboard: false});
          $('#btnadddate7').css({display:'none'});
          $('#btnadddate8').css({display:'inline-block'});
      });

    $('#btnadddate8').on('click', function() {
      $('#add_date').modal({backdrop: 'static', keyboard: false});
      $('#btnadddate8').css({display:'none'});
      $('#btnadddate9').css({display:'inline-block'}); 
    });
    $('#btnadddate9').on('click', function() {
      $('#add_date').modal({backdrop: 'static', keyboard: false});
      $('#btnadddate9').css({display:'none'});
      $('#btnadddate10').css({display:'inline-block'}); 
    });
    $('#btnadddate10').on('click', function() {
      $('#add_date').modal({backdrop: 'static', keyboard: false});
      $('#btnadddate10').css({display:'none'});
      $('#btnadddate11').css({display:'inline-block'}); 
    });
    $('#btnadddate11').on('click', function() {
      $('#add_date').modal({backdrop: 'static', keyboard: false});
      $('#btnadddate11').css({display:'none'});
      $('#btnadddate12').css({display:'inline-block'}); 
    });
    $('#btnadddate12').on('click', function() {
      $('#add_date').modal({backdrop: 'static', keyboard: false});
      $('#btnadddate12').css({display:'none'});
      $('#btnadddate13').css({display:'inline-block'}); 
    });
    $('#btnadddate13').on('click', function() {
      $('#add_date').modal({backdrop: 'static', keyboard: false});
      $('#btnadddate13').css({display:'none'});
      $('#btnadddate14').css({display:'inline-block'}); 
    });
    $('#btnadddate14').on('click', function() {
      $('#add_date').modal({backdrop: 'static', keyboard: false});
      $('#btnadddate14').css({display:'none'});
      $('#btnadddate15').css({display:'inline-block'}); 
    });
    $('#btnadddate15').on('click', function() {
      $('#add_date').modal({backdrop: 'static', keyboard: false});
      $('#btnadddate15').css({display:'none'});
      $('#btnadddate2').css({display:'inline-block'});
      $('#btnadddate2').prop("disabled", true);
    });
    
      $('.btn_add2').on('click', function() {
         var new_date = $('#input_date').val();
         var datalength = $('#datalength').val();
         var loid = [];
         var u_id = [];
           for(var i=0; i<datalength; i++){
                loid[i]   = $('#lo_id'+i).val();
                u_id[i]   = $('#u_id'+i).val(); 
           }

          if (new_date != '') {

              $.ajax({
                    url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
                    method : "POST",
                    data : {loid: loid, u_id: u_id, att_time: new_date},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        
                       if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (new_date == $('#att_date8').val()) || (new_date == $('#att_date9').val()) || (new_date == $('#att_date10').val()) || (new_date == $('#att_date11').val()) || (new_date == $('#att_date12').val()) || (new_date == $('#att_date13').val()) || (new_date == $('#att_date14').val()) || (new_date == $('#att_date15').val()) || (data.length > 0) ) 
                        { alert('Date already exists !'); }
                       else{
                           $('#add_date').modal('hide');
                           var newformat = new Date(new_date).toLocaleDateString('en-US', {
                              weekday : 'long',
                              day : 'numeric',
                              month : 'short',
                              year : 'numeric'
                          }).split(' ').join(' ');
                          var day = new Date(new_date).toLocaleDateString('en-US', {
                              weekday : 'long'
                          }).split(' ').join(' ');
                          for(var i=0; i<datalength; i++){
                                var uid_id = $('#u_id'+i).val();
                                $('#remove_array'+i).append('<div class="added_row" id="div_removearray2_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid2[]" id="removeid2<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival2[]" id="ival2_'+i+'" value="'+i+'"></center></div>');
                                $('#remove'+i).append('<div class="added_row" id="div_remove2_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn2('+i+')" class="remove"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                                  $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate2_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time2[]" id="att_date2" class="form-control" value="'+new_date+'">'+newformat+'</center><input type="hidden" name="holiday2[]" id="holiday2'+i+'"></div>');
                                  $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox2_'+i+'"><br><center><label for="am2'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am2'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm2'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm2'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period2[]" id="period2'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker2('+i+')" class="showothers_btn2'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker2('+i+')" class="hide hideothers_btn2'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id2'+i+'" style="display:none;"><select id="markers_id2'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                                  $('#addabsent'+i).append('<div class="added_row" id="div_addabsent2_'+i+'"><br><center><input type="number" name="absent2[]" id="absent2'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                                  if (day == 'Sunday') {
                                    $('#addot'+i).append('<div class="added_row" id="div_addot2_'+i+'"><br><center><input type="number" name="overtime2[]" value="8" class="form-control"></center></div>'); }
                                  else{
                                     $('#addot'+i).append('<div class="added_row" id="div_addot2_'+i+'"><br><center><input type="number" name="overtime2[]" id="overtime2'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                                };
                          $('#input_date').val(''); 
                          $('.btn_add2').css({display:'none'});
                          $('.btn_add3').css({display:'inline-block'});
                       }
                    } });
              }
          else{ alert('Empty field !'); }
      });

      $('.btn_add3').on('click', function() {
         var new_date = $('#input_date').val();
         var datalength = $('#datalength').val();
         var loid = [];
         var u_id = [];
           for(var i=0; i<datalength; i++){
                loid[i]   = $('#lo_id'+i).val();
                u_id[i]   = $('#u_id'+i).val(); 
           }
                  
          if (new_date != '') {

              $.ajax({
                    url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
                    method : "POST",
                    data : {loid: loid, u_id: u_id, att_time: new_date},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        
                       if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (new_date == $('#att_date8').val()) || (new_date == $('#att_date9').val()) || (new_date == $('#att_date10').val()) || (new_date == $('#att_date11').val()) || (new_date == $('#att_date12').val()) || (new_date == $('#att_date13').val()) || (new_date == $('#att_date14').val()) || (new_date == $('#att_date15').val()) || (data.length > 0) ) 
                        { alert('Date already exists !'); }
                       else{
                           $('#add_date').modal('hide');
                           var newformat = new Date(new_date).toLocaleDateString('en-US', {
                              weekday : 'long',
                              day : 'numeric',
                              month : 'short',
                              year : 'numeric'
                          }).split(' ').join(' ');
                           var day = new Date(new_date).toLocaleDateString('en-US', {
                              weekday : 'long'
                          }).split(' ').join(' ');
                          for(var i=0; i<datalength; i++){
                                  var uid_id = $('#u_id'+i).val();
                                  $('#remove_array'+i).append('<div class="added_row" id="div_removearray3_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid3[]" id="removeid3<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival3[]" id="ival3_'+i+'" value="'+i+'"></center></div>');
                                  $('#remove'+i).append('<div class="added_row" id="div_remove3_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn3('+i+')"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                                   $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate3_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time3[]" id="att_date3" class="form-control" value="'+new_date+'">'+newformat+' </center><input type="hidden" name="holiday3[]" id="holiday3'+i+'"></div>');
                                    $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox3_'+i+'"><br><center><label for="am3'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am3'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm3'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm3'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period3[]" id="period3'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker3('+i+')" class="showothers_btn3'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker3('+i+')" class="hide hideothers_btn3'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id3'+i+'" style="display:none;"><select id="markers_id3'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                                  $('#addabsent'+i).append('<div class="added_row" id="div_addabsent3_'+i+'"><br><center><input type="number" name="absent3[]" id="absent3'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                                  if (day == 'Sunday') {
                                    $('#addot'+i).append('<div class="added_row" id="div_addot3_'+i+'"><br><center><input type="number" name="overtime3[]" value="8" class="form-control"></center></div>'); }
                                  else{
                                     $('#addot'+i).append('<div class="added_row" id="div_addot3_'+i+'"><br><center><input type="number" name="overtime3[]" id="overtime3'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                                  };
                            $('#input_date').val('');
                            $('.btn_add3').css({display:'none'});
                            $('.btn_add4').css({display:'inline-block'});
                       }
                    } });
              }
          else{ alert('Empty field !'); }
      });

      $('.btn_add4').on('click', function() {
         var new_date = $('#input_date').val();
         var datalength = $('#datalength').val();
         var loid = [];
         var u_id = [];
           for(var i=0; i<datalength; i++){
                loid[i]   = $('#lo_id'+i).val();
                u_id[i]   = $('#u_id'+i).val(); 
           }
                  
          if (new_date != '') {

              $.ajax({
                    url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
                    method : "POST",
                    data : {loid: loid, u_id: u_id, att_time: new_date},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        
                       if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (new_date == $('#att_date8').val()) || (new_date == $('#att_date9').val()) || (new_date == $('#att_date10').val()) || (new_date == $('#att_date11').val()) || (new_date == $('#att_date12').val()) || (new_date == $('#att_date13').val()) || (new_date == $('#att_date14').val()) || (new_date == $('#att_date15').val()) || (data.length > 0) ) 
                        { alert('Date already exists !'); }
                       else{
                           $('#add_date').modal('hide');
                           var newformat = new Date(new_date).toLocaleDateString('en-US', {
                              weekday : 'long',
                              day : 'numeric',
                              month : 'short',
                              year : 'numeric'
                          }).split(' ').join(' ');
                           var day = new Date(new_date).toLocaleDateString('en-US', {
                              weekday : 'long'
                          }).split(' ').join(' ');
                          for(var i=0; i<datalength; i++){
                                  var uid_id = $('#u_id'+i).val();
                                  $('#remove_array'+i).append('<div class="added_row" id="div_removearray4_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid4[]" id="removeid4<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival4[]" id="ival4_'+i+'" value="'+i+'"></center></div>');
                                  $('#remove'+i).append('<div class="added_row" id="div_remove4_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn4('+i+')" class="remove"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                                   $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate4_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time4[]" id="att_date4" class="form-control" value="'+new_date+'">'+newformat+' </center><input type="hidden" name="holiday4[]" id="holiday4'+i+'"></div>');
                                    $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox4_'+i+'"><br><center><label for="am4'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am4'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm4'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm4'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period4[]" id="period4'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker4('+i+')" class="showothers_btn4'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker4('+i+')" class="hide hideothers_btn4'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id4'+i+'" style="display:none;"><select id="markers_id4'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                                  $('#addabsent'+i).append('<div class="added_row" id="div_addabsent4_'+i+'"><br><center><input type="number" name="absent4[]" id="absent4'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                                  if (day == 'Sunday') {
                                    $('#addot'+i).append('<div class="added_row" id="div_addot4_'+i+'"><br><center><input type="number" name="overtime4[]" value="8" class="form-control"></center></div>'); }
                                  else{
                                     $('#addot'+i).append('<div class="added_row" id="div_addot4_'+i+'"><br><center><input type="number" name="overtime4[]" id="overtime4'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                                  };
                            $('#input_date').val('');
                            $('.btn_add4').css({display:'none'});
                            $('.btn_add5').css({display:'inline-block'});
                       }
                    } });
              }
          else{ alert('Empty field !'); }
      });

      $('.btn_add5').on('click', function() {
         var new_date = $('#input_date').val();
         var datalength = $('#datalength').val();
         var loid = [];
         var u_id = [];
           for(var i=0; i<datalength; i++){
                loid[i]   = $('#lo_id'+i).val();
                u_id[i]   = $('#u_id'+i).val(); 
           }
                  
          if (new_date != '') {

              $.ajax({
                    url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
                    method : "POST",
                    data : {loid: loid, u_id: u_id, att_time: new_date},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        
                       if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (new_date == $('#att_date8').val()) || (new_date == $('#att_date9').val()) || (new_date == $('#att_date10').val()) || (new_date == $('#att_date11').val()) || (new_date == $('#att_date12').val()) || (new_date == $('#att_date13').val()) || (new_date == $('#att_date14').val()) || (new_date == $('#att_date15').val()) || (data.length > 0) ) 
                        { alert('Date already exists !'); }
                       else{
                           $('#add_date').modal('hide');
                           var newformat = new Date(new_date).toLocaleDateString('en-US', {
                              weekday : 'long',
                              day : 'numeric',
                              month : 'short',
                              year : 'numeric'
                          }).split(' ').join(' ');
                           var day = new Date(new_date).toLocaleDateString('en-US', {
                              weekday : 'long'
                          }).split(' ').join(' ');
                          for(var i=0; i<datalength; i++){
                                    var uid_id = $('#u_id'+i).val();
                                    $('#remove_array'+i).append('<div class="added_row" id="div_removearray5_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid5[]" id="removeid5<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival5[]" id="ival5_'+i+'" value="'+i+'"></center></div>');
                                    $('#remove'+i).append('<div class="added_row" id="div_remove5_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn5('+i+')" class="remove"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                                    $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate5_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time5[]" id="att_date5" class="form-control" value="'+new_date+'">'+newformat+' </center><input type="hidden" name="holiday5[]" id="holiday5'+i+'"></div>');
                                    $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox5_'+i+'"><br><center><label for="am5'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am5'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm5'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm5'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period5[]" id="period5'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker5('+i+')" class="showothers_btn5'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker5('+i+')" class="hide hideothers_btn5'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id5'+i+'" style="display:none;"><select id="markers_id5'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                                  $('#addabsent'+i).append('<div class="added_row" id="div_addabsent5_'+i+'"><br><center><input type="number" name="absent5[]" id="absent5'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                                  if (day == 'Sunday') {
                                    $('#addot'+i).append('<div class="added_row" id="div_addot5_'+i+'"><br><center><input type="number" name="overtime5[]" value="8" class="form-control"></center></div>'); }
                                  else{
                                    $('#addot'+i).append('<div class="added_row" id="div_addot5_'+i+'"><br><center><input type="number" name="overtime5[]" id="overtime5'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                                  };
                            $('#input_date').val('');
                            $('.btn_add5').css({display:'none'});
                            $('.btn_add6').css({display:'inline-block'});
                       }
                    } });
              }
          else{ alert('Empty field !'); }
      });

      $('.btn_add6').on('click', function() {
         var new_date = $('#input_date').val();
         var datalength = $('#datalength').val();
         var loid = [];
         var u_id = [];
           for(var i=0; i<datalength; i++){
                loid[i]   = $('#lo_id'+i).val();
                u_id[i]   = $('#u_id'+i).val(); 
           }
                  
          if (new_date != '') {

              $.ajax({
                    url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
                    method : "POST",
                    data : {loid: loid, u_id: u_id, att_time: new_date},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        
                       if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (data.length > 0) ) 
                        { alert('Date already exists !'); }
                       else{
                           $('#add_date').modal('hide');
                           var newformat = new Date(new_date).toLocaleDateString('en-US', {
                              weekday : 'long',
                              day : 'numeric',
                              month : 'short',
                              year : 'numeric'
                          }).split(' ').join(' ');
                           var day = new Date(new_date).toLocaleDateString('en-US', {
                              weekday : 'long'
                          }).split(' ').join(' ');
                          for(var i=0; i<datalength; i++){
                                    var uid_id = $('#u_id'+i).val();
                                    $('#remove_array'+i).append('<div class="added_row" id="div_removearray6_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid6[]" id="removeid6<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival6[]" id="ival6_'+i+'" value="'+i+'"></center></div>');
                                    $('#remove'+i).append('<div class="added_row" id="div_remove6_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn6('+i+')" class="remove"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                                    $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate6_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time6[]" id="att_date6" class="form-control" value="'+new_date+'">'+newformat+' </center><input type="hidden" name="holiday6[]" id="holiday6'+i+'"></div>');
                                    $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox6_'+i+'"><br><center><label for="am6'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am6'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm6'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm6'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period6[]" id="period6'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker6('+i+')" class="showothers_btn6'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker6('+i+')" class="hide hideothers_btn6'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id6'+i+'" style="display:none;"><select id="markers_id6'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                                  $('#addabsent'+i).append('<div class="added_row" id="div_addabsent6_'+i+'"><br><center><input type="number" name="absent6[]" id="absent6'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                                  if (day == 'Sunday') {
                                    $('#addot'+i).append('<div class="added_row" id="div_addot6_'+i+'"><br><center><input type="number" name="overtime6[]" value="8" class="form-control"></center></div>'); }
                                  else{
                                    $('#addot'+i).append('<div class="added_row" id="div_addot6_'+i+'"><br><center><input type="number" name="overtime6[]" id="overtime6'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                                  };
                            $('#input_date').val('');
                            $('.btn_add6').css({display:'none'});
                            $('.btn_add7').css({display:'inline-block'});
                       }
                    } });
              }
          else{ alert('Empty field !'); }
      });

      $('.btn_add7').on('click', function() {
         var new_date = $('#input_date').val();
         var datalength = $('#datalength').val();
         var loid = [];
         var u_id = [];
           for(var i=0; i<datalength; i++){
                loid[i]   = $('#lo_id'+i).val();
                u_id[i]   = $('#u_id'+i).val(); 
           }
                  
          if (new_date != '') {

              $.ajax({
                    url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
                    method : "POST",
                    data : {loid: loid, u_id: u_id, att_time: new_date},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        
                       if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (new_date == $('#att_date8').val()) || (new_date == $('#att_date9').val()) || (new_date == $('#att_date10').val()) || (new_date == $('#att_date11').val()) || (new_date == $('#att_date12').val()) || (new_date == $('#att_date13').val()) || (new_date == $('#att_date14').val()) || (new_date == $('#att_date15').val()) || (data.length > 0) ) 
                        { alert('Date already exists !'); }
                       else{
                           $('#add_date').modal('hide');
                           var newformat = new Date(new_date).toLocaleDateString('en-US', {
                              weekday : 'long',
                              day : 'numeric',
                              month : 'short',
                              year : 'numeric'
                          }).split(' ').join(' ');
                          var day = new Date(new_date).toLocaleDateString('en-US', {
                              weekday : 'long'
                          }).split(' ').join(' ');
                          for(var i=0; i<datalength; i++){
                                    var uid_id = $('#u_id'+i).val();
                                    $('#remove_array'+i).append('<div class="added_row" id="div_removearray7_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid7[]" id="removeid7<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival7[]" id="ival7_'+i+'" value="'+i+'"></center></div>');
                                    $('#remove'+i).append('<div class="added_row" id="div_remove7_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn7('+i+')" class="remove"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                                     $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate7_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time7[]" id="att_date7" class="form-control" value="'+new_date+'">'+newformat+' </center><input type="hidden" name="holiday7[]" id="holiday7'+i+'"></div>');
                                      $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox7_'+i+'"><br><center><label for="am7'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am7'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm7'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm7'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period7[]" id="period7'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker7('+i+')" class="showothers_btn7'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker7('+i+')" class="hide hideothers_btn7'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id7'+i+'" style="display:none;"><select id="markers_id7'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                                      $('#addabsent'+i).append('<div class="added_row" id="div_addabsent7_'+i+'"><br><center><input type="number" name="absent7[]" id="absent7'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                                  if (day == 'Sunday') {
                                    $('#addot'+i).append('<div class="added_row" id="div_addot7_'+i+'"><br><center><input type="number" name="overtime7[]" value="8" class="form-control"></center></div>'); }
                                  else{
                                    $('#addot'+i).append('<div class="added_row" id="div_addot7_'+i+'"><br><center><input type="number" name="overtime7[]" id="overtime7'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                                    };
                              $('#input_date').val('');
                              $('.btn_add7').css({display:'none'});
                              $('.btn_add8').css({display:'inline-block'});
                       }
                    } });
              }
          else{ alert('Empty field !'); }
      });

      $('.btn_add8').on('click', function() {
        var new_date = $('#input_date').val();
        var datalength = $('#datalength').val();
        var loid = [];
        var u_id = [];
        for(var i=0; i<datalength; i++){
          loid[i]   = $('#lo_id'+i).val();
          u_id[i]   = $('#u_id'+i).val(); 
        }
                  
        if (new_date != '') {
          $.ajax({
            url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
            method : "POST",
            data : {loid: loid, u_id: u_id, att_time: new_date},
            async : true,
            dataType : 'json',
            success: function(data){
                    
              if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (new_date == $('#att_date8').val()) || (new_date == $('#att_date9').val()) || (new_date == $('#att_date10').val()) || (new_date == $('#att_date11').val()) || (new_date == $('#att_date12').val()) || (new_date == $('#att_date13').val()) || (new_date == $('#att_date14').val()) || (new_date == $('#att_date15').val()) || (data.length > 0) ) 
              { alert('Date already exists !'); }
              else{
                $('#add_date').modal('hide');
                var newformat = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long',
                  day : 'numeric',
                  month : 'short',
                  year : 'numeric'
                }).split(' ').join(' ');
                var day = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long'
                }).split(' ').join(' ');
                for(var i=0; i<datalength; i++){
                  var uid_id = $('#u_id'+i).val();
                  $('#remove_array'+i).append('<div class="added_row" id="div_removearray8_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid8[]" id="removeid8<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival8[]" id="ival8_'+i+'" value="'+i+'"></center></div>');
                  $('#remove'+i).append('<div class="added_row" id="div_remove8_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn8('+i+')" class="remove"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                  $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate8_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time8[]" id="att_date8" class="form-control" value="'+new_date+'">'+newformat+' </center><input type="hidden" name="holiday8[]" id="holiday8'+i+'"></div>');
                  $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox8_'+i+'"><br><center><label for="am8'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am8'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm8'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm8'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period8[]" id="period8'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker8('+i+')" class="showothers_btn8'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker8('+i+')" class="hide hideothers_btn8'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id8'+i+'" style="display:none;"><select id="markers_id8'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                $('#addabsent'+i).append('<div class="added_row" id="div_addabsent8_'+i+'"><br><center><input type="number" name="absent8[]" id="absent8'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                if (day == 'Sunday') {
                  $('#addot'+i).append('<div class="added_row" id="div_addot8_'+i+'"><br><center><input type="number" name="overtime8[]" value="8" class="form-control"></center></div>'); }
                else{
                  $('#addot'+i).append('<div class="added_row" id="div_addot8_'+i+'"><br><center><input type="number" name="overtime8[]" id="overtime8'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                };
                      $('#input_date').val('');
                      $('.btn_add8').css({display:'none'});
                      $('.btn_add9').css({display:'inline-block'});
              }
            } 
          });
        }
        else{ alert('Empty field !'); }
      });

      $('.btn_add9').on('click', function() {
        var new_date = $('#input_date').val();
        var datalength = $('#datalength').val();
        var loid = [];
        var u_id = [];
        for(var i=0; i<datalength; i++){
          loid[i]   = $('#lo_id'+i).val();
          u_id[i]   = $('#u_id'+i).val(); 
        }
                  
        if (new_date != '') {
          $.ajax({
            url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
            method : "POST",
            data : {loid: loid, u_id: u_id, att_time: new_date},
            async : true,
            dataType : 'json',
            success: function(data){
                    
              if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (new_date == $('#att_date8').val()) || (new_date == $('#att_date9').val()) || (new_date == $('#att_date10').val()) || (new_date == $('#att_date11').val()) || (new_date == $('#att_date12').val()) || (new_date == $('#att_date13').val()) || (new_date == $('#att_date14').val()) || (new_date == $('#att_date15').val()) || (data.length > 0) ) 
              { alert('Date already exists !'); }
              else{
                $('#add_date').modal('hide');
                var newformat = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long',
                  day : 'numeric',
                  month : 'short',
                  year : 'numeric'
                }).split(' ').join(' ');
                var day = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long'
                }).split(' ').join(' ');
                for(var i=0; i<datalength; i++){
                  var uid_id = $('#u_id'+i).val();
                  $('#remove_array'+i).append('<div class="added_row" id="div_removearray9_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid9[]" id="removeid9<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival9[]" id="ival9_'+i+'" value="'+i+'"></center></div>');
                  $('#remove'+i).append('<div class="added_row" id="div_remove9_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn9('+i+')" class="remove"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                  $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate9_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time9[]" id="att_date9" class="form-control" value="'+new_date+'">'+newformat+' </center><input type="hidden" name="holiday9[]" id="holiday9'+i+'"></div>');
                  $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox9_'+i+'"><br><center><label for="am9'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am9'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm9'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm9'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period9[]" id="period9'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker9('+i+')" class="showothers_btn9'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker9('+i+')" class="hide hideothers_btn9'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id9'+i+'" style="display:none;"><select id="markers_id9'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                $('#addabsent'+i).append('<div class="added_row" id="div_addabsent9_'+i+'"><br><center><input type="number" name="absent9[]" id="absent9'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                if (day == 'Sunday') {
                  $('#addot'+i).append('<div class="added_row" id="div_addot9_'+i+'"><br><center><input type="number" name="overtime9[]" value="8" class="form-control"></center></div>'); }
                else{
                  $('#addot'+i).append('<div class="added_row" id="div_addot9_'+i+'"><br><center><input type="number" name="overtime9[]" id="overtime9'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                };
                      $('#input_date').val('');
                      $('.btn_add9').css({display:'none'});
                      $('.btn_add10').css({display:'inline-block'});
              }
            } 
          });
        }
        else{ alert('Empty field !'); }
      });

      $('.btn_add10').on('click', function() {
        var new_date = $('#input_date').val();
        var datalength = $('#datalength').val();
        var loid = [];
        var u_id = [];
        for(var i=0; i<datalength; i++){
          loid[i]   = $('#lo_id'+i).val();
          u_id[i]   = $('#u_id'+i).val(); 
        }
                  
        if (new_date != '') {
          $.ajax({
            url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
            method : "POST",
            data : {loid: loid, u_id: u_id, att_time: new_date},
            async : true,
            dataType : 'json',
            success: function(data){
                    
              if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (new_date == $('#att_date8').val()) || (new_date == $('#att_date9').val()) || (new_date == $('#att_date10').val()) || (new_date == $('#att_date11').val()) || (new_date == $('#att_date12').val()) || (new_date == $('#att_date13').val()) || (new_date == $('#att_date14').val()) || (new_date == $('#att_date15').val()) || (data.length > 0) ) 
              { alert('Date already exists !'); }
              else{
                $('#add_date').modal('hide');
                var newformat = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long',
                  day : 'numeric',
                  month : 'short',
                  year : 'numeric'
                }).split(' ').join(' ');
                var day = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long'
                }).split(' ').join(' ');
                for(var i=0; i<datalength; i++){
                  var uid_id = $('#u_id'+i).val();
                  $('#remove_array'+i).append('<div class="added_row" id="div_removearray10_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid10[]" id="removeid10<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival10[]" id="ival10_'+i+'" value="'+i+'"></center></div>');
                  $('#remove'+i).append('<div class="added_row" id="div_remove10_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn10('+i+')" class="remove"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                  $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate10_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time10[]" id="att_date10" class="form-control" value="'+new_date+'">'+newformat+' </center><input type="hidden" name="holiday10[]" id="holiday10'+i+'"></div>');
                  $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox10_'+i+'"><br><center><label for="am10'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am10'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm10'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm10'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period10[]" id="period10'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker10('+i+')" class="showothers_btn10'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker10('+i+')" class="hide hideothers_btn10'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id10'+i+'" style="display:none;"><select id="markers_id10'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                $('#addabsent'+i).append('<div class="added_row" id="div_addabsent10_'+i+'"><br><center><input type="number" name="absent10[]" id="absent10'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                if (day == 'Sunday') {
                  $('#addot'+i).append('<div class="added_row" id="div_addot10_'+i+'"><br><center><input type="number" name="overtime10[]" value="8" class="form-control"></center></div>'); }
                else{
                  $('#addot'+i).append('<div class="added_row" id="div_addot10_'+i+'"><br><center><input type="number" name="overtime10[]" id="overtime10'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                };
                      $('#input_date').val('');
                      $('.btn_add10').css({display:'none'});
                      $('.btn_add11').css({display:'inline-block'});
              }
            } 
          });
        }
        else{ alert('Empty field !'); }
      });

      $('.btn_add11').on('click', function() {
        var new_date = $('#input_date').val();
        var datalength = $('#datalength').val();
        var loid = [];
        var u_id = [];
        for(var i=0; i<datalength; i++){
          loid[i]   = $('#lo_id'+i).val();
          u_id[i]   = $('#u_id'+i).val(); 
        }
                  
        if (new_date != '') {
          $.ajax({
            url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
            method : "POST",
            data : {loid: loid, u_id: u_id, att_time: new_date},
            async : true,
            dataType : 'json',
            success: function(data){
                    
              if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (new_date == $('#att_date8').val()) || (new_date == $('#att_date9').val()) || (new_date == $('#att_date10').val()) || (new_date == $('#att_date11').val()) || (new_date == $('#att_date12').val()) || (new_date == $('#att_date13').val()) || (new_date == $('#att_date14').val()) || (new_date == $('#att_date15').val()) || (data.length > 0) ) 
              { alert('Date already exists !'); }
              else{
                $('#add_date').modal('hide');
                var newformat = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long',
                  day : 'numeric',
                  month : 'short',
                  year : 'numeric'
                }).split(' ').join(' ');
                var day = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long'
                }).split(' ').join(' ');
                for(var i=0; i<datalength; i++){
                  var uid_id = $('#u_id'+i).val();
                  $('#remove_array'+i).append('<div class="added_row" id="div_removearray11_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid11[]" id="removeid11<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival11[]" id="ival11_'+i+'" value="'+i+'"></center></div>');
                  $('#remove'+i).append('<div class="added_row" id="div_remove11_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn11('+i+')" class="remove"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                  $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate11_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time11[]" id="att_date11" class="form-control" value="'+new_date+'">'+newformat+' </center><input type="hidden" name="holiday11[]" id="holiday11'+i+'"></div>');
                  $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox11_'+i+'"><br><center><label for="am11'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am11'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm11'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm11'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period11[]" id="period11'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker11('+i+')" class="showothers_btn11'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker11('+i+')" class="hide hideothers_btn11'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id11'+i+'" style="display:none;"><select id="markers_id11'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                $('#addabsent'+i).append('<div class="added_row" id="div_addabsent11_'+i+'"><br><center><input type="number" name="absent11[]" id="absent11'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                if (day == 'Sunday') {
                  $('#addot'+i).append('<div class="added_row" id="div_addot11_'+i+'"><br><center><input type="number" name="overtime11[]" value="8" class="form-control"></center></div>'); }
                else{
                  $('#addot'+i).append('<div class="added_row" id="div_addot11_'+i+'"><br><center><input type="number" name="overtime11[]" id="overtime11'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                };
                      $('#input_date').val('');
                      $('.btn_add11').css({display:'none'});
                      $('.btn_add12').css({display:'inline-block'});
              }
            } 
          });
        }
        else{ alert('Empty field !'); }
      });

      $('.btn_add12').on('click', function() {
        var new_date = $('#input_date').val();
        var datalength = $('#datalength').val();
        var loid = [];
        var u_id = [];
        for(var i=0; i<datalength; i++){
          loid[i]   = $('#lo_id'+i).val();
          u_id[i]   = $('#u_id'+i).val(); 
        }
                  
        if (new_date != '') {
          $.ajax({
            url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
            method : "POST",
            data : {loid: loid, u_id: u_id, att_time: new_date},
            async : true,
            dataType : 'json',
            success: function(data){
                    
              if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (new_date == $('#att_date8').val()) || (new_date == $('#att_date9').val()) || (new_date == $('#att_date10').val()) || (new_date == $('#att_date11').val()) || (new_date == $('#att_date12').val()) || (new_date == $('#att_date13').val()) || (new_date == $('#att_date14').val()) || (new_date == $('#att_date15').val()) || (data.length > 0) ) 
              { alert('Date already exists !'); }
              else{
                $('#add_date').modal('hide');
                var newformat = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long',
                  day : 'numeric',
                  month : 'short',
                  year : 'numeric'
                }).split(' ').join(' ');
                var day = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long'
                }).split(' ').join(' ');
                for(var i=0; i<datalength; i++){
                  var uid_id = $('#u_id'+i).val();
                  $('#remove_array'+i).append('<div class="added_row" id="div_removearray12_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid12[]" id="removeid12<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival12[]" id="ival12_'+i+'" value="'+i+'"></center></div>');
                  $('#remove'+i).append('<div class="added_row" id="div_remove12_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn12('+i+')" class="remove"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                  $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate12_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time12[]" id="att_date12" class="form-control" value="'+new_date+'">'+newformat+' </center><input type="hidden" name="holiday12[]" id="holiday12'+i+'"></div>');
                  $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox12_'+i+'"><br><center><label for="am12'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am12'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm12'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm12'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period12[]" id="period12'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker12('+i+')" class="showothers_btn12'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker12('+i+')" class="hide hideothers_btn12'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id12'+i+'" style="display:none;"><select id="markers_id12'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                $('#addabsent'+i).append('<div class="added_row" id="div_addabsent12_'+i+'"><br><center><input type="number" name="absent12[]" id="absent12'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                if (day == 'Sunday') {
                  $('#addot'+i).append('<div class="added_row" id="div_addot12_'+i+'"><br><center><input type="number" name="overtime12[]" value="8" class="form-control"></center></div>'); }
                else{
                  $('#addot'+i).append('<div class="added_row" id="div_addot12_'+i+'"><br><center><input type="number" name="overtime12[]" id="overtime12'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                };
                      $('#input_date').val('');
                      $('.btn_add12').css({display:'none'});
                      $('.btn_add13').css({display:'inline-block'});
              }
            } 
          });
        }
        else{ alert('Empty field !'); }
      });

      $('.btn_add13').on('click', function() {
        var new_date = $('#input_date').val();
        var datalength = $('#datalength').val();
        var loid = [];
        var u_id = [];
        for(var i=0; i<datalength; i++){
          loid[i]   = $('#lo_id'+i).val();
          u_id[i]   = $('#u_id'+i).val(); 
        }
                  
        if (new_date != '') {
          $.ajax({
            url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
            method : "POST",
            data : {loid: loid, u_id: u_id, att_time: new_date},
            async : true,
            dataType : 'json',
            success: function(data){
                    
              if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (new_date == $('#att_date8').val()) || (new_date == $('#att_date9').val()) || (new_date == $('#att_date10').val()) || (new_date == $('#att_date11').val()) || (new_date == $('#att_date12').val()) || (new_date == $('#att_date13').val()) || (new_date == $('#att_date14').val()) || (new_date == $('#att_date15').val()) || (data.length > 0) ) 
              { alert('Date already exists !'); }
              else{
                $('#add_date').modal('hide');
                var newformat = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long',
                  day : 'numeric',
                  month : 'short',
                  year : 'numeric'
                }).split(' ').join(' ');
                var day = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long'
                }).split(' ').join(' ');
                for(var i=0; i<datalength; i++){
                  var uid_id = $('#u_id'+i).val();
                  $('#remove_array'+i).append('<div class="added_row" id="div_removearray13_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid13[]" id="removeid13<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival13[]" id="ival13_'+i+'" value="'+i+'"></center></div>');
                  $('#remove'+i).append('<div class="added_row" id="div_remove13_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn13('+i+')" class="remove"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                  $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate13_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time13[]" id="att_date13" class="form-control" value="'+new_date+'">'+newformat+' </center><input type="hidden" name="holiday13[]" id="holiday13'+i+'"></div>');
                  $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox13_'+i+'"><br><center><label for="am13'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am13'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm13'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm13'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period13[]" id="period13'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker13('+i+')" class="showothers_btn13'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker13('+i+')" class="hide hideothers_btn13'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id13'+i+'" style="display:none;"><select id="markers_id13'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                $('#addabsent'+i).append('<div class="added_row" id="div_addabsent13_'+i+'"><br><center><input type="number" name="absent13[]" id="absent13'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                if (day == 'Sunday') {
                  $('#addot'+i).append('<div class="added_row" id="div_addot13_'+i+'"><br><center><input type="number" name="overtime13[]" value="8" class="form-control"></center></div>'); }
                else{
                  $('#addot'+i).append('<div class="added_row" id="div_addot13_'+i+'"><br><center><input type="number" name="overtime13[]" id="overtime13'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                };
                      $('#input_date').val('');
                      $('.btn_add13').css({display:'none'});
                      $('.btn_add14').css({display:'inline-block'});
              }
            } 
          });
        }
        else{ alert('Empty field !'); }
      });

      $('.btn_add14').on('click', function() {
        var new_date = $('#input_date').val();
        var datalength = $('#datalength').val();
        var loid = [];
        var u_id = [];
        for(var i=0; i<datalength; i++){
          loid[i]   = $('#lo_id'+i).val();
          u_id[i]   = $('#u_id'+i).val(); 
        }
                  
        if (new_date != '') {
          $.ajax({
            url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
            method : "POST",
            data : {loid: loid, u_id: u_id, att_time: new_date},
            async : true,
            dataType : 'json',
            success: function(data){
                    
              if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (new_date == $('#att_date8').val()) || (new_date == $('#att_date9').val()) || (new_date == $('#att_date10').val()) || (new_date == $('#att_date11').val()) || (new_date == $('#att_date12').val()) || (new_date == $('#att_date13').val()) || (new_date == $('#att_date14').val()) || (new_date == $('#att_date15').val()) || (data.length > 0) ) 
              { alert('Date already exists !'); }
              else{
                $('#add_date').modal('hide');
                var newformat = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long',
                  day : 'numeric',
                  month : 'short',
                  year : 'numeric'
                }).split(' ').join(' ');
                var day = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long'
                }).split(' ').join(' ');
                for(var i=0; i<datalength; i++){
                  var uid_id = $('#u_id'+i).val();
                  $('#remove_array'+i).append('<div class="added_row" id="div_removearray14_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid14[]" id="removeid14<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival14[]" id="ival14_'+i+'" value="'+i+'"></center></div>');
                  $('#remove'+i).append('<div class="added_row" id="div_remove14_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn14('+i+')" class="remove"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                  $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate14_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time14[]" id="att_date14" class="form-control" value="'+new_date+'">'+newformat+' </center><input type="hidden" name="holiday14[]" id="holiday14'+i+'"></div>');
                  $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox14_'+i+'"><br><center><label for="am14'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am14'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm14'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm14'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period14[]" id="period14'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker14('+i+')" class="showothers_btn14'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker14('+i+')" class="hide hideothers_btn14'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id14'+i+'" style="display:none;"><select id="markers_id14'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                $('#addabsent'+i).append('<div class="added_row" id="div_addabsent14_'+i+'"><br><center><input type="number" name="absent14[]" id="absent14'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                if (day == 'Sunday') {
                  $('#addot'+i).append('<div class="added_row" id="div_addot14_'+i+'"><br><center><input type="number" name="overtime14[]" value="8" class="form-control"></center></div>'); }
                else{
                  $('#addot'+i).append('<div class="added_row" id="div_addot14_'+i+'"><br><center><input type="number" name="overtime14[]" id="overtime14'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                };
                      $('#input_date').val('');
                      $('.btn_add14').css({display:'none'});
                      $('.btn_add15').css({display:'inline-block'});
              }
            } 
          });
        }
        else{ alert('Empty field !'); }
      });

      $('.btn_add15').on('click', function() {
        var new_date = $('#input_date').val();
        var datalength = $('#datalength').val();
        var loid = [];
        var u_id = [];
        for(var i=0; i<datalength; i++){
          loid[i]   = $('#lo_id'+i).val();
          u_id[i]   = $('#u_id'+i).val(); 
        }
                  
        if (new_date != '') {
          $.ajax({
            url : "<?php echo admin_url('attendance_form/checkweeklylog');?>",
            method : "POST",
            data : {loid: loid, u_id: u_id, att_time: new_date},
            async : true,
            dataType : 'json',
            success: function(data){
                    
              if ( (new_date == $('#att_date').val()) || (new_date == $('#att_date2').val()) || (new_date == $('#att_date3').val()) || (new_date == $('#att_date4').val()) || (new_date == $('#att_date5').val()) || (new_date == $('#att_date6').val()) || (new_date == $('#att_date7').val()) || (new_date == $('#att_date8').val()) || (new_date == $('#att_date9').val()) || (new_date == $('#att_date10').val()) || (new_date == $('#att_date11').val()) || (new_date == $('#att_date12').val()) || (new_date == $('#att_date13').val()) || (new_date == $('#att_date14').val()) || (new_date == $('#att_date15').val()) || (data.length > 0) ) 
              { alert('Date already exists !'); }
              else{
                $('#add_date').modal('hide');
                var newformat = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long',
                  day : 'numeric',
                  month : 'short',
                  year : 'numeric'
                }).split(' ').join(' ');
                var day = new Date(new_date).toLocaleDateString('en-US', {
                  weekday : 'long'
                }).split(' ').join(' ');
                for(var i=0; i<datalength; i++){
                  var uid_id = $('#u_id'+i).val();
                  $('#remove_array'+i).append('<div class="added_row" id="div_removearray15_'+i+'" style="margin-top: 10px;"><br><center><input type="text" name="removeid15[]" id="removeid15<?php echo $key ?>" value="'+uid_id+'"><input type="text" name="ival15[]" id="ival15_'+i+'" value="'+i+'"></center></div>');
                  $('#remove'+i).append('<div class="added_row" id="div_remove15_'+i+'" style="margin-top: 10px;"><br><center><span style="cursor:pointer;" onclick="removeBtn15('+i+')" class="remove"><i class="fa fa-window-close" style="font-size:25px;color:red"></i></span></center></div>');
                  $('#adddate'+i).append('<div class="added_row addedrows" id="div_adddate15_'+i+'" style="margin-top: 10px;"><br><center><input type="hidden" name="att_time15[]" id="att_date15" class="form-control" value="'+new_date+'">'+newformat+' </center><input type="hidden" name="holiday15[]" id="holiday15'+i+'"></div>');
                  $('#addcheckbox'+i).append('<div class="added_row" id="div_addcheckbox15_'+i+'"><br><center><label for="am15'+i+'" class="btn btn-custom btn-primary">AM &nbsp;&nbsp;<input type="checkbox" id="am15'+i+'" value="AM" class="badgebox"><span class="badge">&check;</span></label> &nbsp;&nbsp;<label for="pm15'+i+'" class="btn btn-custom btn-primary">PM &nbsp;&nbsp;<input type="checkbox" value="PM" id="pm15'+i+'" class="badgebox"><span class="badge">&check;</span></label><input type="hidden" name="period15[]" id="period15'+i+'">&emsp;<div style="cursor:pointer;display:inline-block;" onclick="show_marker15('+i+')" class="showothers_btn15'+i+'"><i class="fa fa-chevron-circle-right" style="font-size: 15px;"></i></div><div style="cursor:pointer;display:inline-block;" onclick="hide_marker15('+i+')" class="hide hideothers_btn15'+i+'"><i class="fa fa-chevron-circle-down" style="font-size: 15px;"></i></div><br><br><div id="divmarkers_id15'+i+'" style="display:none;"><select id="markers_id15'+i+'" class="form-control"><option value="">Nothing Selected</option><option value="onleave">On Leave</option><option value="sick">Sick</option><option value="excused">Excused</option><option value="holiday">Holiday</option><option value="awol">Absent Without Leave</option></select></div></center></div>');
                $('#addabsent'+i).append('<div class="added_row" id="div_addabsent15_'+i+'"><br><center><input type="number" name="absent15[]" id="absent15'+i+'" placeholder="Enter here" class="form-control"></center></div>');
                if (day == 'Sunday') {
                  $('#addot'+i).append('<div class="added_row" id="div_addot15_'+i+'"><br><center><input type="number" name="overtime15[]" value="8" class="form-control"></center></div>'); }
                else{
                  $('#addot'+i).append('<div class="added_row" id="div_addot15_'+i+'"><br><center><input type="number" name="overtime15[]" id="overtime15'+i+'" placeholder="Enter here" class="form-control"></center></div>');  }
                };
                      $('#input_date').val('');
                      $('.btn_add15').css({display:'none'});
                      $('.btn_add2').css({display:'inline-block'});
              }
            } 
          });
        }
        else{ alert('Empty field !'); }
      });
        
  });
</script>
<script type="text/javascript">
 
  $(document).ready(function(){

    $("#check_period").change(function(event){
        var period = $('#period0').val();
        var datalength = $('#datalength').val();
        if (this.checked){
            for(var i=0; i<datalength; i++){
              $('#period'+i).val(period);
              $('#period2'+i).val(period);
              $('#period3'+i).val(period);
              $('#period4'+i).val(period);
              $('#period5'+i).val(period);
              $('#period6'+i).val(period);
              $('#period7'+i).val(period);
              $('#period8'+i).val(period);
              $('#period9'+i).val(period);
              $('#period10'+i).val(period);
              $('#period11'+i).val(period);
              $('#period12'+i).val(period);
              $('#period13'+i).val(period);
              $('#period14'+i).val(period);
              $('#period15'+i).val(period);
              if ( $('#am0').is(":checked") ) {
                $('#am'+i).prop("checked", true);
                $('#am2'+i).prop("checked", true);
                $('#am3'+i).prop("checked", true);
                $('#am4'+i).prop("checked", true);
                $('#am5'+i).prop("checked", true);
                $('#am6'+i).prop("checked", true);
                $('#am7'+i).prop("checked", true);
                $('#am8'+i).prop("checked", true);
                $('#am9'+i).prop("checked", true);
                $('#am10'+i).prop("checked", true);
                $('#am11'+i).prop("checked", true);
                $('#am12'+i).prop("checked", true);
                $('#am13'+i).prop("checked", true);
                $('#am14'+i).prop("checked", true);
                $('#am15'+i).prop("checked", true);
              }
              if ( $('#pm0').is(":checked") ) {
                $('#pm'+i).prop("checked", true);
                $('#pm2'+i).prop("checked", true);
                $('#pm3'+i).prop("checked", true);
                $('#pm4'+i).prop("checked", true);
                $('#pm5'+i).prop("checked", true);
                $('#pm6'+i).prop("checked", true);
                $('#pm7'+i).prop("checked", true);
                $('#pm8'+i).prop("checked", true);
                $('#pm9'+i).prop("checked", true);
                $('#pm10'+i).prop("checked", true);
                $('#pm11'+i).prop("checked", true);
                $('#pm12'+i).prop("checked", true);
                $('#pm13'+i).prop("checked", true);
                $('#pm14'+i).prop("checked", true);
                $('#pm15'+i).prop("checked", true);
              }

            }
        } else {
            for(var i=0; i<datalength; i++){
              $('#period'+i).val('');
              $('#am'+i).prop("checked", false);
              $('#pm'+i).prop("checked", false);
              $('#period2'+i).val('');
              $('#am2'+i).prop("checked", false);
              $('#pm2'+i).prop("checked", false);
              $('#period3'+i).val('');
              $('#am3'+i).prop("checked", false);
              $('#pm3'+i).prop("checked", false);
              $('#period4'+i).val('');
              $('#am4'+i).prop("checked", false);
              $('#pm4'+i).prop("checked", false);
              $('#period5'+i).val('');
              $('#am5'+i).prop("checked", false);
              $('#pm5'+i).prop("checked", false);
              $('#period6'+i).val('');
              $('#am6'+i).prop("checked", false);
              $('#pm6'+i).prop("checked", false);
              $('#period7'+i).val('');
              $('#am7'+i).prop("checked", false);
              $('#pm7'+i).prop("checked", false);
              $('#period8'+i).val('');
              $('#am8'+i).prop("checked", false);
              $('#pm8'+i).prop("checked", false);
              $('#period9'+i).val('');
              $('#am9'+i).prop("checked", false);
              $('#pm9'+i).prop("checked", false);
              $('#period10'+i).val('');
              $('#am10'+i).prop("checked", false);
              $('#pm10'+i).prop("checked", false);
              $('#period11'+i).val('');
              $('#am11'+i).prop("checked", false);
              $('#pm11'+i).prop("checked", false);
              $('#period12'+i).val('');
              $('#am12'+i).prop("checked", false);
              $('#pm12'+i).prop("checked", false);
              $('#period13'+i).val('');
              $('#am13'+i).prop("checked", false);
              $('#pm13'+i).prop("checked", false);
              $('#period14'+i).val('');
              $('#am14'+i).prop("checked", false);
              $('#pm14'+i).prop("checked", false);
              $('#period15'+i).val('');
              $('#am15'+i).prop("checked", false);
              $('#pm15'+i).prop("checked", false);
            }
        }
    });

});
</script>
<script type="text/javascript">
 
        $(document).ready(function(){

           $('#butsave').on('click', function() {

            var datalength = $('#datalength').val();

            var regular = ['1/1','4/9','2020/4/9','2021/4/1','2022/4/14','2023/4/6','2024/3/28','2025/4/17','2020/4/10','2021/4/2','2022/4/15','2023/4/7','2024/3/29','2025/4/18','5/1','2020/5/24','2021/5/13','2022/5/3','2024/4/10','2025/3/31','6/12','2020/7/31','2021/7/20','2022/7/10','2023/6/29','2024/6/17','2025/6/7','2020/8/31','2021/8/30','2022/8/29','2023/8/28','2024/8/26','2025/8/25','11/30','12/25','12/30'];

            var special = ['2020/1/25','2021/2/12','2022/2/1','2023/1/22','2024/2/10','2025/1/29','2/25','2020/4/11','8/21','11/1','11/2','12/8','12/24','12/31'];

            var regular_holiday = false; var special_holiday = false; var regular_holiday2 = false; var special_holiday2 = false; var regular_holiday3 = false; var special_holiday3 = false; var regular_holiday4 = false; var special_holiday4 = false; var regular_holiday5 = false; var special_holiday5 = false; var regular_holiday6 = false; var special_holiday6 = false; var regular_holiday7 = false; var special_holiday7 = false; var regular_holiday8 = false; var special_holiday8 = false; var regular_holiday9 = false; var special_holiday9 = false; var regular_holiday10 = false; var special_holiday10 = false; var regular_holiday11 = false; var special_holiday11 = false; var regular_holiday12 = false; var special_holiday12 = false; var regular_holiday13 = false; var special_holiday13 = false; var regular_holiday14 = false; var special_holiday14 = false; var regular_holiday15 = false; var special_holiday15 = false;

            var attdate_day = new Date($('#att_date').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year = new Date($('#att_date').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate = attdate_year+'/'+attdate_day;
            var attdate_day2 = new Date($('#att_date2').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year2 = new Date($('#att_date2').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate2 = attdate_year2+'/'+attdate_day2;
            var attdate_day3 = new Date($('#att_date3').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year3 = new Date($('#att_date3').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate3 = attdate_year3+'/'+attdate_day3;
            var attdate_day4 = new Date($('#att_date4').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year4 = new Date($('#att_date4').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate4 = attdate_year4+'/'+attdate_day4;
            var attdate_day5 = new Date($('#att_date5').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year5 = new Date($('#att_date5').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate5 = attdate_year5+'/'+attdate_day5;
            var attdate_day6 = new Date($('#att_date6').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year6 = new Date($('#att_date6').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate6 = attdate_year6+'/'+attdate_day6;
            var attdate_day7 = new Date($('#att_date7').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year7 = new Date($('#att_date7').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate7 = attdate_year7+'/'+attdate_day7;

            var attdate_day8 = new Date($('#att_date8').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year8 = new Date($('#att_date8').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate8 = attdate_year8+'/'+attdate_day8;
            var attdate_day9 = new Date($('#att_date9').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year9 = new Date($('#att_date9').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate9 = attdate_year9+'/'+attdate_day9;
            var attdate_day10 = new Date($('#att_date10').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year10 = new Date($('#att_date10').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate10 = attdate_year10+'/'+attdate_day10;
            var attdate_day11 = new Date($('#att_date11').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year11 = new Date($('#att_date11').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate11 = attdate_year11+'/'+attdate_day11;
            var attdate_day12 = new Date($('#att_date12').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year12 = new Date($('#att_date12').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate12 = attdate_year12+'/'+attdate_day12;
            var attdate_day13 = new Date($('#att_date13').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year13 = new Date($('#att_date13').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate13 = attdate_year13+'/'+attdate_day13;
            var attdate_day14 = new Date($('#att_date14').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year14 = new Date($('#att_date14').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate14 = attdate_year14+'/'+attdate_day14;
            var attdate_day15 = new Date($('#att_date15').val()).toLocaleDateString('en-US', { day : 'numeric', month : 'numeric' }).split(' ').join(' '); var attdate_year15 = new Date($('#att_date15').val()).toLocaleDateString('en-US', { year : 'numeric' }).split(' ').join(' '); var attdate15 = attdate_year15+'/'+attdate_day15;

            for(var i=0; i < regular.length; i++){
               if( (regular[i] == attdate_day) ||  (regular[i] == attdate) ) { regular_holiday = true; } 
               if( (regular[i] == attdate_day2) ||  (regular[i] == attdate2) ) { regular_holiday2 = true; }
               if( (regular[i] == attdate_day3) ||  (regular[i] == attdate3) ) { regular_holiday3 = true; }
               if( (regular[i] == attdate_day4) ||  (regular[i] == attdate4) ) { regular_holiday4 = true; }
               if( (regular[i] == attdate_day5) ||  (regular[i] == attdate5) ) { regular_holiday5 = true; }
               if( (regular[i] == attdate_day6) ||  (regular[i] == attdate6) ) { regular_holiday6 = true; }
               if( (regular[i] == attdate_day7) ||  (regular[i] == attdate7) ) { regular_holiday7 = true; }
               if( (regular[i] == attdate_day8) ||  (regular[i] == attdate8) ) { regular_holiday8 = true; }
               if( (regular[i] == attdate_day9) ||  (regular[i] == attdate9) ) { regular_holiday9 = true; }
               if( (regular[i] == attdate_day10) ||  (regular[i] == attdate10) ) { regular_holiday10 = true; }
               if( (regular[i] == attdate_day11) ||  (regular[i] == attdate11) ) { regular_holiday11 = true; }
               if( (regular[i] == attdate_day12) ||  (regular[i] == attdate12) ) { regular_holiday12 = true; }
               if( (regular[i] == attdate_day13) ||  (regular[i] == attdate13) ) { regular_holiday13 = true; }
               if( (regular[i] == attdate_day14) ||  (regular[i] == attdate14) ) { regular_holiday14 = true; }
               if( (regular[i] == attdate_day15) ||  (regular[i] == attdate15) ) { regular_holiday15 = true; }}
            for(var i=0; i < special.length; i++){
               if( (special[i] == attdate_day) ||  (special[i] == attdate) ) { special_holiday = true; }
               if( (special[i] == attdate_day2) ||  (special[i] == attdate2) ) { special_holiday2 = true; }
               if( (special[i] == attdate_day3) ||  (special[i] == attdate3) ) { special_holiday3 = true; }
               if( (special[i] == attdate_day4) ||  (special[i] == attdate4) ) { special_holiday4 = true; }
               if( (special[i] == attdate_day5) ||  (special[i] == attdate5) ) { special_holiday5 = true; }
               if( (special[i] == attdate_day6) ||  (special[i] == attdate6) ) { special_holiday6 = true; }
               if( (special[i] == attdate_day7) ||  (special[i] == attdate7) ) { special_holiday7 = true; }
               if( (special[i] == attdate_day8) ||  (special[i] == attdate8) ) { special_holiday8 = true; }
               if( (special[i] == attdate_day9) ||  (special[i] == attdate9) ) { special_holiday9 = true; }
               if( (special[i] == attdate_day10) ||  (special[i] == attdate10) ) { special_holiday10 = true; }
               if( (special[i] == attdate_day11) ||  (special[i] == attdate11) ) { special_holiday11 = true; }
               if( (special[i] == attdate_day12) ||  (special[i] == attdate12) ) { special_holiday12 = true; }
               if( (special[i] == attdate_day13) ||  (special[i] == attdate13) ) { special_holiday13 = true; }
               if( (special[i] == attdate_day14) ||  (special[i] == attdate14) ) { special_holiday14 = true; }
               if( (special[i] == attdate_day15) ||  (special[i] == attdate15) ) { special_holiday15 = true; }
                }

            if (regular_holiday == true){
              for(var i=0; i<datalength; i++){
                $('#holiday'+i).val('Regular');
              } }
            if (regular_holiday2 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday2'+i).val('Regular');
              } }
            if (regular_holiday3 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday3'+i).val('Regular');
              } }
            if (regular_holiday4 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday4'+i).val('Regular');
              } }
            if (regular_holiday5 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday5'+i).val('Regular');
              } }
            if (regular_holiday6 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday6'+i).val('Regular');
              } }
            if (regular_holiday7 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday7'+i).val('Regular');
              } }
            if (regular_holiday8 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday8'+i).val('Regular');
              } }
            if (regular_holiday9 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday9'+i).val('Regular');
              } }
            if (regular_holiday10 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday10'+i).val('Regular');
              } }
            if (regular_holiday11 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday11'+i).val('Regular');
              } }
            if (regular_holiday12 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday12'+i).val('Regular');
              } }
            if (regular_holiday13 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday13'+i).val('Regular');
              } }
            if (regular_holiday14 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday14'+i).val('Regular');
              } }
            if (regular_holiday15 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday15'+i).val('Regular');
              } }

            if (special_holiday == true){
              for(var i=0; i<datalength; i++){
                $('#holiday'+i).val('Special Non-Working');
              } }
            if (special_holiday2 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday2'+i).val('Special Non-Working');
              } }
            if (special_holiday3 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday3'+i).val('Special Non-Working');
              } }
            if (special_holiday4 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday4'+i).val('Special Non-Working');
              } }
            if (special_holiday5 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday5'+i).val('Special Non-Working');
              } }
            if (special_holiday6 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday6'+i).val('Special Non-Working');
              } }
            if (special_holiday7 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday7'+i).val('Special Non-Working');
              } }
            if (special_holiday8 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday8'+i).val('Special Non-Working');
              } }
            if (special_holiday9 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday9'+i).val('Special Non-Working');
              } }
            if (special_holiday10 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday10'+i).val('Special Non-Working');
              } }
            if (special_holiday11 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday11'+i).val('Special Non-Working');
              } }
            if (special_holiday12 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday12'+i).val('Special Non-Working');
              } }
            if (special_holiday13 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday13'+i).val('Special Non-Working');
              } }
            if (special_holiday14 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday14'+i).val('Special Non-Working');
              } }
            if (special_holiday15 == true){
              for(var i=0; i<datalength; i++){
                $('#holiday15'+i).val('Special Non-Working');
              } }

            for(var i=0; i<datalength; i++){

                      //if ($('#period'+i).val() == '') {$('#am'+i).prop("required", true);}
                      if ( ($('#am'+i).is(":checked")) && ($('#pm'+i).is(":checked"))  ) {
                          $('#period'+i).val('Whole Day');
                          $('#am'+i).prop("required", false); }
                      else if ( (!$('#am'+i).is(":checked")) && (!$('#pm'+i).is(":checked")) && (empty($('#markers_id'+i).val())) ) {
                          $('#period'+i).val('---'); }

                      else if ( (!$('#am'+i).is(":checked")) && (!$('#pm'+i).is(":checked")) && (!empty($('#markers_id'+i).val())) ) {
                          var marker = $('#markers_id'+i).val();
                          $('#period'+i).val(marker); }

                      else if ( ($('#am'+i).is(":checked")) ) {
                        $('#period'+i).val('AM');
                        $('#am'+i).prop("required", false); }
                      else if ( ($('#pm'+i).is(":checked")) ) {
                        $('#period'+i).val('PM');
                        $('#am'+i).prop("required", false); }
                }
            for(var i=0; i<datalength; i++){

                      //if ($('#period2'+i).val() == '') {$('#am2'+i).prop("required", true);}
                      if ( ($('#am2'+i).is(":checked")) && ($('#pm2'+i).is(":checked"))  ) {
                          $('#period2'+i).val('Whole Day');
                          $('#am2'+i).prop("required", false); }
                      else if ( (!$('#am2'+i).is(":checked")) && (!$('#pm2'+i).is(":checked"))  && (empty($('#markers_id2'+i).val())) ) {
                          $('#period2'+i).val('---'); }

                      else if ( (!$('#am2'+i).is(":checked")) && (!$('#pm2'+i).is(":checked")) && (!empty($('#markers_id2'+i).val())) ) {
                          var marker2 = $('#markers_id2'+i).val();
                          $('#period2'+i).val(marker2); }

                      else if ( ($('#am2'+i).is(":checked")) ) {
                        $('#period2'+i).val('AM');
                        $('#am2'+i).prop("required", false); }
                      else if ( ($('#pm2'+i).is(":checked")) ) {
                        $('#period2'+i).val('PM');
                        $('#am2'+i).prop("required", false); }
                }
              for(var i=0; i<datalength; i++){

                      //if ($('#period3'+i).val() == '') {$('#am3'+i).prop("required", true);}
                      if ( ($('#am3'+i).is(":checked")) && ($('#pm3'+i).is(":checked"))  ) {
                          $('#period3'+i).val('Whole Day');
                          $('#am3'+i).prop("required", false); }
                      else if ( (!$('#am3'+i).is(":checked")) && (!$('#pm3'+i).is(":checked"))  && (empty($('#markers_id3'+i).val())) ) {
                          $('#period3'+i).val('---'); }

                      else if ( (!$('#am3'+i).is(":checked")) && (!$('#pm3'+i).is(":checked")) && (!empty($('#markers_id3'+i).val())) ) {
                          var marker3 = $('#markers_id3'+i).val();
                          $('#period3'+i).val(marker3); }

                      else if ( ($('#am3'+i).is(":checked")) ) {
                        $('#period3'+i).val('AM');
                        $('#am3'+i).prop("required", false); }
                      else if ( ($('#pm3'+i).is(":checked")) ) {
                        $('#period3'+i).val('PM');
                        $('#am3'+i).prop("required", false); }
                }
              for(var i=0; i<datalength; i++){

                      //if ($('#period4'+i).val() == '') {$('#am4'+i).prop("required", true);}
                      if ( ($('#am4'+i).is(":checked")) && ($('#pm4'+i).is(":checked"))  ) {
                          $('#period4'+i).val('Whole Day');
                          $('#am4'+i).prop("required", false); }
                      else if ( (!$('#am4'+i).is(":checked")) && (!$('#pm4'+i).is(":checked")) && (empty($('#markers_id4'+i).val())) ) {
                          $('#period4'+i).val('---'); }

                      else if ( (!$('#am4'+i).is(":checked")) && (!$('#pm4'+i).is(":checked")) && (!empty($('#markers_id4'+i).val())) ) {
                          var marker4 = $('#markers_id4'+i).val();
                          $('#period4'+i).val(marker4); }

                      else if ( ($('#am4'+i).is(":checked")) ) {
                        $('#period4'+i).val('AM');
                        $('#am4'+i).prop("required", false); }
                      else if ( ($('#pm3'+i).is(":checked")) ) {
                        $('#period4'+i).val('PM');
                        $('#am4'+i).prop("required", false); }
                }
              for(var i=0; i<datalength; i++){

                     // if ($('#period5'+i).val() == '') {$('#am5'+i).prop("required", true);}
                      if ( ($('#am5'+i).is(":checked")) && ($('#pm5'+i).is(":checked"))  ) {
                          $('#period5'+i).val('Whole Day');
                          $('#am5'+i).prop("required", false); }
                      else if ( (!$('#am5'+i).is(":checked")) && (!$('#pm5'+i).is(":checked")) && (empty($('#markers_id5'+i).val())) ) {
                          $('#period5'+i).val('---'); }

                      else if ( (!$('#am5'+i).is(":checked")) && (!$('#pm5'+i).is(":checked")) && (!empty($('#markers_id5'+i).val())) ) {
                          var marker5 = $('#markers_id5'+i).val();
                          $('#period5'+i).val(marker5); }

                      else if ( ($('#am5'+i).is(":checked")) ) {
                        $('#period5'+i).val('AM');
                        $('#am5'+i).prop("required", false); }
                      else if ( ($('#pm5'+i).is(":checked")) ) {
                        $('#period5'+i).val('PM');
                        $('#am5'+i).prop("required", false); }
                }
              for(var i=0; i<datalength; i++){

                      //if ($('#period6'+i).val() == '') {$('#am6'+i).prop("required", true);}
                      if ( ($('#am6'+i).is(":checked")) && ($('#pm6'+i).is(":checked"))  ) {
                          $('#period6'+i).val('Whole Day');
                          $('#am6'+i).prop("required", false); }
                      else if ( (!$('#am6'+i).is(":checked")) && (!$('#pm6'+i).is(":checked")) && (empty($('#markers_id6'+i).val())) ) {
                          $('#period6'+i).val('---'); }

                      else if ( (!$('#am6'+i).is(":checked")) && (!$('#pm6'+i).is(":checked")) && (!empty($('#markers_id6'+i).val())) ) {
                          var marker6 = $('#markers_id6'+i).val();
                          $('#period6'+i).val(marker6); }

                      else if ( ($('#am6'+i).is(":checked")) ) {
                        $('#period6'+i).val('AM');
                        $('#am6'+i).prop("required", false); }
                      else if ( ($('#pm6'+i).is(":checked")) ) {
                        $('#period6'+i).val('PM');
                        $('#am6'+i).prop("required", false); }
                }
              for(var i=0; i<datalength; i++){

                      //if ($('#period7'+i).val() == '') {$('#am7'+i).prop("required", true);}
                      if ( ($('#am7'+i).is(":checked")) && ($('#pm7'+i).is(":checked"))  ) {
                          $('#period7'+i).val('Whole Day');
                          $('#am7'+i).prop("required", false); }
                      else if ( (!$('#am7'+i).is(":checked")) && (!$('#pm7'+i).is(":checked")) && (empty($('#markers_id7'+i).val())) ) {
                          $('#period7'+i).val('---'); }

                      else if ( (!$('#am7'+i).is(":checked")) && (!$('#pm7'+i).is(":checked")) && (!empty($('#markers_id7'+i).val())) ) {
                          var marker7 = $('#markers_id7'+i).val();
                          $('#period7'+i).val(marker7); }

                      else if ( ($('#am7'+i).is(":checked")) ) {
                        $('#period7'+i).val('AM');
                        $('#am7'+i).prop("required", false); }
                      else if ( ($('#pm7'+i).is(":checked")) ) {
                        $('#period7'+i).val('PM');
                        $('#am7'+i).prop("required", false); }
                }

              for(var i=0; i<datalength; i++){
                if ( ($('#am8'+i).is(":checked")) && ($('#pm8'+i).is(":checked"))  ) {
                    $('#period8'+i).val('Whole Day');
                    $('#am8'+i).prop("required", false); }
                else if ( (!$('#am8'+i).is(":checked")) && (!$('#pm8'+i).is(":checked")) && (empty($('#markers_id8'+i).val())) ) {
                    $('#period8'+i).val('---'); }

                else if ( (!$('#am8'+i).is(":checked")) && (!$('#pm8'+i).is(":checked")) && (!empty($('#markers_id8'+i).val())) ) {
                          var marker8 = $('#markers_id8'+i).val();
                          $('#period8'+i).val(marker8); }

                else if ( ($('#am8'+i).is(":checked")) ) {
                  $('#period8'+i).val('AM');
                  $('#am8'+i).prop("required", false); }
                else if ( ($('#pm8'+i).is(":checked")) ) {
                  $('#period8'+i).val('PM');
                  $('#am8'+i).prop("required", false); }
              }
              for(var i=0; i<datalength; i++){
                if ( ($('#am9'+i).is(":checked")) && ($('#pm9'+i).is(":checked"))  ) {
                    $('#period9'+i).val('Whole Day');
                    $('#am9'+i).prop("required", false); }
                else if ( (!$('#am9'+i).is(":checked")) && (!$('#pm9'+i).is(":checked")) && (empty($('#markers_id9'+i).val())) ) {
                    $('#period9'+i).val('---'); }

                else if ( (!$('#am9'+i).is(":checked")) && (!$('#pm9'+i).is(":checked")) && (!empty($('#markers_id9'+i).val())) ) {
                          var marker9 = $('#markers_id9'+i).val();
                          $('#period9'+i).val(marker9); }

                else if ( ($('#am9'+i).is(":checked")) ) {
                  $('#period9'+i).val('AM');
                  $('#am9'+i).prop("required", false); }
                else if ( ($('#pm9'+i).is(":checked")) ) {
                  $('#period9'+i).val('PM');
                  $('#am9'+i).prop("required", false); }
              }
              for(var i=0; i<datalength; i++){
                if ( ($('#am10'+i).is(":checked")) && ($('#pm10'+i).is(":checked"))  ) {
                    $('#period10'+i).val('Whole Day');
                    $('#am10'+i).prop("required", false); }
                else if ( (!$('#am10'+i).is(":checked")) && (!$('#pm10'+i).is(":checked")) && (empty($('#markers_id10'+i).val())) ) {
                    $('#period10'+i).val('---'); }

                else if ( (!$('#am10'+i).is(":checked")) && (!$('#pm10'+i).is(":checked")) && (!empty($('#markers_id10'+i).val())) ) {
                          var marker10 = $('#markers_id10'+i).val();
                          $('#period10'+i).val(marker10); }

                else if ( ($('#am10'+i).is(":checked")) ) {
                  $('#period10'+i).val('AM');
                  $('#am10'+i).prop("required", false); }
                else if ( ($('#pm10'+i).is(":checked")) ) {
                  $('#period10'+i).val('PM');
                  $('#am10'+i).prop("required", false); }
              }
              for(var i=0; i<datalength; i++){
                if ( ($('#am11'+i).is(":checked")) && ($('#pm11'+i).is(":checked"))  ) {
                    $('#period11'+i).val('Whole Day');
                    $('#am11'+i).prop("required", false); }
                else if ( (!$('#am11'+i).is(":checked")) && (!$('#pm11'+i).is(":checked")) && (empty($('#markers_id11'+i).val())) ) {
                    $('#period11'+i).val('---'); }

                else if ( (!$('#am11'+i).is(":checked")) && (!$('#pm11'+i).is(":checked")) && (!empty($('#markers_id11'+i).val())) ) {
                          var marker11 = $('#markers_id11'+i).val();
                          $('#period11'+i).val(marker11); }

                else if ( ($('#am11'+i).is(":checked")) ) {
                  $('#period11'+i).val('AM');
                  $('#am11'+i).prop("required", false); }
                else if ( ($('#pm11'+i).is(":checked")) ) {
                  $('#period11'+i).val('PM');
                  $('#am11'+i).prop("required", false); }
              }
              for(var i=0; i<datalength; i++){
                if ( ($('#am12'+i).is(":checked")) && ($('#pm12'+i).is(":checked"))  ) {
                    $('#period12'+i).val('Whole Day');
                    $('#am12'+i).prop("required", false); }
                else if ( (!$('#am12'+i).is(":checked")) && (!$('#pm12'+i).is(":checked")) && (empty($('#markers_id12'+i).val())) ) {
                    $('#period12'+i).val('---'); }

                else if ( (!$('#am12'+i).is(":checked")) && (!$('#pm12'+i).is(":checked")) && (!empty($('#markers_id12'+i).val())) ) {
                          var marker12 = $('#markers_id12'+i).val();
                          $('#period12'+i).val(marker12); }

                else if ( ($('#am12'+i).is(":checked")) ) {
                  $('#period12'+i).val('AM');
                  $('#am12'+i).prop("required", false); }
                else if ( ($('#pm12'+i).is(":checked")) ) {
                  $('#period12'+i).val('PM');
                  $('#am12'+i).prop("required", false); }
              }
              for(var i=0; i<datalength; i++){
                if ( ($('#am13'+i).is(":checked")) && ($('#pm13'+i).is(":checked"))  ) {
                    $('#period13'+i).val('Whole Day');
                    $('#am13'+i).prop("required", false); }
                else if ( (!$('#am13'+i).is(":checked")) && (!$('#pm13'+i).is(":checked")) && (empty($('#markers_id13'+i).val())) ) {
                    $('#period13'+i).val('---'); }

                else if ( (!$('#am13'+i).is(":checked")) && (!$('#pm13'+i).is(":checked")) && (!empty($('#markers_id13'+i).val())) ) {
                          var marker13 = $('#markers_id13'+i).val();
                          $('#period13'+i).val(marker13); }

                else if ( ($('#am13'+i).is(":checked")) ) {
                  $('#period13'+i).val('AM');
                  $('#am13'+i).prop("required", false); }
                else if ( ($('#pm13'+i).is(":checked")) ) {
                  $('#period13'+i).val('PM');
                  $('#am13'+i).prop("required", false); }
              }
              for(var i=0; i<datalength; i++){
                if ( ($('#am14'+i).is(":checked")) && ($('#pm14'+i).is(":checked"))  ) {
                    $('#period14'+i).val('Whole Day');
                    $('#am14'+i).prop("required", false); }
                else if ( (!$('#am14'+i).is(":checked")) && (!$('#pm14'+i).is(":checked")) && (empty($('#markers_id14'+i).val())) ) {
                    $('#period14'+i).val('---'); }

                else if ( (!$('#am14'+i).is(":checked")) && (!$('#pm14'+i).is(":checked")) && (!empty($('#markers_id14'+i).val())) ) {
                          var marker14 = $('#markers_id14'+i).val();
                          $('#period14'+i).val(marker14); }

                else if ( ($('#am14'+i).is(":checked")) ) {
                  $('#period14'+i).val('AM');
                  $('#am14'+i).prop("required", false); }
                else if ( ($('#pm14'+i).is(":checked")) ) {
                  $('#period14'+i).val('PM');
                  $('#am14'+i).prop("required", false); }
              }
              for(var i=0; i<datalength; i++){
                if ( ($('#am15'+i).is(":checked")) && ($('#pm15'+i).is(":checked"))  ) {
                    $('#period15'+i).val('Whole Day');
                    $('#am15'+i).prop("required", false); }
                else if ( (!$('#am15'+i).is(":checked")) && (!$('#pm15'+i).is(":checked")) && (empty($('#markers_id15'+i).val())) ) {
                    $('#period15'+i).val('---'); }

                else if ( (!$('#am15'+i).is(":checked")) && (!$('#pm15'+i).is(":checked")) && (!empty($('#markers_id15'+i).val())) ) {
                          var marker15 = $('#markers_id15'+i).val();
                          $('#period15'+i).val(marker15); }

                else if ( ($('#am15'+i).is(":checked")) ) {
                  $('#period15'+i).val('AM');
                  $('#am15'+i).prop("required", false); }
                else if ( ($('#pm15'+i).is(":checked")) ) {
                  $('#period15'+i).val('PM');
                  $('#am15'+i).prop("required", false); }
              }

           });
         
           $('#Continue').on('click', function() {

              appValidateForm($('form'),{proj_id:'required',loc_id:'required',emp_id:'required',att_time:'required'});
            
           });

        });
</script>
</body>
</html>




