<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?php init_head(); ?>
<style>
  .form-control[readonly]{
    background-color: #ffffff !important;
    border-color: #d1dbe5 !important;
    color: #000000 !important;
    cursor: not-allowed; }

  .pager li >a{
    display: inline-block;
    padding: 5px 14px;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 0 !important;
}
.pager>li:first-child>a {
    border-top-left-radius: 4px !important;
    border-bottom-left-radius: 4px !important;
}
.pager>li:last-child>a {
    border-top-right-radius: 4px !important;
    border-bottom-right-radius: 4px !important;
}
.pagination>.active>a{
    background-color: #007bff !important;
    border-color: #007bff  !important;
     color: white !important;
}

a.prev_link.page-link.disabled,a.next_link.page-link.disabled {
    color: #6c757d !important;
}
a.page_link.page-link,a.prev_link.page-link,a.next_link.page-link{
  color: #007bff ;
}
table thead>tr>th {
    color: #4e75ad !important;
    background: #f6f8fa !important;
    vertical-align: middle !important;
    border-bottom: 1px solid !important;
    border-color: #ebf5ff !important;
    font-size: 13px !important;
    padding-top: 9px !important;
    padding-bottom: 8px !important;
}
table.table {
    margin-top: 10px !important;
}
</style>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
          
            <div class="row">
               <div class="col-md-12" id="small-table">
                  <div class="panel_s">
                     <div class="panel-body">
                        <div class="clearfix"></div>
                          <br>

                            <?php 

                              if ( isset($result_display) ) { ?>
                                <div class="row">
                                    <div class="col-md-5">
                                      <label for="date1"><strong>From</strong></label>
                                      <input type="text" class="form-control" id="date1" value="<?php

                                      $originalDate = $display_date['date1'];
                                      $newDate = date("M d, Y", strtotime($originalDate));

                                       echo  $newDate ?>" readonly>
                                    </div>
                                    <div class="col-md-5">
                                      <label for="date2"><strong>To</strong></label>
                                       <input type="text" class="form-control" id="date2" value="<?php
                                      
                                      $originalDate = $display_date['date2'];
                                      $newDate = date("M d, Y", strtotime($originalDate));

                                       echo  $newDate ?>" readonly>
                                    </div> 
                                 <div class="col-md-2">
                                  <a style="margin-top: 25px;" href="<?php echo admin_url('attendance_log/atten_log'); ?>" class="btn btn-success">Reset</a>
                                 </div>
                               </div>
                               <br>
                                <?php
                                
                                echo "
                                  <br>
                                 <table id='order_data' class='table' >"; ?>
                                
                                 <thead>
                                      <tr role="row">
                                       <th>Month</th>
                                       <th>Project Name</th>
                                       <th>Location</th>
                                       <th>No. of Members</th>
                                       <th>Dates</th>
                                      </tr>
                                </thead>
                                <tbody id="myTable">
                                <?php
                                if ($result_display == 'No record found !') {
                                   echo '<tr>
                                                <td colspan="5" style="text-align:center">No record(s) found.</td>
                                        </tr>';
                                } else {
                                foreach ($result_display as $value) { 

                                $get_proj = $this->db->query("SELECT * FROM " . db_prefix() . "attendance_project WHERE uid='".$value->uid."' AND (att_time_min BETWEEN '".$value->min_time."' AND '".$value->max_time."' AND att_time_max BETWEEN '".$value->min_time."' AND '".$value->max_time."') GROUP BY project_id ")->result_array();
                                
                                $att_month = date("m", strtotime($value->max_time));
                                switch ($att_month)
                                {
                                    case "1":
                                        $att_month = 'January';
                                        break;
                                    case "2":
                                        $att_month = 'February';
                                        break;
                                    case "3":
                                        $att_month = 'March';
                                        break;
                                    case "4":
                                        $att_month = 'April';
                                        break;
                                    case "5":
                                        $att_month = 'May';
                                        break;
                                    case "6":
                                        $att_month = 'June';
                                        break;
                                    case "7":
                                        $att_month = 'July';
                                        break;
                                    case "8":
                                        $att_month = 'August';
                                        break;
                                    case "9":
                                        $att_month = 'September';
                                        break;
                                    case "10":
                                        $att_month = 'October';
                                        break;
                                    case "11":
                                        $att_month = 'November';
                                        break;
                                    case "12":
                                        $att_month = 'December';
                                        break;
                                }
                                
                                $value->location_id != '' ? $value->location_id == 1 ? $loc = 'Office Work' : $loc = 'Work At Home' : $loc = '';

                                if ($value->min_time==$value->max_time) {
                                  $time = date("M d, Y", strtotime($value->min_time));
                                }
                                else{
                                  $time = date("M d, Y", strtotime($value->min_time)) . ' - ' . date("M d, Y", strtotime($value->max_time));
                                }

                                echo "<tr class='has-row-options odd'>" . 

                                  "<td> 
                                    <a href='". admin_url('attendance_log/fetchdetailspdf/' . $value->month_log . '/' . $value->uid . '/' . $value->number )."' target='_blank' > " . $att_month . "
                                    </a>
                                    <div class='row-options'>
                                      <a href='". admin_url('attendance_log/fetchdetailspdf/' . $value->month_log . '/' . $value->uid . '/' . $value->number)."' target='_blank'> View PDF
                                        </a> |
                                      <a class='text-danger _delete' href='". admin_url('attendance_log/deletepdf/' . $value->month_log . '/' . $value->uid . '/' . $value->number)."'> Delete
                                        </a>
                                    </div>
                                  </td>";
                                  echo "<td>";
                                    foreach($get_proj as $proj_val) {
                                      $getproj = $this->db->query("SELECT * FROM tblprojects WHERE id='".$proj_val['project_id']."' ");
                                      $rowproj = $getproj->row();
                                      echo "<a href='". admin_url('projects/view/' . $rowproj->id )."' target='_blank' > " . $rowproj->name . "
                                      </a>, ";
                                    }
                                  echo "</td>" . 

                                    "<td> " . $loc . " </td>";

                                    $proj = $this->db->query("SELECT * FROM " . db_prefix() ."attendance_history JOIN " . db_prefix() . "employee on " . db_prefix() . "employee.emp_id = " . db_prefix() . "attendance_history.uid WHERE MONTH(att_time)='".$value->month_log."' AND uid='".$value->uid."' GROUP BY uid ")->row();
                                    echo "<td>";
                                      echo "<a href='". admin_url('employee/view_emp/' . $proj->uid)."' target='_blank' >" . $proj->first_name . " " . $proj->last_name ."</a>&nbsp";
                                    echo "</td>";
                                    
                                    echo "<td>  " . $time . " </td>" .

                                      "</tr>";

                                  ?>

                                   
                                <?php   }
                                } ?>
                                </tbody>
                                <?php
                                echo "</table>
                                <div class='col-md-12 text-right'>
                                  <ul class='pagination pager' id='myPager'></ul>
                                </div>
                                ";   ?>
                                <?php
                                

                              }

                              else{ ?>
                                  <?php

                                   echo form_open('admin/attendance_log/atten_log'); ?>
                                  <div class="row">
                                   <div class="col-md-5">
                                    <label for="date1"><strong>From</strong></label>
                                    <?php 
                                            $data = array(
                                                    'type' => 'date',
                                                    'name' => 'date_from',
                                                    'placeholder' => 'yyyy-mm-dd',
                                                    'class'      => 'form-control',
                                                    'id'      => 'date1',
                                                    'required'      => 'required',
                                                    );
                                                    echo form_input($data);
                                    ?>
                                  </div>
                                  <div class="col-md-5">
                                    <label for="date2"><strong>To</strong></label>
                                     <?php 
                                            $data = array(
                                                    'type' => 'date',
                                                    'name' => 'date_to',
                                                    'placeholder' => 'yyyy-mm-dd',
                                                    'class'      => 'form-control',
                                                    'id'      => 'date2',
                                                    'required'      => 'required',
                                                    );
                                                    echo form_input($data);
                                    ?>
                                  </div>     
                                 <div class="col-md-2">
                                  <input style="margin-top: 25px;"  type="submit" name="filter" id="submit" value="Search" class="btn btn-info" />
                                 </div>
                               </div>
                            <?php  
                                echo form_close();


                                 echo "
                                  <br>
                                 <table id='order_data' class='table' >"; ?>
                                
                                 <thead>
                                      <tr role="row">
                                       <th>Month</th>
                                       <th>Project Name</th>
                                       <th>Location</th>
                                       <th>Employee</th>
                                       <th>Dates</th>
                                      </tr>
                                </thead>
                                <tbody id="myTable">
                                <?php
                                if ($show_table == 'Database is empty !') {
                                   echo '<tr>
                                          <td colspan="5" style="text-align:center">No record(s) found.</td>
                                        </tr>';
                                } else {
                                foreach ($show_table as $value) {

                                $get_proj = $this->db->query("SELECT * FROM " . db_prefix() . "attendance_project WHERE uid='".$value->uid."' AND (att_time_min BETWEEN '".$value->min_time."' AND '".$value->max_time."' AND att_time_max BETWEEN '".$value->min_time."' AND '".$value->max_time."') GROUP BY project_id ")->result_array();
                                
                                $att_month = date("m", strtotime($value->max_time));
                                switch ($att_month)
                                {
                                    case "1":
                                        $att_month = 'January';
                                        break;
                                    case "2":
                                        $att_month = 'February';
                                        break;
                                    case "3":
                                        $att_month = 'March';
                                        break;
                                    case "4":
                                        $att_month = 'April';
                                        break;
                                    case "5":
                                        $att_month = 'May';
                                        break;
                                    case "6":
                                        $att_month = 'June';
                                        break;
                                    case "7":
                                        $att_month = 'July';
                                        break;
                                    case "8":
                                        $att_month = 'August';
                                        break;
                                    case "9":
                                        $att_month = 'September';
                                        break;
                                    case "10":
                                        $att_month = 'October';
                                        break;
                                    case "11":
                                        $att_month = 'November';
                                        break;
                                    case "12":
                                        $att_month = 'December';
                                        break;
                                }

                                $value->location_id != '' ? $value->location_id == 1 ? $loc = 'Office Work' : $loc = 'Work At Home' : $loc = '';

                                if ($value->min_time==$value->max_time) {
                                  $time = date("M d, Y", strtotime($value->min_time));
                                }
                                else{
                                  $time = date("M d, Y", strtotime($value->min_time)) . ' - ' . date("M d, Y", strtotime($value->max_time));
                                }

                                echo "<tr class='has-row-options odd'>" . 

                                  "<td> 
                                    <a href='". admin_url('attendance_log/fetchdetailspdf/' . $value->month_log . '/' . $value->uid . '/' . $value->number)."' target='_blank' > " . $att_month . "
                                    </a>
                                    <div class='row-options'>
                                      <a href='". admin_url('attendance_log/fetchdetailspdf/' . $value->month_log . '/' . $value->uid . '/' . $value->number)."' target='_blank'> View PDF
                                        </a> |
                                      <a  class='text-danger _delete' href='". admin_url('attendance_log/deletepdf/' . $value->month_log . '/' . $value->uid . '/' . $value->number )."'> Delete
                                        </a>
                                    </div>
                                  </td>";

                                  echo "<td>";
                                    foreach($get_proj as $proj_val) {
                                      $getproj = $this->db->query("SELECT * FROM tblprojects WHERE id='".$proj_val['project_id']."' ");
                                      $rowproj = $getproj->row();
                                      echo "<a href='". admin_url('projects/view/' . $rowproj->id )."' target='_blank' > " . $rowproj->name . "
                                      </a>, ";
                                    }
                                  echo "</td>" . 

                                     "<td> " . $loc . " </td>";

                                    $proj = $this->db->query("SELECT * FROM " . db_prefix() ."attendance_history JOIN " . db_prefix() . "employee on " . db_prefix() . "employee.emp_id = " . db_prefix() . "attendance_history.uid WHERE MONTH(att_time)='".$value->month_log."' AND uid='".$value->uid."' GROUP BY uid ")->row();
                                    echo "<td>";
                                      echo "<a href='". admin_url('employee/view_emp/' . $proj->uid)."' target='_blank' >" . $proj->first_name . " " . $proj->last_name ."</a>&nbsp";
                                    echo "</td>";

                                    echo "<td>  " . $time . " </td>" .

                                      "</tr>";

                                  ?>

                                   
                                <?php   }
                                } ?>
                                </tbody>
                                <?php
                                echo "</table>

                               
                                <div class='row'>
                                   <div class='col-md-12 text-right'>
                                    <ul class='pagination pager' id='myPager'></ul>
                                  </div>
                                </div>
                                ";
                            }

                            ?>
                            
                       

                     </div>
                  </div>
               </div>
             <!--   <div class="col-md-7 small-table-right-col">
                  <div id="expense" class="hide">
                  </div>
               </div> -->
            </div>
         </div>
      </div>
   </div>
</div>


<script>var hidden_columns = [4,5,6,7,8,9];</script>
 <?php init_tail(); ?>  
 <script type="text/javascript">
   $.fn.pageMe = function(opts){
    var $this = this,
        defaults = {
            perPage: 7,
            showPrevNext: false,
            hidePageNumbers: false
        },
        settings = $.extend(defaults, opts);
    
    var listElement = $this;
    var perPage = settings.perPage; 
    var children = listElement.children();
    var pager = $('.pager');
    
    if (typeof settings.childSelector!="undefined") {
        children = listElement.find(settings.childSelector);
    }
    
    if (typeof settings.pagerSelector!="undefined") {
        pager = $(settings.pagerSelector);
    }
    
    var numItems = children.size();
    var numPages = Math.ceil(numItems/perPage);

    pager.data("curr",0);
    
    if (settings.showPrevNext){
        $('<li class="page-item"><a href="#" class="prev_link page-link">Previous</a></li>').appendTo(pager);
    }
    
    var curr = 0;
    while(numPages > curr && (settings.hidePageNumbers==false)){
        $('<li class="page-item"><a href="#" class="page_link page-link">'+(curr+1)+'</a></li>').appendTo(pager);
        curr++;
    }
    
    if (settings.showPrevNext){
        $('<li class="page-item"><a href="#" class="next_link page-link">Next</a></li>').appendTo(pager);
    }
    
    pager.find('.page_link:first').addClass('active');
    pager.find('.prev_link').addClass('disabled');
    if (numPages<=1) {
        pager.find('.next_link').addClass('disabled');
    }
    pager.children().eq(1).addClass("active");
    
    children.hide();
    children.slice(0, perPage).show();
    
    pager.find('li .page_link').click(function(){
        var clickedPage = $(this).html().valueOf()-1;
        goTo(clickedPage,perPage);
        return false;
    });
    pager.find('li .prev_link').click(function(){
        previous();
        return false;
    });
    pager.find('li .next_link').click(function(){
        next();
        return false;
    });
    
    function previous(){
        var goToPage = parseInt(pager.data("curr")) - 1;
        goTo(goToPage);
    }
     
    function next(){
        goToPage = parseInt(pager.data("curr")) + 1;
        goTo(goToPage);
    }
    
    function goTo(page){
        var startAt = page * perPage,
            endOn = startAt + perPage;
        
        children.css('display','none').slice(startAt, endOn).show();
        
        if (page>=1) {
            pager.find('.prev_link').removeClass('disabled');
        }
        else {
            pager.find('.prev_link').addClass('disabled');
        }
        
        if (page<(numPages-1)) {
            pager.find('.next_link').removeClass('disabled');
        }
        else {
            pager.find('.next_link').addClass('disabled');
        }
        
        pager.data("curr",page);
        pager.children().removeClass("active");
        pager.children().eq(page+1).addClass("active");
    
    }
};

$(document).ready(function(){

    
  $('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:10});
    
});
 </script>
</body>
</html>
