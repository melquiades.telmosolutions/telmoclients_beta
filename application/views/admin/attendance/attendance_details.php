
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s mbot10">

               <div class="panel-body _buttons">
                <h2 class="no-margin font-large pull-left">
                  <i class="fa fa-home" aria-hidden="true"></i> 
                 Attendance Log </h2>
               </div>
        </div> 
        <div class="row">
          <div class="col-md-12" id="small-table">
            <div class="panel_s">
              <div class="panel-body">
                 <div class="clearfix"></div> 
                  <h2 style="text-align: center;"><?php echo $employee->first_name ." ".  $employee->middle_name ." ". $employee->last_name ?></h2>

                  
                 <table class='table table-lg table-bordered table-striped'>
                        
                  <tbody>
                     <tr>
                      <td style="vertical-align: middle;text-align: center;" rowspan='<?php echo $attencount+1 ?>'>
                        <h3><?php echo $attendate ?> </h3>
                      </td>
                      <td><strong><center>PUNCH TIME</center></strong></td>
                       <td><strong><center>STATUS</center></strong></td>
                     </tr>
                    

                 <?php  



                   
                    foreach ($show_table as $value) {
                      

                     ?>
                        
                         <?php
                           echo "<tr>" 
                           . "<td style='text-align:center'>" . date("h:i A", strtotime($value->att_time)) . "</td>" 
                          . "<td>" . $value->status . "</td>"  . 
                           "</tr>"; ?>
                        
                        
                  <?php
                     }
                     
                  ?>
                  <tr>
                    <td style="text-align: center;">
                      <h4>Overtime: &nbsp;&nbsp;&nbsp;<strong><?php
                        $hms = explode(":", $attenot->overtime);
                        $ot = ($hms[0] + ($hms[1]/60) + ($hms[2]/3600));

                        if ($ot == 0) {
                                    $ovt = $ot;
                                  }
                        else{
                                    $ovt = number_format($ot, 1);
                         }
                                 
                       echo $ovt ?>
                         
                       </strong></h4>
                    </td>
                    <td colspan="2" >
                      <h4>Total Time: &nbsp;&nbsp;&nbsp;<strong><?php
                       $hms2 = explode(":", $attentotal->total_time);
                      $tt = ($hms2[0] + ($hms2[1]/60) + ($hms2[2]/3600));

                       if ($tt == 0) {
                           $total = $tt;
                       }
                         else{
                          $total = number_format($tt, 1);
                       }
                       echo $total ?></strong></h4>
                    </td>
                  </tr>
                </tbody>
                      </table> 

                    

               </div> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<?php init_tail(); ?>

<script>
   $(function(){
       var DepartmentServerParams = {};
       $.each($('._hidden_inputs._filters input'),function(){
          DepartmentServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
      });
       DepartmentServerParams['exclude_inactive'] = '[name="exclude_inactive"]:checked';

       var tAPI = initDataTable('.table-department', admin_url+'department/table', [0], [0], DepartmentServerParams,<?php echo hooks()->apply_filters('department_table_default_order', json_encode(array(0,'asc'))); ?>);
       $('input[name="exclude_inactive"]').on('change',function(){
           tAPI.ajax.reload();
       });
   });
</script>
</body>
</html>
