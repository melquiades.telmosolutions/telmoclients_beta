<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
   <title>PDF Compesation</title>
 <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
  <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js') ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
</head>
<body>
  
<table class="table" border="1" style="font-size: 11px">
     <thead>
        <tr>
          <th style="padding: 7px;text-align: center;" rowspan="2">NAME</th>
          <th style="padding: 7px;text-align: center;" rowspan="2">TIN</th>
          <th style="padding: 7px;text-align: center;" rowspan="2">RATE</th>
          <th style="padding: 7px;text-align: center;" colspan="11">GROSS COMPENSATION</th>
          <th style="padding: 7px;text-align: center;" rowspan="2">13TH MO. PAY</th>
          <th style="padding: 7px;text-align: center;" rowspan="2">OTHERS</th>
          <th style="padding: 7px;text-align: center;" colspan="5">SSS</th>
          <th style="padding: 7px;text-align: center;" colspan="3">PHIC</th>
          <th style="padding: 7px;text-align: center;" colspan="3">HDMF</th>
          <th style="padding: 7px;text-align: center;" rowspan="2">NET TAXABLE</th>
          <th style="padding: 7px;text-align: center;" rowspan="2">WTAX</th>
        </tr>
    </thead>
    <tbody>
      <tr>
          <th></th>
          <th></th>
          <th></th>
          <th style="padding: 7px;text-align: center;">COMPENSATION</th>
          <th style="padding: 7px;text-align: center;">COMMISSION</th>
          <th style="padding: 7px;text-align: center;">ALLOWANCE</th>
          <th style="padding: 7px;text-align: center;">INCENTIVES</th>
          <th style="padding: 7px;text-align: center;">HOLIDAY PAY</th>
          <th style="padding: 7px;text-align: center;">OVERTIME</th>
          <th style="padding: 7px;text-align: center;">OTHERS</th>
          <th style="padding: 7px;text-align: center;">TOTAL</th>
          <th></th>
          <th></th>
          <th style="padding: 7px;text-align: center;">ER</th>
          <th style="padding: 7px;text-align: center;">EE</th>
          <th style="padding: 7px;text-align: center;">TOTAL</th>
          <th style="padding: 7px;text-align: center;">ER</th>
          <th style="padding: 7px;text-align: center;">EE</th>
          <th style="padding: 7px;text-align: center;">TOTAL</th>
          <th style="padding: 7px;text-align: center;">ER</th>
          <th style="padding: 7px;text-align: center;">EE</th>
          <th style="padding: 7px;text-align: center;">TOTAL</th>
          <th></th>
          <th></th>
        </tr>
      <?php

      for ($i=0; $i <15 ; $i++) { ?>
        
        <tr>
          <?php
            for ($t=0; $t <29 ; $t++) { ?>
              <td style="padding: 12px;text-align: center;"></td>
          <?php  }
          ?>
         
        </tr>
       
     <?php }

      ?>
    </tbody>
  </table>


  

</body>
</html>
