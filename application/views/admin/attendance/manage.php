
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- <div class="panel_s mbot10">

               <div class="panel-body _buttons">
                <h2 class="no-margin font-large pull-left">
                  <i class="fa fa-home" aria-hidden="true"></i> 
                  Division</h2>
                  <a href="#" class="pull-right btn btn-primary mleft5 btn-with-tooltip" data-target="#newdiv" data-toggle="modal" title="Add New Department"><i class="fa fa-plus-circle"></i> New Division</a>
               </div>
        </div>  -->
        <div class="row">
          <div class="col-md-12" id="small-table">
            <div class="panel_s">
              <div class="panel-body">
                <div class="clearfix"></div>  
                <?php
                  $table_data = array();
                  $_table_data = array(
                    array(
                      'name'=>_l('emp_num'),
                      'th_attrs'=>array('class'=>'toggleable', 'id'=>'th-number')
                    ),
                     array(
                      'name'=>_l('emp_id'),
                      'th_attrs'=>array('class'=>'toggleable', 'id'=>'th-employeename')
                    ),
                    array(
                      'name'=>_l('emp_time'),
                      'th_attrs'=>array('class'=>'toggleable', 'id'=>'th-emp_time')
                    ),
                  );
                  foreach($_table_data as $_t){
                    array_push($table_data,$_t);
                  }

                  // $custom_fields = get_custom_fields('customers',array('show_on_table'=>1));
                  // foreach($custom_fields as $field){
                  //   array_push($table_data,$field['name']);
                  // }

                  $table_data = hooks()->apply_filters('attendance_history_table_columns', $table_data);

                     render_datatable($table_data,'attendance_history',[],[
                           'data-last-order-identifier' => 'attendance_history',
                           'data-default-order'         => get_table_last_order('attendance_history'),
                     ]);
                     ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- modals -->
<!-- <div id="newdiv" class="modal fade" role="dialog">
          <div class="modal-dialog modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <strong>
                    <h3 class="no-margin">
                      <i class="fa fa-home" aria-hidden="true"></i> 
                      New Division</h3>
                  </strong>
              </div>
                <div class="modal-body">
                  <div class="form-group">
                    <label for="sel_dept"><strong>Select Department</strong></label>
                    <div class="row-fluid">
                      <select class="selectpicker" id="sel_dept"  data-width="100%" data-live-search="true">
                        <option>Select</option>
                        <option>Department1</option>
                        <option>Department2</option>
                        <option>Department3</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="dept"><strong>Division</strong></label>
                    <input type="text" class="form-control" id="dept" placeholder="Enter here">
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="form-group text-right">
                    <button type="button" data-dismiss="modal" class="btn btn-default w-md m-b-5">Cancel</button>
                    <button type="submit" class="btn btn-info w-md m-b-5">Save</button>
                  </div>
                </div>
              
            </div>
          </div>
        </div> -->

<?php init_tail(); ?>
<script>
   $(function(){
       var DivisionServerParams = {};
       $.each($('._hidden_inputs._filters input'),function(){
          DivisionServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
      });
       DivisionServerParams['exclude_inactive'] = '[name="exclude_inactive"]:checked';

       var tAPI = initDataTable('.table-attendance_history', admin_url+'attendance_log/table', [0], [0], DivisionServerParams,<?php echo hooks()->apply_filters('attendance_history_table_default_order', json_encode(array(0,'asc'))); ?>);
       $('input[name="exclude_inactive"]').on('change',function(){
           tAPI.ajax.reload();
       });
   });
   // function customers_bulk_action(event) {
   //     var r = confirm(app.lang.confirm_action_prompt);
   //     if (r == false) {
   //         return false;
   //     } else {
   //         var mass_delete = $('#mass_delete').prop('checked');
   //         var ids = [];
   //         var data = {};
   //         if(mass_delete == false || typeof(mass_delete) == 'undefined'){
   //             data.groups = $('select[name="move_to_groups_customers_bulk[]"]').selectpicker('val');
   //             if (data.groups.length == 0) {
   //                 data.groups = 'remove_all';
   //             }
   //         } else {
   //             data.mass_delete = true;
   //         }
   //         var rows = $('.table-clients').find('tbody tr');
   //         $.each(rows, function() {
   //             var checkbox = $($(this).find('td').eq(0)).find('input');
   //             if (checkbox.prop('checked') == true) {
   //                 ids.push(checkbox.val());
   //             }
   //         });
   //         data.ids = ids;
   //         $(event).addClass('disabled');
   //         setTimeout(function(){
   //           $.post(admin_url + 'clients/bulk_action', data).done(function() {
   //            window.location.reload();
   //        });
   //       },50);
   //     }
   // }
</script>
</body>
</html>
