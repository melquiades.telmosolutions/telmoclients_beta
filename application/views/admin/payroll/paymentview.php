<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="row">
               <div class="col-md-12">
                  <div class="panel_s">
                     <div class="panel-body">
                      <div class="clearfix"></div>
                      <?php
                        $table_data = array(
                          _l('employee_id_num'),
                          _l('employee'),
                          _l('project'),
                          _l('date'),
                          _l('tot_w_h'),
                          _l('tot_w_d'),
                          _l('tot_ot'),
                          _l('benefit'),
                          _l('bonus'),
                          _l('rate'),
                          _l('tot_salary'),
                        );
                        render_datatable($table_data,'salary');
                      ?>
                    </div>
                  </div>

                  <div id="paymentview_edit" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-md">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <strong>Manage Employee Salary</strong>
                        </div>
                        <?php echo form_open(admin_url('payroll/update_payment_view/'.$pv_id->emp_sal_pay_id),array('id'=>'paymentview_form')) ?>
                        <div class="modal-body">
                          <input type="hidden" name="pv_emp_id" class="form-control" value="<?php echo isset($pv_id) ? $pv_id->employee_id : '' ?>" readonly>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-4">
                                <label for="pv_employee_id"><?php echo _l('employee_id_num'); ?></label>
                                <input type="text" name="pv_employee_id" class="form-control" value="<?php echo isset($pv_id) ? $pv_id->emp_id_number : '' ?>" readonly>
                              </div>
                              <div class="col-sm-8">
                                <label for="pv_emp_name"><?php echo _l('employee_name'); ?></label>
                                <input type="text" name="pv_emp_name" class="form-control" value="<?php echo isset($pv_id) ? $pv_id->first_name.' '.$pv_id->last_name : '' ?>" readonly>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-8">
                                <label for="pv_date"><?php echo _l('date'); ?></label>
                                <input type="text" name="pv_date" class="form-control" value="<?php echo isset($pv_id) ? $pv_id->name : '' ?>" readonly>
                              </div>
                              <div class="col-sm-4">
                                <label for="payment_modes"><?php echo _l('payment_mode'); ?></label>
                                <select name="payment_modes" class="form-control selectpicker" id="pmodes">
                                  <option value="">Nothing Selected</option>
                                  <?php foreach($pay_modes as $row_pm) { ?>
                                    <option <?php if($pv_id->paymentmode == $row_pm['id']) { echo 'selected'; } elseif ($pv_id->paymentmode == $row_pm['id']) { echo 'selected'; } ?> value="<?php echo $row_pm['id']; ?>"><?php echo $row_pm['name']; ?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-6">
                                <label for="pv_rate"><?php echo _l('rate'); ?><small> (₱)</small></label>
                                <input type="number" name="pv_rate" id="pv_rate" class="form-control" value="<?php echo isset($pv_id) ? number_format($pv_id->rate, 2) : '' ?>" readonly>
                                <input type="hidden" id="pv_ratetype" value="<?php echo isset($pv_id) ? $pv_id->rate_type : '' ?>">
                                <input type="hidden" id="pv_salarysetup" value="<?php echo isset($pv_id) ? $pv_id->salary_setup : '' ?>">
                              </div>
                              <div class="col-sm-6">
                                <label for="pv_skillindex"><?php echo _l('nav_pay_frequency'); ?></label>
                                <input type="text" name="pv_skillindex" id="pv_skillindex" class="form-control" value="<?php echo isset($pv_id) ? $pv_id->skill_index == 0 ? '' : number_format($pv_id->skill_index, 2) : '' ?>">
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-6">
                                <label for="pv_total_working_hours"><?php echo _l('tot_w_h'); ?><small> (hrs)</small></label>
                                <input type="number" name="pv_total_working_hours" id="pv_twh" class="form-control" value="<?php echo isset($pv_id) ? $pv_id->total_working_hours : '' ?>">
                              </div>
                              <div class="col-sm-6">
                                <label for="pv_working_period"><?php echo _l('tot_w_d'); ?></label>
                                <input type="number" name="pv_working_period" id="pv_wp" class="form-control" value="<?php echo isset($pv_id) ? $pv_id->working_period : '' ?>">
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="row">
                              <div class="col-sm-4">
                                <label for="pv_overtime"><?php echo _l('tot_ot'); ?><small> (hrs)</small></label>
                                <input type="number" name="pv_overtime" id="pv_ot" class="form-control" value="<?php echo isset($pv_id) ? $pv_id->overtime == 0 ? '' : $pv_id->overtime : '' ?>">
                              </div>
                              <div class="col-sm-4">
                                <label for="pv_regular_ot_rate"><?php echo _l('regular_ot_rate'); ?><small> (₱)</small></label>
                                <input type="number" name="pv_regular_ot_rate" id="pv_ror" class="form-control" value="<?php echo isset($pv_id) ? $pv_id->regular_ot_rate == 0 ? '' : $pv_id->regular_ot_rate : '' ?>">
                              </div>
                              <div class="col-sm-4">
                                <label for="pv_special_ot_rate"><?php echo _l('special_ot_rate'); ?><small> (₱)</small></label>
                                <input type="number" name="pv_special_ot_rate" id="pv_sor" class="form-control" value="<?php echo isset($pv_id) ? $pv_id->special_ot_rate == 0 ? '' : $pv_id->special_ot_rate : '' ?>">
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="pv_total_salary"><?php echo _l('tot_salary'); ?></label>
                            <input type="number" name="pv_total_salary" id="pv_tot_salary" class="form-control" value="<?php echo isset($pv_id) ? $pv_id->total_salary : '' ?>">
                          </div>
                          <div class="form-group">
                            <?php $value = (isset($pv_id) ? $pv_id->remarks : ''); ?>
                            <?php echo render_textarea('remarks', _l('remarks'), $value); ?>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <div class="form-group text-right">
                            <button type="close" class="btn btn-default w-md m-b-5" data-dismiss="modal"><?php echo _l('cancel'); ?></button>
                            <button type="submit" data-form="paymentview_form" id="update_btn" onclick="pv_submit()" class="btn btn-success w-md m-b-5" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('update'); ?></button>
                          </div>
                        </div>
                        <?php echo form_close(); ?>
                      </div>
                    </div>
                  </div>

               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php init_tail(); ?>
<script>
   $(function(){

    var url = window.location.pathname;
    var id = url.substring(url.lastIndexOf('/') + 1);
    if(!empty(id) && $.isNumeric(id)) {
      $('#paymentview_edit').modal('show');
    }

    $('#pv_twh').on('keyup', function() {
      var rate_type = $('#pv_ratetype').val();
      var rate      = parseFloat($('#pv_rate').val());
      var ssetup    = $('#pv_salarysetup').val();
      var twh       = parseFloat($('#pv_twh').val());
      var ot        = parseFloat($('#pv_ot').val());
      var twh_ot = twh + ot;
      if(rate_type == 1) {
        var t_sal = rate * twh_ot;
        if(ssetup < 0) {
          var sl = ssetup.length;
          var ss = parseFloat(ssetup.substring(1, sl));
          var total_sal = t_sal - ss;
        } else {
          var total_sal = t_sal + parseFloat(ssetup);
        }
      } else if(rate_type == 2) {
        var d_rate = rate / 8;
        var t_sal = d_rate * twh_ot;
        if(ssetup < 0) {
          var sl = ssetup.length;
          var ss = parseFloat(ssetup.substring(1, sl));
          var total_sal = t_sal - ss;
        } else {
          var total_sal = t_sal + parseFloat(ssetup);
        }
      }
      $('#pv_tot_salary').val(total_sal);
    });

    $('#pv_ot').on('keyup', function() {
      var rate_type = $('#pv_ratetype').val();
      var rate      = parseFloat($('#pv_rate').val());
      var ssetup    = $('#pv_salarysetup').val();
      var twh       = parseFloat($('#pv_twh').val());
      var ot        = parseFloat($('#pv_ot').val());
      var twh_ot = twh + ot;
      if(rate_type == 1) {
        var t_sal = rate * twh_ot;
        if(ssetup < 0) {
          var sl = ssetup.length;
          var ss = parseFloat(ssetup.substring(1, sl));
          var total_sal = t_sal - ss;
        } else {
          var total_sal = t_sal + parseFloat(ssetup);
        }
      } else if(rate_type == 2) {
        var d_rate = rate / 8;
        var t_sal = d_rate * twh_ot;
        if(ssetup < 0) {
          var sl = ssetup.length;
          var ss = parseFloat(ssetup.substring(1, sl));
          var total_sal = t_sal - ss;
        } else {
          var total_sal = t_sal + parseFloat(ssetup);
        }
      }
      $('#pv_tot_salary').val(total_sal);
    });

    function pv_submit() {
      $("#paymentview_form").submit();
    }

    var SalaryServerParams = {};
    initDataTable('.table-salary', window.location.href, [0], [0], SalaryServerParams,<?php echo hooks()->apply_filters('salary_table_default_order', json_encode(array(3,'asc'))); ?>);
  });
</script>
</body>
</html>
