<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s mbot10">
                    <div class="panel-body">
                        <button type="button" class="btn btn-info modal-md" id="add_new" data-target="#add_bonus" data-toggle="modal">
                            <?php echo _l('bonus_add');?>
                        </button>
                    </div>
                </div>
                <div id="add_bonus" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <strong><?php echo _l('bonus_setup');?></strong>
                            </div>
                            <?php echo form_open(admin_url('payroll/bonus_setup'),array('id'=>'bonussetup_form')) ?>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <span style='color:red;'>*</span> <label for="employee_name"><?php echo _l('employee_name'); ?></label>
                                        <select name="employee_name" class="form-control selectpicker" id="ename" required>
                                            <option value="">Nothing Selected</option>
                                            <?php foreach($employee as $row_emp) { ?>
                                                <option value="<?php echo $row_emp['emp_id'] ?>"><?php echo $row_emp['first_name'] . ' ' . $row_emp['last_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <span style='color:red;'>*</span> <label for="bonus_name"><?php echo _l('bonus_name'); ?></label>
                                        <input type="text" class="form-control" name="bonus_name" id="bname">
                                    </div>
                                    <div class="form-group">
                                        <span style='color:red;'>*</span> <label for="bonus_total_amount"><?php echo _l('bonus_tot_amnt'); ?></label>
                                        <input type="number" class="form-control" name="bonus_total_amount" id="btotamt">
                                    </div>
                                    <div class="form-group">
                                        <label for="month_year"><?php echo 'Month/Year'; ?></label>
                                        <input type="text" class="form-control monthYearPicker" name="month_year" placeholder="<?php echo _l('salary_month') ?>" id="month_year">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-group text-right">
                                        <button type="close" class="btn btn-default w-md m-b-5" data-dismiss="modal"><?php echo _l('cancel') ; ?></button>
                                        <button type="submit" data-form="bonus_form" id="add_btn" onclick="bonus_submit()" class="btn btn-success w-md m-b-5" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('bonus_add'); ?></button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>

                <div id="add_bonus_ve" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <strong><?php echo _l('bonus_setup');?></strong>
                            </div>
                            <?php echo isset($bonus_data_edit) ? form_open(admin_url('payroll/update_bonus/'.$bonus_data_edit->bonus_id),array('id'=>'bonussetup_form')) : form_open(admin_url('payroll/bonus_setup'),array('id'=>'bonussetup_form')) ?>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <span style='color:red;'>*</span> <label for="employee_name"><?php echo _l('employee_name'); ?></label>
                                        <select name="employee_name" class="form-control selectpicker" id="ename" <?php echo isset($bonus_data) ? 'disabled' : 'required'; ?> >
                                            <option value="">Nothing Selected</option>
                                            <?php foreach($employee as $row_emp) { ?>
                                                <option <?php echo isset($bonus_data) ? $bonus_data->bonus_emp_id==$row_emp['emp_id'] ? 'selected' : '' : ''; echo isset($bonus_data_edit) ? $bonus_data_edit->bonus_emp_id==$row_emp['emp_id'] ? 'selected' : '' : '';?> value="<?php echo $row_emp['emp_id'] ?>"><?php echo $row_emp['first_name'] . ' ' . $row_emp['last_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <span style='color:red;'>*</span> <label for="bonus_name"><?php echo _l('bonus_name'); ?></label>
                                        <input type="text" class="form-control" name="bonus_name" id="bname" value="<?php echo isset($bonus_data) ? $bonus_data->bonus_name : ''; echo isset($bonus_data_edit) ? $bonus_data_edit->bonus_name : ''; ?>" <?php echo isset($bonus_data) ? 'disabled' : ''; ?>>
                                    </div>
                                    <div class="form-group">
                                        <span style='color:red;'>*</span> <label for="bonus_total_amount"><?php echo _l('bonus_tot_amnt'); ?></label>
                                        <input type="number" class="form-control" name="bonus_total_amount" id="btotamt" value="<?php echo isset($bonus_data) ? number_format($bonus_data->bonus_amount,2) : ''; echo isset($bonus_data_edit) ? number_format($bonus_data_edit->bonus_amount,2) : ''; ?>" <?php echo isset($bonus_data) ? 'disabled' : ''; ?>>
                                    </div>
                                    <?php isset($bonus_data) ? $m_y = $bonus_data->bonus_date : '';
                                          isset($bonus_data_edit) ? $m_y = $bonus_data_edit->bonus_date : '';

                                        $my_pos = strpos($m_y, '/');
                                        $my_sub = substr($m_y, 0, $my_pos); // month
                                        switch ($my_sub)
                                        {
                                            case "01":
                                                $my_sub = 'January';
                                                break;
                                            case "02":
                                                $my_sub = 'February';
                                                break;
                                            case "03":
                                                $my_sub = 'March';
                                                break;
                                            case "04":
                                                $my_sub = 'April';
                                                break;
                                            case "05":
                                                $my_sub = 'May';
                                                break;
                                            case "06":
                                                $my_sub = 'June';
                                                break;
                                            case "07":
                                                $my_sub = 'July';
                                                break;
                                            case "08":
                                                $my_sub = 'August';
                                                break;
                                            case "09":
                                                $my_sub = 'September';
                                                break;
                                            case "10":
                                                $my_sub = 'October';
                                                break;
                                            case "11":
                                                $my_sub = 'November';
                                                break;
                                            case "12":
                                                $my_sub = 'December';
                                                break;
                                        }
                                        $my_len = strlen($m_y);
                                        $mysub  = substr($m_y, $my_pos+1, $my_len); // year
                                    ?>
                                    <div class="form-group">
                                        <label for="month_year"><?php echo 'Month/Year'; ?></label>
                                        <input type="text" class="form-control monthYearPicker" name="month_year" placeholder="<?php echo _l('salary_month') ?>" id="month_year" value="<?php echo $my_sub, ' ', $mysub; ?>" <?php echo isset($bonus_data) ? 'disabled' : ''; ?>>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-group text-right">
                                        <button type="close" class="btn btn-default w-md m-b-5" data-dismiss="modal"><?php echo (isset($bonus_data_edit) ? _l('cancel') : _l('close')) ; ?></button>
                                        <?php if(isset($bonus_data)) {} 
                                        elseif($bonus_data_edit) { ?>
                                            <button type="submit" data-form="bonus_form" id="edit_btn" onclick="bonus_submit()" class="btn btn-success w-md m-b-5" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('bonus_update'); ?></button>
                                        <?php } else { ?>
                                            <button type="submit" data-form="bonus_form" id="add_btn" onclick="bonus_submit()" class="btn btn-success w-md m-b-5" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('bonus_add'); ?></button>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel_s">
                            <div class="panel-body">
                                <?php
                                    $table_data = array(
                                        _l('the_number_sign'),
                                        _l('bonus_name'),
                                        _l('employee'),
                                        _l('invoice_table_amount_heading'),
                                        _l('date'),
                                    );
                                    render_datatable($table_data,'bonus');
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link  rel="stylesheet" href="<?php echo base_url();?>assets/css/datepicker3.css">
<script>
    var url = window.location.pathname;
    var id = url.substring(url.lastIndexOf('/') + 1);
    if(!empty(id) && $.isNumeric(id)) {
      $('#add_bonus_ve').modal('show');
    }

    var BonusServerParams = {};
    initDataTable('.table-bonus', window.location.href, [0], [0], BonusServerParams,<?php echo hooks()->apply_filters('bonus_table_default_order', json_encode(array(0,'desc'))); ?>);

    $('.monthYearPicker').datepicker({
      autoclose: true,
      minViewMode: 1,
      format: 'MM yyyy'
    }).on('changeDate', function(selected){
      startDate = new Date(selected.date.valueOf());
      startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
    }); 
</script>
</body>
</html>
