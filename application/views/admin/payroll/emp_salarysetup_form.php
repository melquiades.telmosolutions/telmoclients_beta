<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="panel_s mbot10">
              <div class="panel-body">
                <button type="button" class="btn btn-info modal-md" id="add_ben" data-target="#addss" data-toggle="modal">
                  <?php echo _l('a_salary_type');?>
                </button>
              </div>
            </div>
            <div id="addss" class="modal fade" role="dialog">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <strong><?php echo _l('salary_type');?></strong>
                  </div>
                  <?php echo (isset($benefit) ? form_open(admin_url('payroll/update_benefits/'.$benefit->benefit_id),array('id'=>'benefit_form')) : form_open(admin_url('payroll/create_benefits'),array('id'=>'benefit_form'))); ?>
                    <div class="modal-body">
                      <span style='color:red;'>*</span> <label for="benefit_name"><?php echo _l('name'); ?></label>
                      <input type="text" name="benefit_name" class="form-control clear_val" id="bname" value="<?php echo (isset($benefit) ? $benefit->benefit_name : ''); ?>" required>
                      <br>
                      <?php $value = (isset($benefit) ? $benefit->benefit_description : ''); ?>
                      <?php echo render_input('benefit_desc', _l('d_desc'), $value, 'text', '', '', '', 'clear_val'); ?>
                      <?php $val = (isset($benefit) ? $benefit->default_amount : ''); ($benefit->default_amount == 0 ? $value = '' : $value = $val._l('peso')); ?>
                      <?php echo render_input('benefit_d_amount', _l('benefit_d_amount'), $value, 'number', array('placeholder'=>_l('peso')), '', '', 'clear_val'); ?>
                      <span style='color:red;'>*</span> <label for="benefit_type"><?php echo _l('t_type'); ?></label>
                      <select name="benefit_type" class="form-control clear_val" id="btype" required>
                        <option value="">Nothing Selected</option>
                        <?php if(isset($benefit) && ($benefit->benefit_type) == 1) { ?>
                          <option value="1" selected>Add</option>;
                          <option value="2">Deduct</option>;
                          <?php } elseif(isset($benefit) && ($benefit->benefit_type) == 2) { ?>
                            <option value="1">Add</option>;
                            <option value="2" selected>Deduct</option>;
                          <?php } else { ?>
                            <option value="1">Add</option>
                            <option value="2">Deduct</option>
                        <?php } ?>
                      </select>
                      <br>
                      <span style='color:red;'>*</span> <label for="benefit_status"><?php echo _l('status'); ?></label>
                      <select name="benefit_status" id="stat" class="form-control clear_val" required>
                        <option value="">Nothing Selected</option>
                        <?php if(isset($benefit) && ($benefit->status) == 1) { ?>
                          <option value="1" selected>Active</option>;
                          <option value="2">Inactive</option>;
                          <?php } elseif(isset($benefit) && ($benefit->status) == 2) { ?>
                            <option value="1">Active</option>;
                            <option value="2" selected>Inactive</option>;
                          <?php } else { ?>
                            <option value="1">Active</option>
                            <option value="2">Inactive</option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="modal-footer">
                      <div class="form-group text-right">
                        <button type="close" class="btn btn-default w-md m-b-5" data-dismiss="modal"><?php echo _l('cancel'); ?></button>
                        <button type="submit" data-form="benefit_form" onclick="submit_b()" class="btn btn-success w-md m-b-5 btn_modal" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo (isset($benefit) ? _l('u_salary_type') : _l('a_salary_type')); ?></button>
                      </div>
                    </div>
                  <?php echo form_close(); ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="panel_s">
                  <div class="panel-body">
                    <?php
                      $table_data = array(
                        _l('id'),
                        _l('name'),
                        _l('d_desc'),
                        _l('nav_rate_type'),
                        _l('amount_'),
                        _l('status'),
                      );
                      render_datatable($table_data,'benefit');
                    ?>
                  </div>
                </div>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php init_tail(); ?>
<script>
  $(function(){
    var BenefitServerParams = {};
    initDataTable('.table-benefit', window.location.href, [0], [0], BenefitServerParams,<?php echo hooks()->apply_filters('benefit_table_default_order', json_encode(array(0,'desc'))); ?>);

    $('#add_ben').on('click', function() {
      $('.clear_val').val('');
      $('.btn_modal').text('Add Salary Type Setup');
      $('#benefit_form').attr('action', admin_url+'payroll/create_benefits');
    });

    var url = window.location.pathname;
    var id = url.substring(url.lastIndexOf('/') + 1);
    if(!empty(id) && $.isNumeric(id)) {
      $('#addss').modal('show');
    }
  });
  function submit_b() {
    var n = $('#bname').val();
    var t = $('#btype').val();
    var s = $('#stat').val();
    if(!empty(n) && !empty(t) && !empty(s)) {
      $("#benefit_form").submit();
    }
  }

</script>
</body>
</html>
