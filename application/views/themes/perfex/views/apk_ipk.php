<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php $this->load->view('authentication/includes/head.php'); ?>
<style>
  .lgn
  {
      margin-top: 20px;
      background-color: black;
      border: none;
      padding: 50px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
      cursor: pointer;
      border-radius: 50%;

      font-size: 11px;
      text-transform: uppercase;
      letter-spacing: 2.5px;
      font-weight: 500;
      color: #000;
      box-shadow: 0px 8px 15px  rgba(82, 81, 80, 10);
      transition: all 0.3s ease 0s;
      outline: none;
  }
  .lgn:hover
  {
      background-color: #2EE59D;
      box-shadow: 0px 15px 20px rgba(0, 102, 255, 0.7);
      color: #fff;
      transform: translateY(-5px);
  }
  .choosepanel
  {
    margin: 0;
    background-color: #000099;
    padding: 10px;
    border-radius: 5px;
    color: white;
  }
  .h3-l
  {
      color: black;
      font-family: "Lucida Console", Monaco, monospace;
  }
  .panel-body 
  {
      margin-bottom: 30px;
  }
</style>
</head>
<body>
<div class="container"> <br/>
  <br/>
  <br/>
  <div class="row text-center ">
    <div class="col-md-12"><br/>
      <div class="company-logo">
        <?php get_company_logo(); ?>
      </div>
  </div>
  <div class="row ">
    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
      <div class="panel panel-success" id="loginBox">
        <!-- <div class="panel-heading text-left">
          Choose Login Type
        </div> -->
        <div class="panel-body">
          <div class="choosepanel">
            <h5>TEST OUT OUR DEMO ANDROID AND iOS APPS!</h5>
          </div> <hr>
          <div class="col-md-6" align="center">
            <a href="<?php echo base_url('apk/telmoclients.apk'); ?>">
                <button style="background-color:#000099 !important;border-color:#000099 !important;" type="submit" id="login-crm" class="lgn btn btn-success">
                  <img src="<?php echo base_url('assets/images/android-icon.png'); ?>" style="width: 110px;height: 120px;">
                </button>
            </a>
          </div>
          <div class="col-md-6" align="center">
          	<a href="<?php echo base_url('ipa/Telmoclients.ipa'); ?>">
              	<button style="background-color:#000099 !important;border-color:#000099 !important;" type="submit" id="login-qrcode" class="lgn btn btn-success">
              		<img src="<?php echo base_url('assets/images/ios-icon.png'); ?>" style="width: 110px;height: 120px;">
              	</button>
          	</a>
          </div>
        </div>
        <div class="panel-footer text-left">
          <a href="<?php echo site_url('authentication/login'); ?>"><?php echo _l('admin_auth_login'); ?></a>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>