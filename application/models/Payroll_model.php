<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Payroll_model extends App_Model
{

    const STATUS_ACTIVE = 1;

    const STATUS_INACTIVE = 2;

    public function __construct()
    {
        parent::__construct();
    }

    public function insert_salary_g($data = array())
    {
        return $this->db->insert(db_prefix() . 'salary_sheet_generate', $data);
    }

    public function get_salary_type_o()
    {
        return $this->db->where('benefit_type', 1)->where('status', 1)->get(db_prefix() . 'benefit')->result_array();
    }

    public function get_salary_type_z()
    {
        return $this->db->where('benefit_type', 2)->where('status', 1)->get(db_prefix() . 'benefit')->result_array();
    }

    public function add_benefit($data = array())
    {
        return $this->db->insert(db_prefix() . 'benefit', $data);
    }

    public function benefit($id)
    {
        return $this->db->where('benefit_id', $id)->get(db_prefix() . 'benefit')->row();
    }

    public function max_benefit()
    {
        return $this->db->query('SELECT MAX(benefit_id) as b_id FROM ' . db_prefix() . 'benefit')->row();
    }

    public function delete_ben($id)
    {
        $this->db->where("benefit_id", $id);  
        return $this->db->delete(db_prefix().'benefit'); 
    }

    public function update_benefit($data, $id)
    {
        return $this->db->where('benefit_id', $id)->update(db_prefix() . 'benefit', $data);
    }

    public function create_salary_setup($data = array())
    {
        return $this->db->insert(db_prefix() . 'employee_salary_setup', $data);
    }

    public function get_emp_ss($id)
    {
        return $this->db->join(db_prefix() . 'employee', db_prefix() . 'employee.emp_id=' . db_prefix() . 'employee_salary_setup.employee_id')->where('employee_id', $id)->get(db_prefix() . 'employee_salary_setup')->row();
    }

    public function get_emp_ss_amount_o($id)
    {
        $this->db->select(db_prefix() . 'employee_salary_setup.*,' . db_prefix() . 'benefit.*');
        $this->db->join(db_prefix() . 'benefit', db_prefix() . 'benefit.benefit_id=' .db_prefix() . 'employee_salary_setup.salary_type_id');
        $this->db->where(db_prefix() . 'employee_salary_setup.employee_id', $id);
        $this->db->where(db_prefix() . 'benefit.benefit_type', 1);
        $this->db->where(db_prefix() . 'benefit.status', 1);
        $this->db->where(db_prefix() . 'employee_salary_setup.type', null);
        return $this->db->get(db_prefix() . 'employee_salary_setup')->result_array();
    }

    public function get_emp_ss_amount_z($id)
    {
        $this->db->select(db_prefix() . 'employee_salary_setup.*,' . db_prefix() . 'benefit.*');
        $this->db->join(db_prefix() . 'benefit', db_prefix() . 'benefit.benefit_id=' .db_prefix() . 'employee_salary_setup.salary_type_id');
        $this->db->where(db_prefix() . 'employee_salary_setup.employee_id', $id);
        $this->db->where(db_prefix() . 'benefit.benefit_type', 2);
        $this->db->where(db_prefix() . 'benefit.status', 1);
        $this->db->where(db_prefix() . 'employee_salary_setup.type', null);
        return $this->db->get(db_prefix() . 'employee_salary_setup')->result_array();
    }

    public function get_emp_bonus($id, $bid)
    {
        return $this->db->where('employee_id', $id)->where('salary_type_id', $bid)->where('type', 'bonus')->get(db_prefix() . 'employee_salary_setup')->result_array();
    }

    public function update_salary_setup($data, $id)
    {
        $term = array('employee_id' => $data['employee_id'], 'salary_type_id' => $data['salary_type_id'], 'type' => null);

        $this->db->where($term);
        return $this->db->update(db_prefix() . 'employee_salary_setup', $data);
    }

    public function update_salary_setup_bonus($data, $id)
    {
        $term = array('employee_id' => $data['employee_id'], 'salary_type_id' => $data['salary_type_id'], 'type' => 'bonus');

        $this->db->where($term);
        return $this->db->update(db_prefix() . 'employee_salary_setup', $data);
    }

    public function delete_emp_s_s($id)
    {
        $this->db->where("employee_id", $id);  
        return $this->db->delete(db_prefix().'employee_salary_setup'); 
    }

    public function check_salary_setup($id)
    {
        $this->db->where("employee_id", $id);  
        return $this->db->get(db_prefix().'employee_salary_setup')->result_array();
    }

    public function get_emp_salary_all($proj_id)
    {
        return $this->db->where('project_id', $proj_id)->order_by('employee_id')->join(db_prefix() . 'employee', db_prefix() . 'employee.emp_id='. db_prefix() . 'employee_salary_payment.employee_id')->get(db_prefix() . 'employee_salary_payment')->result_array();
    }
    public function get_emp_salary_thismo($startDate, $endDate, $proj_id)
    {
        return $this->db->query('
            SELECT * 
            FROM ' . db_prefix() . 'employee_salary_payment esm
            JOIN ' . db_prefix() . 'employee e
                ON e.emp_id = esm.employee_id
            JOIN ' . db_prefix() . 'salary_sheet_generate ssg
                ON ssg.ssg_id = esm.salary_name_id
            WHERE esm.project_id = ' . $proj_id . ' AND ssg.start_date BETWEEN "' . $startDate . '" AND "' . $endDate . '" AND ssg.end_date BETWEEN "' . $startDate . '" AND "' . $endDate . '" 
            ORDER BY esm.employee_id')
            ->result_array();
    }
    public function get_emp_salary_lastmo($startDate, $endDate, $proj_id)
    {
        return $this->db->query('
            SELECT * 
            FROM ' . db_prefix() . 'employee_salary_payment esm
            JOIN ' . db_prefix() . 'employee e
                ON e.emp_id = esm.employee_id
            JOIN ' . db_prefix() . 'salary_sheet_generate ssg
                ON ssg.ssg_id = esm.salary_name_id
            WHERE esm.project_id = ' . $proj_id . ' AND "' . $startDate . '" BETWEEN ssg.start_date AND ssg.end_date OR "' . $endDate . '" BETWEEN ssg.start_date AND ssg.end_date
            ORDER BY esm.employee_id')
            ->result_array();
    }

    public function date_min_max()
    {
        return $this->db->query('SELECT MIN(start_date) as min_start, MAX(end_date) as max_end FROM ' . db_prefix() . 'employee_salary_payment JOIN ' . db_prefix() . 'salary_sheet_generate ON ' . db_prefix() . 'salary_sheet_generate.ssg_id=' . db_prefix() . 'employee_salary_payment.salary_name_id')->row();
    }

    public function get_general_payroll($gp1, $gp2, $gyear1, $gyear2)
    {
        return $this->db->query('
            SELECT * 
            FROM ' . db_prefix() . 'employee_salary_payment 
            JOIN ' . db_prefix() . 'employee ON emp_id = employee_id 
            JOIN ' . db_prefix() . 'salary_sheet_generate ON ssg_id = salary_name_id 
            WHERE (start_date BETWEEN "' . $gp1 . '" AND "' . $gp2 . '" AND end_date BETWEEN "' . $gp1 . '" AND "' . $gp2 . '") AND (start_date BETWEEN "' . $gyear1 . '" AND "' . $gyear2 . '" AND end_date BETWEEN "' . $gyear1 . '" AND "' . $gyear2 . '") ')
            ->result_array();
    }

    public function get_emp_salarypayment($id)
    {
        return $this->db->where('emp_sal_pay_id', $id)->join(db_prefix() . 'employee', db_prefix() . 'employee.emp_id='. db_prefix() . 'employee_salary_payment.employee_id')->join(db_prefix() . 'salary_sheet_generate', db_prefix() . 'salary_sheet_generate.ssg_id='. db_prefix() . 'employee_salary_payment.salary_name_id')->get(db_prefix() . 'employee_salary_payment')->row();
    }

    public function update_pv_model($data, $id)
    {
        return $this->db->where('emp_sal_pay_id', $id)->update(db_prefix() . 'employee_salary_payment', $data);
    }
    
    public function del_salary_generate($id)
    {
        $this->db->where("ssg_id", $id);  
        return $this->db->delete(db_prefix().'salary_sheet_generate'); 
    }
    
    public function payslip_emp_salary_all($payslip_empid)
    {
        return $this->db->join(db_prefix() . 'employee', db_prefix() . 'employee.emp_id='. db_prefix() . 'employee_salary_payment.employee_id')->join(db_prefix() . 'salary_sheet_generate', db_prefix() . 'salary_sheet_generate.ssg_id='. db_prefix() . 'employee_salary_payment.salary_name_id')->where('employee_id', $payslip_empid)->order_by('employee_id')->order_by('employee_id')->get(db_prefix() . 'employee_salary_payment')->result_array();
    }
    public function payslip_thismo($startDate, $endDate, $payslip_empid)
    {
        return $this->db->query('
            SELECT * 
            FROM ' . db_prefix() . 'employee_salary_payment esm
            JOIN ' . db_prefix() . 'employee e
                ON e.emp_id = esm.employee_id
            JOIN ' . db_prefix() . 'salary_sheet_generate ssg
                ON ssg.ssg_id = esm.salary_name_id
            WHERE (ssg.start_date BETWEEN "' . $startDate . '" AND "' . $endDate . '" AND ssg.end_date BETWEEN "' . $startDate . '" AND "' . $endDate . '") AND esm.employee_id = "' . $payslip_empid . '" 
            ORDER BY esm.employee_id')
            ->result_array();
    }
    public function payslip_lastmo($startDate, $endDate, $payslip_empid)
    {
        return $this->db->query('
            SELECT * 
            FROM ' . db_prefix() . 'employee_salary_payment esm
            JOIN ' . db_prefix() . 'employee e
                ON e.emp_id = esm.employee_id
            JOIN ' . db_prefix() . 'salary_sheet_generate ssg
                ON ssg.ssg_id = esm.salary_name_id
            WHERE ("' . $startDate . '" BETWEEN ssg.start_date AND ssg.end_date OR "' . $endDate . '" BETWEEN ssg.start_date AND ssg.end_date) AND esm.employee_id = "' . $payslip_empid . '"
            ORDER BY esm.employee_id')
            ->result_array();
    }
    public function payslip_custom($startDate, $endDate, $payslip_empid)
    {
        return $this->db->query('
            SELECT * 
            FROM ' . db_prefix() . 'employee_salary_payment esm
            JOIN ' . db_prefix() . 'employee e
                ON e.emp_id = esm.employee_id
            JOIN ' . db_prefix() . 'salary_sheet_generate ssg
                ON ssg.ssg_id = esm.salary_name_id
            WHERE ("' . $startDate . '" BETWEEN ssg.start_date AND ssg.end_date AND "' . $endDate . '" BETWEEN ssg.start_date AND ssg.end_date) OR (ssg.start_date BETWEEN "' . $startDate . '" AND "' . $endDate . '" AND ssg.end_date BETWEEN "' . $startDate . '" AND "' . $endDate . '") AND esm.employee_id = "' . $payslip_empid . '"
            ORDER BY esm.employee_id')
            ->result_array();
    }

    public function payslip_emp_salary_all_()
    {
        return $this->db->join(db_prefix() . 'employee', db_prefix() . 'employee.emp_id='. db_prefix() . 'employee_salary_payment.employee_id')->join(db_prefix() . 'salary_sheet_generate', db_prefix() . 'salary_sheet_generate.ssg_id='. db_prefix() . 'employee_salary_payment.salary_name_id')->order_by('employee_id')->order_by('employee_id')->get(db_prefix() . 'employee_salary_payment')->result_array();
    }
    public function payslip_thismo_($startDate, $endDate)
    {
        return $this->db->query('
            SELECT * 
            FROM ' . db_prefix() . 'employee_salary_payment esm
            JOIN ' . db_prefix() . 'employee e
                ON e.emp_id = esm.employee_id
            JOIN ' . db_prefix() . 'salary_sheet_generate ssg
                ON ssg.ssg_id = esm.salary_name_id
            WHERE ssg.start_date BETWEEN "' . $startDate . '" AND "' . $endDate . '" AND ssg.end_date BETWEEN "' . $startDate . '" AND "' . $endDate . '"
            ORDER BY esm.employee_id')
            ->result_array();
    }
    public function payslip_lastmo_($startDate, $endDate)
    {
        return $this->db->query('
            SELECT * 
            FROM ' . db_prefix() . 'employee_salary_payment esm
            JOIN ' . db_prefix() . 'employee e
                ON e.emp_id = esm.employee_id
            JOIN ' . db_prefix() . 'salary_sheet_generate ssg
                ON ssg.ssg_id = esm.salary_name_id
            WHERE "' . $startDate . '" BETWEEN ssg.start_date AND ssg.end_date OR "' . $endDate . '" BETWEEN ssg.start_date AND ssg.end_date
            ORDER BY esm.employee_id')
            ->result_array();
    }
    public function payslip_custom_($startDate, $endDate)
    {
        return $this->db->query('
            SELECT * 
            FROM ' . db_prefix() . 'employee_salary_payment esm
            JOIN ' . db_prefix() . 'employee e
                ON e.emp_id = esm.employee_id
            JOIN ' . db_prefix() . 'salary_sheet_generate ssg
                ON ssg.ssg_id = esm.salary_name_id
            WHERE ("' . $startDate . '" BETWEEN ssg.start_date AND ssg.end_date AND "' . $endDate . '" BETWEEN ssg.start_date AND ssg.end_date) OR (ssg.start_date BETWEEN "' . $startDate . '" AND "' . $endDate . '" AND ssg.end_date BETWEEN "' . $startDate . '" AND "' . $endDate . '")
            ORDER BY esm.employee_id')
            ->result_array();
    }

    public function add_bonus($data)
    {
        return $this->db->insert(db_prefix() . 'bonus_setup', $data);
    }

    public function get_bonus($id)
    {
        return $this->db->where('bonus_id', $id)->get(db_prefix() . 'bonus_setup')->row();
    }

    public function update_bonus($data, $id)
    {
        return $this->db->where('bonus_id', $id)->update(db_prefix() . 'bonus_setup', $data);
    }

    public function delete_bonus($id)
    {
        return $this->db->where("bonus_id", $id)->delete(db_prefix().'bonus_setup'); 
    }

    public function delete_salaryname($id)
    {
        $get_id = $this->db->where("salary_name_id", $id)->get(db_prefix().'employee_salary_payment')->result_array();
        foreach($get_id as $id_) {
            $this->db->where("emp_sal_pay_id", $id_['emp_sal_pay_id'])->delete(db_prefix().'employeesalarypayment_project');
        }
        $this->db->where("salary_name_id", $id)->delete(db_prefix().'employee_salary_payment');
        return $this->db->where("ssg_id", $id)->delete(db_prefix().'salary_sheet_generate'); 
    }

    public function delete_salary($id)
    {
        $get_id = $this->db->where("emp_sal_pay_id", $id)->get(db_prefix().'employee_salary_payment')->result_array();
        foreach($get_id as $id_) {
            $this->db->where("emp_sal_pay_id", $id_['emp_sal_pay_id'])->delete(db_prefix().'employeesalarypayment_project');
        }
        return $this->db->where("emp_sal_pay_id", $id)->delete(db_prefix().'employee_salary_payment');
    }
}