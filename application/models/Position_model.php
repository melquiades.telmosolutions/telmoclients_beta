<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Position_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        return $this->db->get(db_prefix() . 'position')->result_array();
    }
}
