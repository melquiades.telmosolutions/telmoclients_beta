<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Employee_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_country()
    {
        return $this->db->get(db_prefix() . 'countries')->result_array();
    }

    public function get_duty_type()
    {
        return $this->db->get(db_prefix() . 'duty_type')->result_array();
    }

    public function get_employee()
    {
        return $this->db->get(db_prefix() . 'employee')->result_array();
    }

    public function get_marital_status()
    {
        return $this->db->get(db_prefix() . 'marital_info')->result_array();
    }

    public function add_empp($data = array())
    {
        return $this->db->insert(db_prefix() . 'employee', $data);
    }

    public function update_empp($data = array(), $id)
    {
        return $this->db->where('emp_id', $id)->update(db_prefix() . 'employee', $data);
    }

    public function get_all_empdata($id = '')
    {
        $this->db->where('emp_id', $id);
        return $this->db->get(db_prefix() . 'employee')->row();
    }
    
    // public function get_emp_benefit_imp()
    // {
    //     $this->db->where('status', 1);
    //     $this->db->where('add_ded', 2);
    //     return $this->db->get(db_prefix() . 'benefit')->result_array();
    // }

    public function get_emp_benefit()
    {
        return $this->db->get(db_prefix() . 'benefit')->result_array();
    }

    public function get_empbenefit($id = '')
    {
        $this->db->where('employee_id', $id);
        return $this->db->get(db_prefix() . 'employee_benefit')->row();
    }

    public function delete_emp($id)
    {
        $this->db->where("emp_id", $id);  
        return $this->db->delete(db_prefix().'employee');
    }

    public function get_pos($id)
    {
        $this->db->where('pos_id', $id);
        return $this->db->get(db_prefix() . 'position')->row();
    }

    public function update_pos($data, $id)
    {
        $this->db->where('pos_id', $id);
        return $this->db->update(db_prefix() . 'position', $data);
    }

    public function add_position($data = array())
    {
        return $this->db->insert(db_prefix() . 'position', $data);
    }

    public function delete_pos($id)
    {
        $this->db->where("pos_id", $id);  
        return $this->db->delete(db_prefix().'position'); 
    }
}
